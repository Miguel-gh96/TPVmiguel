package TPV.clases;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import TPV.Ppal;
import TPV.librerias.Funciones;

@XStreamAlias("Fecha")
public class Fecha implements Serializable {

	@XStreamAlias("dia")
	private int dia;
	@XStreamAlias("mes")
	private int mes;
	@XStreamAlias("anyo")
	private int anyo;
	@XStreamAlias("fecha")
	private String fecha;
	
	

	// Constructor
	public Fecha(String introducefecha) {
		String[] SplitArray = null;
		String fechaform = null;

		if (Ppal.config.getFormFecha().equals("dd/MM/yyyy")) {
			SplitArray = introducefecha.split("/");

			this.dia = Integer.parseInt(SplitArray[0]);
			this.mes = Integer.parseInt(SplitArray[1]);
			this.anyo = Integer.parseInt(SplitArray[2]);

			fechaform = dia + "/" + mes + "/" + anyo;
			this.fecha = fechaform;
		}

		else if (Ppal.config.getFormFecha().equals("yyyy/MM/dd")) {

			SplitArray = introducefecha.split("/");

			this.anyo = Integer.parseInt(SplitArray[0]);
			this.mes = Integer.parseInt(SplitArray[1]);
			this.dia = Integer.parseInt(SplitArray[2]);

			fechaform = dia + "/" + mes + "/" + anyo;
			this.fecha = fechaform;

		}

		else if (Ppal.config.getFormFecha().equals("MM/dd/yyyy")) {
			SplitArray = introducefecha.split("/");

			this.mes = Integer.parseInt(SplitArray[0]);
			this.dia = Integer.parseInt(SplitArray[1]);
			this.anyo = Integer.parseInt(SplitArray[2]);

			fechaform = dia + "/" + mes + "/" + anyo;
			this.fecha = fechaform;
		}

		else if (Ppal.config.getFormFecha().equals("MM-dd-yyyy")) {
			SplitArray = introducefecha.split("-");

			this.mes = Integer.parseInt(SplitArray[0]);
			this.dia = Integer.parseInt(SplitArray[1]);
			this.anyo = Integer.parseInt(SplitArray[2]);

			fechaform = dia + "/" + mes + "/" + anyo;
			this.fecha = fechaform;
		}

		else if (Ppal.config.getFormFecha().equals("yyyy-MM-dd")) {

			SplitArray = introducefecha.split("-");

			this.anyo = Integer.parseInt(SplitArray[0]);
			this.mes = Integer.parseInt(SplitArray[1]);
			this.dia = Integer.parseInt(SplitArray[2]);

			fechaform = dia + "/" + mes + "/" + anyo;
			this.fecha = fechaform;

		}

		else if (Ppal.config.getFormFecha().equals("dd-MM-yyyy")) {

			SplitArray = introducefecha.split("-");

			this.dia = Integer.parseInt(SplitArray[0]);
			this.mes = Integer.parseInt(SplitArray[1]);
			this.anyo = Integer.parseInt(SplitArray[2]);

			fechaform = dia + "/" + mes + "/" + anyo;
			this.fecha = fechaform;

		}

	}

	// Constructor Dummies
	public Fecha(String introducefecha, int i) {
		String[] SplitArray = null;
		String fechaform = null;

		SplitArray = introducefecha.split("/");

		this.dia = Integer.parseInt(SplitArray[0]);
		this.mes = Integer.parseInt(SplitArray[1]);
		this.anyo = Integer.parseInt(SplitArray[2]);

		fechaform = dia + "/" + mes + "/" + anyo;
		this.fecha = fechaform;

	}

	// Getters and Setters


	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAnyo() {
		return anyo;
	}

	public void setAnyo(int anyo) {
		this.anyo = anyo;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	// toString
	public String toString() {

		if (Ppal.config == null)
			return this.fecha;

		else {

			if (Ppal.config.getFormFecha().equals("dd/MM/yyyy"))
				return this.fecha;

			else if (Ppal.config.getFormFecha().equals("yyyy/MM/dd"))
				return this.anyo + "/" + this.mes + "/" + this.dia;

			else if (Ppal.config.getFormFecha().equals("MM/dd/yyyy"))
				return this.mes + "/" + this.dia + "/" + this.anyo;

			else if (Ppal.config.getFormFecha().equals("MM-dd-yyyy"))
				return this.mes + "-" + this.dia + "-" + this.anyo;

			else if (Ppal.config.getFormFecha().equals("dd-MM-yyyy"))
				return this.dia + "-" + this.mes + "-" + this.anyo;

			else if (Ppal.config.getFormFecha().equals("yyyy-MM-dd"))
				return this.anyo + "-" + this.mes + "-" + this.dia;
		}

		return "-1";

	}

	// Pasar un string a calendar
	
        public Calendar deStringToCalendar(String fecha) {
		java.util.Date fechaDate = null;
		Calendar fechaCalendar = new GregorianCalendar();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			fechaDate = formato.parse(fecha);
			fechaCalendar.setTime(fechaDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return fechaCalendar;
	}

	// Pasar un calendar a string
	public String deCalendartoString() {
		Calendar cal = Calendar.getInstance();
		return (cal.getTime().toString());
	}

	// Pasar un calendar a GregorianCalendar
	public GregorianCalendar deCalendarToGregorian(Calendar fecha) {
		GregorianCalendar fec = new GregorianCalendar();

		fec.set(fecha.get(Calendar.YEAR), fecha.get(Calendar.MONTH), fecha.get(Calendar.DATE) - 1);

		return fec;
	}

	// obtener fecha del sistema
	public static String getTodayAs() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = new GregorianCalendar();
		return sdf.format(cal.getTime());
	}

	// comprobar fecha
	public boolean compruebafecha() {

		//Calendar cal = this.deStringToCalendar(fecha);
		int mes = this.mes;
		int dia = this.dia;
		int anyo = this.anyo;
            
		int[] meses = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		if ((mes < 1) || (mes > 12))
			return false;

		if (mes == 2) {
			int febMax = 28;

			if (((anyo % 4) == 0) && ((anyo % 100) != 0) || ((anyo % 400) == 0))
				febMax = 29;
			if ((dia < 1) || (dia > febMax))
				return false;

		} else if ((dia < 1) || (dia > meses[mes]))
			return false;

		if ((anyo < 1900) || (anyo > 2100))
			return false;

		return true;
	}

	// calcular edad ---> Si el resultado es -1 se volverá a pedir la edad.
	public int calcularedad() {
		int anyo, anyob, cont = 0;

		GregorianCalendar date = new GregorianCalendar(this.anyo, this.mes - 1, this.dia);
		GregorianCalendar dateSystem = deCalendarToGregorian(Calendar.getInstance());

		if (date.before(dateSystem) || (date.equals(dateSystem))) {
			long tiempo = dateSystem.getTimeInMillis() - date.getTimeInMillis();// diferencia
			// en milisegundos

			anyo = (int) ((tiempo) / (1000 * 60 * 60 * 24));// paso los
			// miliseguindos a días

			for (int i = date.get(Calendar.YEAR) + 1; i <= dateSystem.get(Calendar.YEAR); i++) {
				if (date.isLeapYear(i)) {
					cont++;// cuento el nº de años bisiestos entre las fechas
					i = i + 3;
				}
			}

			anyob = cont * 366;// obtengo el nº de días de años bisiestos
			anyo = anyo - anyob;// se los resto al total de días
			anyo = anyo / 365;// obtengo los años de los días restantes
			anyo = anyo + cont;// sumo los años bisiestos

		} else
			anyo = -1;

		return anyo;

	}

	// comparar fechas //validate
	public int compararFechas(Fecha fecha1) {
		int resultado = 0;
		Calendar c1 = null;
		Calendar c2 = null;

		c1 = this.deStringToCalendar(this.fecha);
		c2 = fecha1.deStringToCalendar(fecha1.fecha);

		if (c1.before(c2)) {// return 1 cuando c1 es anterior a c2
			resultado = 1;
		} else if (c1.after(c2)) {// return 0 cuando c1 es posterior a c2
			resultado = 0;
		} else if (c1.compareTo(c2) == 0) {// return -1 cuando c1 i c2 son
											// iguales
			resultado = -1;
		}

		return resultado;
	}

	// restar fechas //validate
	public int restafecha(Fecha fechas) {
		int anyo = 0, anyob = 0, cont = 0;

		GregorianCalendar fecha1 = new GregorianCalendar(this.anyo, this.mes - 1, this.dia);
		GregorianCalendar fecha2 = new GregorianCalendar(fechas.anyo, fechas.mes - 1, fechas.dia);

		if ((fecha1.get(Calendar.YEAR) >= fecha2.get(Calendar.YEAR))) {
			long tiempo = fecha1.getTimeInMillis() - fecha2.getTimeInMillis();// diferencia
			// en milisegundos

			anyo = (int) ((tiempo) / (1000 * 60 * 60 * 24));// paso los
			// miliseguindos a días

			for (int i = fecha2.get(Calendar.YEAR) + 1; i <= fecha1.get(Calendar.YEAR); i++) {
				if (fecha1.isLeapYear(i)) {
					cont++;// cuento el nº de años bisiestos entre las fechas
					i = i + 3;
				}
			}

			anyob = cont * 366;// obtengo el nº de días de años bisiestos
			anyo = anyo - anyob;// se los resto al total de días
			anyo = anyo / 365;// obtengo los años de los días restantes
			anyo = anyo + cont;// sumo los años bisiestos

		} else {
			anyo = -1;
		}

		return anyo;

	}

	/**
	 * //restafechadiamesanyo public Fecha daterest(Fecha fechas) { Fecha
	 * daterest;
	 * 
	 * GregorianCalendar fecha1 = new
	 * GregorianCalendar(this.anyo,this.mes-1,this.dia); GregorianCalendar
	 * fecha2 = new GregorianCalendar(fechas.anyo,fechas.mes-1,fechas.dia);
	 * 
	 * if(fecha1.after(fecha2)){
	 * 
	 * }
	 * 
	 * return daterest; }
	 */

	// restar fecha actual con otra posterior ---> Si el resultado es -1 se
	// volverá a pedir la fecha.
	public int restafechaSistema() {
		int anyo, anyob, cont = 0;

		GregorianCalendar date = new GregorianCalendar(this.anyo, this.mes - 1, this.dia);
		GregorianCalendar dateSystem = deCalendarToGregorian(Calendar.getInstance());

		if (date.after(dateSystem) || date.equals(dateSystem)) {
			long tiempo = date.getTimeInMillis() - dateSystem.getTimeInMillis();// diferencia
			// en milisegundos

			anyo = (int) ((tiempo) / (1000 * 60 * 60 * 24));// paso los
			// miliseguindos a días

			for (int i = date.get(Calendar.YEAR) + 1; i <= dateSystem.get(Calendar.YEAR); i++) {
				if (date.isLeapYear(i)) {
					cont++;// cuento el nº de años bisiestos entre las fechas
					i = i + 3;
				}
			}

			anyob = cont * 366;// obtengo el nº de días de años bisiestos
			anyo = anyo - anyob;// se los resto al total de días
			anyo = anyo / 365;// obtengo los años de los días restantes
			anyo = anyo + cont;// sumo los años bisiestos

		} else
			anyo = -1;

		return anyo;
	}
}
