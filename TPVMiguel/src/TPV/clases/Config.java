package TPV.clases;

import TPV.modulos.gestionPers.gestionEF.modelo.DAO.EfDAOfitxers;
import TPV.modulos.gestionPers.gestionEF.modelo.DAO.EfGraficoDAO;
import TPV.librerias.Theme;
import TPV.modulos.GestionPedidos.Modelo.Clases.Pedidos;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.EmpleadosFijos;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.SingletonUser;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.Usuario;
import TPV.modulos.moduleProducts.Modelo.Clases.Productos;
import TPV.modulos.moduleProducts.Modelo.Clases.SingletonProd;
import java.util.ArrayList;
import TPV.modulos.GestionPedidos.Modelo.Clases.SingletonPed;

public class Config {

	private char moneda;
	private int decimales;
	private String formFecha;
	private int tipoLibreria;
	
	public Config() {
            
                ArraylistEF.efi = new  ArrayList<EmpleadosFijos>();
		SingletonProd.prod = new ArrayList<Productos>();
                SingletonUser.user = new ArrayList <Usuario> ();
                SingletonPed.ped = new ArrayList<Pedidos>();
		
		this.moneda = '€';
		this.decimales = 2;
		this.formFecha = "dd/MM/yyyy";
		this.tipoLibreria = 3;
		
		Theme.temaElegido(0);//look&Feel

				
		
		
		
		
	}


	//Getters and Setters
	
	public int getTipoLibreria() {
		return tipoLibreria;
	}

	public void setTipoLibreria(int tipoLibreria) {
		this.tipoLibreria = tipoLibreria;
	}
	
	public char getMoneda() {
		return moneda;
	}

	public void setMoneda(char moneda) {
		this.moneda = moneda;
	}

	public int getDecimales() {
		return decimales;
	}

	public void setDecimales(int decimales) {
		this.decimales = decimales;
	}

	public String getFormFecha() {
		return formFecha;
	}

	public void setFormFecha(String formFecha) {
		this.formFecha = formFecha;
	}

	@Override
	public String toString() {
		return "Config [moneda=" + moneda + ", decimales=" + decimales + ", formFecha=" + formFecha +", tipoLibreria="+tipoLibreria+"]";
	}
	
	
	
}
