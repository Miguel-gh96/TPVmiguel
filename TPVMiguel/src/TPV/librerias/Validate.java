package TPV.librerias;


public class Validate {

	private static final String LETRAS_NIF = "TRWAGMYFPDXBNJZSQVHLCKE";
	
    // validate email
    public static boolean Email(String email) {
        return email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }

    // validate date dd/MM/yyyy
    public static boolean DateFormat(String date) {
    	return date.matches("(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]((175[7-9])|(17[6-9][0-9])|(1[8-9][0-9][0-9])|([2-9][0-9][0-9][0-9]))");
    }
    
    // validate name,surname,locality...
	public static boolean palabraSoloLet(String name){
		return name.matches("[a-zA-ZÀ-ÖØ-öø-ÿ]+\\.?(( |\\-)[a-zA-ZÀ-ÖØ-öø-ÿ]+\\.?)*");
	}


	// validate postal code
    public static boolean PostalCode(String postalCode) {
        return postalCode.matches("^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$");
    }
    
    // validate international phone
    public static boolean interPhone(String InPhone){
		return InPhone.matches("0{0,2}([\\+]?[\\d]{1,3} ?)?([\\(]([\\d]{2,3})[)] ?)?[0-9][0-9 \\-]{6,}( ?([xX]|([eE]xt[\\.]?)) ?([\\d]{1,5}))?");
	}
    
    // validate phone
    public static boolean phone(String phone){
		return phone.matches("^[0-9]{2,3}-? ?[0-9]{6,7}$");
	}
    
    // validate user
    public static boolean usuario(String user){
        return user.matches("[a-zA-Z1-9À-ÖØ-öø-ÿ]{4,15}");
    }
    
    // validate pass ( Between 8 and 10 characters )
    public static boolean pass(String pass){
        return pass.matches("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,16})$");
    } 
    
    // validate format nif
    public static boolean niformat(String nif){
    	return nif.matches("([0-9]{8})([A-Z])");
    }
    
    // validate la letra del nif
    public static boolean nifcompruebaletra(String nif) {

        boolean resultado = false;

        try {
        String nif1=nif.toUpperCase();
        if (nif1.matches("[0-9]{8}[" + LETRAS_NIF + "]")) {
                int dni = Integer.parseInt(nif1.substring(0, 8));
                char letraCalculada = LETRAS_NIF.charAt(dni % 23);
                if (letraCalculada == nif1.charAt(8)) {
                        resultado = true;
                }
        }
        } catch (Exception e) {
            // If  user introduce anything error.
            resultado = false;
        }

        return resultado;
}
    // validate ciformat
    public static boolean ciformat(String cif){
    	return cif.matches("^[a-zA-Z]{1}[0-9]{8}$");
    }
    
    // validate CIF
    public static boolean isCifValido(String cif) {

        boolean resultado = false;

        try {
            String vCif = cif.trim();

            int suma = 0;
            int contador = 0;
            int temporal = 0;
            int codigoControl = 0;
            String cadenaTemporal = null;
            String valoresCif = "ABCDEFGHJKLMNPQRSUVW";
            String letraControlCIF = "0123456789";
            String letraSociedadNumerica = "KLMNPQRSW";
            String primeraLetra = null;
            String ultimaLetra = null;

            // Comprueba la longitud correcta del CIF.
            if (!(vCif.length() == 9))
                return false;

            // Si encuentra alg�n caracter que no sea una letra o un n�mero, el cif
            // no es valido.
            if (vCif.matches("[^A-Za-z0-9]"))
                return false;

            // Convierte a may�sculas la cadena.
            vCif = vCif.toUpperCase();

            // Obtiene la primera letra (letra de la sociedad) y la �ltima letra del
            // CIF (letra de control).
            primeraLetra = vCif.substring(0, 1);

            // Obtiene la �ltima letra del CIF, para comprobar si es v�lida.
            ultimaLetra = vCif.substring(8, 9);

            // Comprueba si la primera letra es v�lida.
            if (valoresCif.indexOf(primeraLetra) < 0)
                return false;

            // Obtiene el c�digo de control.
            // Sumamos las cifras pares
            suma = suma + Integer.parseInt(vCif.substring(2, 3)) + Integer.parseInt(vCif.substring(4, 5))
                    + Integer.parseInt(vCif.substring(6, 7));

            // Ahora cada cifra impar la multiplicamos por dos y sumamos las cifras
            // del resultado.
            for (contador = 1; contador < 8; contador = contador + 2) {
                // Multiplica por 2
                temporal = (Integer.parseInt(vCif.substring(contador, contador + 1)) * 2);

                // Suma los digitos.
                // Diferencia si tiene una cifra, por ejemplo: 8 = 8
                // o si tiene varias, por ejemplo: 16 -> 6 + 1 = 7
                if (temporal < 10)
                    suma = suma + temporal;
                else {
                    cadenaTemporal = String.valueOf(temporal);
                    suma = suma + (Integer.parseInt(cadenaTemporal.substring(0, 1)))
                            + (Integer.parseInt(cadenaTemporal.substring(1, 2)));
                }
            }

            // Obtiene las unidades de la suma y se las resta a 10, para obtener el
            // d�gito de control.
            codigoControl = ((10 - (suma % 10)) % 10);

            // Si la letra es K, L, M, N, P, Q � S entonces al codigo de control le
            // suma 64 y
            // obtengo su ASCII para ver si coincide con la ultima letra del cif.
            if (letraSociedadNumerica.indexOf(primeraLetra) >= 0) {
                byte[] ascii = new byte[1];

                // Obtiene el c�digo ASCII asociado, al sumar 64 al c�digo de
                // control.
                if (codigoControl == 0)
                    codigoControl = 10;
                codigoControl = codigoControl + 64;
                ascii[0] = (Integer.valueOf(codigoControl)).byteValue();

                // El �ltimo d�gito tiene que coincidir con el d�gito de control
                // obtenido
                resultado = (ultimaLetra.equals(new String(ascii)));
            } else {
                // Para el resto de letras de comienzo de CIF el �ltimo d�gito debe ser
                // num�rico,
                // y coincidir con el c�digo de control.
                resultado = (codigoControl == letraControlCIF.indexOf(ultimaLetra));
            }
        } catch (Exception e) {
            // Si ha habido alg�n error es porque hay alg�n parseo que tira bien.
            resultado = false;
        }
        return resultado;
    }


}
