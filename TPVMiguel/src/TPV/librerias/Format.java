package TPV.librerias;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Format {

	//  3,12123 -> 3,1 //valid
	public static String format1d(float number){
		DecimalFormat format1 = new DecimalFormat(".#");
		return format1.format(number);
	}
	
	// 4.56456 -> 4,56 //valid
	public static String format2d(float number){
		DecimalFormat format1 = new DecimalFormat(".##");
		return format1.format(number);
	}
	
	// 22.7654321 -> 22,765 //valid
	public static String format3d(float number){
		DecimalFormat format1 = new DecimalFormat(".###");
		return format1.format(number);
	}
	
	// 1200323.1232 -> 1.200.323,12
	public static String format4d(double number){
		DecimalFormat format1 = new DecimalFormat("#.###.##0,00");
		return format1.format(number);
	}
	
	// 0.47 --> 47%  //valid
	public static String porcentajeFloat(float per){
		NumberFormat percentFormatter = NumberFormat.getPercentInstance(Locale.US);
		String percentOut = percentFormatter.format(per);
		return percentOut;//47%
	}
	
	// 30 --> 30% //valid
	public static String porcentajeInt(int i){
		String s = "";
			s = Integer.toString(i) + "%";
		return s;
	}
	
	//23 --> $23.00  //valid
	public static String FormatDollar(float moneda){
		NumberFormat coin=NumberFormat.getCurrencyInstance(Locale.US);//Dolar
		return coin.format(moneda);
	}
	
	//23 -->£23.00 //valid
	public static String FormatLibra(float moneda){
		NumberFormat coin=NumberFormat.getCurrencyInstance( Locale.UK );//Libras
		return coin.format(moneda);
	}
	
	//23 -->23,00€ //valid
	public static String FormatEuro(float moneda){
		NumberFormat coin=NumberFormat.getCurrencyInstance( Locale.FRANCE );//Euro
		return coin.format(moneda);
	}
	
}
