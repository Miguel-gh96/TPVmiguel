package TPV.librerias;

import javax.swing.UIManager;

import TPV.librerias.Funciones;
import TPV.modulos.gestionPers.gestionEF.vista.Configuration;
import TPV.modulos.gestionPers.gestionEF.vista.CrearEF;
import TPV.modulos.gestionPers.gestionEF.vista.PagerEF;
import TPV.modulos.gestionPers.gestionEF.vista.UpdateEF;
import javax.swing.SwingUtilities;

public class Theme {

    public static void temaElegido(int op) {
        try {
            switch (op) {
                case 0:// Metal
                    try {
                        UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                        SwingUtilities.updateComponentTreeUI(Configuration.rbtnMetal);
                    } catch (Exception e) {
                    }
                    break;

                case 1:// GTK - WINDOWS
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                    break;

                case 2:// CDE/Motif
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                    break;

                case 3:// Nimbus
                    UIManager.LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
                    for (UIManager.LookAndFeelInfo laf : lafs) {
                        if ("Nimbus".equals(laf.getName())) {
                            UIManager.setLookAndFeel(laf.getClassName());
                        }
                    }
                    break;

                case 4: //Obtener lista look&feel disponibles en el SO
                    //http://www.codigofantasma.com/blog/java-look-feel-parte-1/
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.mac.MacLookAndFeel");
                    break;
            }
        } catch (Exception e) {
            Funciones.resultado("No pudo cargarse la apariencia deseada", "Error");
        }
    }
}
