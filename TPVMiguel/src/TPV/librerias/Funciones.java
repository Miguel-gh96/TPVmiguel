package TPV.librerias;



import javax.swing.JOptionPane;

/**
 * 
 * @author Miguel
 * @version 1.0.0
 */
public class Funciones {

	// Para pedir solo un int, un float, un string

	/**
	 * 
	 * @param mensaje
	 * @param titulo
	 * @return a
	 */
	public static int valida_int(String mensaje, String titulo) {
		int a = 0;
		String s;
		boolean correcto = true;

		// EL usuario introduce un int.
		do {
			try {
				s = JOptionPane.showInputDialog(null, mensaje, titulo, JOptionPane.QUESTION_MESSAGE);

				a = Integer.parseInt(s);
				correcto = true;

			} catch (Exception e) {
				// Si no ha introducido un int salta error y vuelve a pedr un
				// int.
				JOptionPane
						.showMessageDialog(null, "No has introducido un num int", "Error", JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return a;
	}

	/**
	 * 
	 * @param mensaje
	 * @param titulo
	 * @return a
	 */
	public static float valida_float(String mensaje, String titulo) {

		float a = 0.0f;
		String s;
		boolean correcto = true;

		// Pide un float al usuario
		do {
			try {

				s = JOptionPane.showInputDialog(null, mensaje, titulo, JOptionPane.QUESTION_MESSAGE);

				a = Float.parseFloat(s);
				correcto = true;

			} catch (Exception e) {

				// Salta error si no ha introducido un float y reinicia.
				JOptionPane.showMessageDialog(null, "No has introducido un num float", "Error",
						JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return a;
	}

	public static double valida_double(String mensaje, String titulo) {
		double a = 0.0;
		String s;
		boolean correcto = true;

		// Pide un double al usuario
		do {
			try {

				s = JOptionPane.showInputDialog(null, mensaje, titulo, JOptionPane.QUESTION_MESSAGE);

				a = Double.parseDouble(s);
				correcto = true;

			} catch (Exception e) {

				// Salta error si no ha introducido un float y reinicia.
				JOptionPane.showMessageDialog(null, "No has introducido un num double", "Error",
						JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return a;
	}

	/**
	 * 
	 * @param mensaje
	 * @param titulo
	 * @return c
	 */
	public static char valida_char(String mensaje, String titulo) {

		char c = 0;
		String s;
		boolean correcto = true;

		// piden un char al usuario
		do {
			try {
				s = JOptionPane.showInputDialog(null, mensaje, titulo, JOptionPane.QUESTION_MESSAGE);

				c = s.charAt(0);
				correcto = true;

			} catch (Exception e) {
				// salta error si no ha introducido un char y reinicia.
				JOptionPane.showMessageDialog(null, "No has introducido una letra", "Error", JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return c;
	}

	/**
	 * 
	 * @param mensaje
	 * @param titulo
	 * @return s
	 */
	public static String valida_String(String mensaje, String titulo) {
		String s = "";
		boolean correcto = true;

		// Pide un String al ususario.
		do {
			try {
				s = JOptionPane.showInputDialog(null, mensaje, titulo, JOptionPane.QUESTION_MESSAGE);
				correcto = true;

				// Si el usuario no introduce ningún dato.
				if (s.equals("")) {
					JOptionPane.showMessageDialog(null, "Error de introducción de datos", "Error",
							JOptionPane.ERROR_MESSAGE);
					correcto = false;
				}
			} catch (Exception e) {
				// Si no ha introducido un String, salta error y reinicia.
				JOptionPane
						.showMessageDialog(null, "No has introducido una cadena", "Error", JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		return s;
	}


	// Para pedir al usuario si desea continuar o no. valida_confirmado-->El
	// usuario no tiene q introducir datos; valida_s_n--> El usuario tiene q
	// introducir datos.

	/**
	 * mensaje de confirmado
	 * 
	 * @param mensaje
	 * @param titulo
	 * @return confirmado
	 */
	public static int valida_confirmado(String mensaje, String titulo) {

		int confirmado = JOptionPane.showConfirmDialog(null, mensaje);

		return confirmado;
	}

	/**
	 * mensaje de continuar introduciendo s o n.
	 * 
	 * @param mensaje
	 * @param Titulo
	 * @return seguir
	 */
	public static char valida_s_n(String mensaje, String Titulo) {

		char seguir = 's';
		String s;
		boolean correcto = true;

		// El usuario introducira s o n dependiendo de si desea continuar o no.
		do {

			do {
				try {
					s = JOptionPane.showInputDialog(null, mensaje + "(S/N)", Titulo, JOptionPane.QUESTION_MESSAGE);

					seguir = s.charAt(0);
					seguir = Character.toUpperCase(seguir);
					correcto = true;

				} catch (Exception e) {
					JOptionPane
							.showMessageDialog(null, "Error al introducir datos", "Error", JOptionPane.ERROR_MESSAGE);
					correcto = false;
				}
			} while (correcto == false);

			if ((seguir != 'S') && (seguir != 'N'))
				JOptionPane.showMessageDialog(null, "No has introducido S o N", "Error", JOptionPane.ERROR_MESSAGE);
		} while ((seguir != 'S') && (seguir != 'N'));

		return seguir;
	}

	// Para JOptionPane.showMessage

	/**
	 * resultado
	 * 
	 * @param mensaje
	 * @return void
	 */
	public static void resultado(String mensaje, String titulo) {

		JOptionPane.showMessageDialog(null, mensaje, titulo, JOptionPane.INFORMATION_MESSAGE);
	}

}
