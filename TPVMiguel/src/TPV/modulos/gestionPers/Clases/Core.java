package TPV.modulos.gestionPers.Clases;

import TPV.clases.Fecha;
import TPV.librerias.Funciones;
import TPV.librerias.Validate;

/**
 * 
 * @author miguel
 *
 */
public class Core {

	/**
	 * Para pedir el DNI, si no es correcto se volverá a pedir.
	 * 
	 * @return String nif
	 */
	public static String pideNIF(String titulo) {
		String nif;
		boolean valido1 = false;
		boolean valido2 = false;
		do {
			do {
				nif = Funciones.valida_String("Introduzca el NIF: ",titulo);
				valido1 = Validate.niformat(nif);
				if (valido1 == false)
					Funciones.resultado("No ha introducido correctamente el NIF.", "ERROR NIF");
			} while (valido1 == false);

			valido2 = Validate.nifcompruebaletra(nif);
			if (valido2 == false)
				Funciones.resultado("Los numeros introducidos no se corresponden con la letra", "ERROR NIF");

		} while (valido2 == false);

		return nif;
	}

	/**
	 * Para pedir el CIF de una empresa, si no es correcto se volverá a pedir.
	 * 
	 * @return String cif
	 */
	public static String pideCIF() {
		String cif;
		boolean valido1 = false;
		boolean valido2 = false;

		do {
			do {
				cif = Funciones.valida_String("Introduzca el CIF de la empresea: ", "CIF");
				valido1 = Validate.ciformat(cif);
				if (valido1 == false)
					Funciones.resultado("No ha introducido correctamente el CIF de la empresa.", "ERROR CIF");
			} while (valido1 == false);

			valido2 = Validate.isCifValido(cif);
			if (valido2 == false)
				Funciones.resultado("La letra no coincide con los numeros introducidos.","ERROR CIF");

		} while (valido2 == false);
		return cif;
	}

	/**
	 * Para pedir la fecha de cualquier cosa y además podemos personalizar el mensaje y el titulo
	 * 
	 * @return Fecha fecha
	 */
	public static Fecha pideFecha(String mensaje,String titulo) {
		String Stringfecha;
		boolean valido1 = false;
		boolean valido2 = false;
		Fecha fecha;

		do {
			do {
				Stringfecha = Funciones.valida_String(mensaje, titulo);
				valido1 = Validate.DateFormat(Stringfecha);
				if (valido1 == false)
					Funciones.resultado("El formato de la fecha introducida no es el correcto.(dd/MM/yyyy)", "ERROR");
			} while (valido1 == false);

			
			fecha = new Fecha(Stringfecha);
			
			valido2 = fecha.compruebafecha();
			
			if (valido2 == false)
				Funciones.resultado("La fecha introducida no es válida.", "ERROR");

		} while (valido2 == false);

		return fecha;
	}

	/**
	 * Para pedir cualquier tipo de palabra o palabras (solo letras)
	 * @param mensaje
	 * @param titulo
	 * @return
	 */
	public static String pidesololetras(String mensaje,String titulo){
		String nombre;
		boolean valido = false;
		
		do{
		nombre = Funciones.valida_String(mensaje,titulo);
		valido = Validate.palabraSoloLet(nombre);
		if (valido == false)
			Funciones.resultado("Solo se puede introducir letras.", "ERROR");
		}while(valido == false);
		
		return nombre;
	}

	/**
	 * Para pedir el telefeno
	 * @param mensaje
	 * @param titulo
	 * @return 
	 */
	public static String pidetlfno(String mensaje,String titulo){
		String tlfno;
		boolean valido = false;
		
		do {
			tlfno = Funciones.valida_String(mensaje,titulo);
			valido = Validate.phone(tlfno);
			
			if(valido == false)
				Funciones.resultado("No ha introducido un número correcto.","ERROR");
			
		} while (valido == false);
		
		return tlfno;
	}
	
	/**
	 * Para pedir el e-mail
	 * @param mensaje
	 * @param titulo
	 * @return
	 */
	public static String pidemail(String mensaje,String titulo){
		String correo;
		boolean valido1 = false;
		do {
			correo = Funciones.valida_String(mensaje,titulo);
			valido1 = Validate.Email(correo);
			
			if(valido1 == false)
				Funciones.resultado("No ha introducido correctamente el e-mail.","ERROR");
			
		} while (valido1 == false);
		return correo;
	}
	
}
