package TPV.modulos.gestionPers.Clases;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import TPV.clases.Fecha;

@XStreamAlias("Persona")
public abstract class Persona implements Comparable<Persona>, Serializable {

    @XStreamAlias("nombre")
    private String nombre;
    @XStreamAlias("apellidos")
    private String apellidos;
    @XStreamAlias("direccion")
    private String direccion;
    @XStreamAlias("nif")
    private String nif;
    @XStreamAlias("fechanac")
    private Fecha fechanac;
    @XStreamAlias("tlfno")
    private String tlfno;
    @XStreamAlias("correo")
    private String correo;
    @XStreamAlias("edad")
    private int edad;
    @XStreamAlias("sueldo")
    private float sueldo = 0.0f;
    @XStreamAlias("login")
    private String login="";
    @XStreamAlias("password")
    private String password="";
    @XStreamAlias("avatar")
    private String avatar;
    @XStreamAlias("tipo")
    private String tipo="user";
    private int estado=0;

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }

    //Constructor
    public Persona(String nombre, String apellidos, String direccion, String nif, Fecha fechanac, String tlfno, String correo, String login, String password) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.setEdad(fechanac.calcularedad());
        //this.edad=fechanac.calcularedad(); nooooo
        this.nif = nif;
        this.fechanac = fechanac;
        this.tlfno = tlfno;
        this.correo = correo;
        this.login = login;
        this.password = password;

    }

    //constructor para  ordenar los dni.
    public Persona(String dni) {
        this.nif = dni;
    }
    
    public Persona() {

    }

    public void cambiaredad(Fecha fechanac) {
        this.setEdad(fechanac.calcularedad());
    }

    //Getters and Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public Fecha getFechanac() {
        return fechanac;
    }

    public void setFechanac(Fecha fechanac) {
        this.fechanac = fechanac;
    }

    public String getTlfno() {
        return tlfno;
    }

    public void setTlfno(String tlfno) {
        this.tlfno = tlfno;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getTipo() {
        return tipo;
    }

    public int getEstado() {
        return estado;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    //Para ordenar
    public int compareTo(Persona o) {
        if (this.getNif().compareTo(o.getNif()) > 0) {
            return 1;
        }
        if (this.getNif().compareTo(o.getNif()) < 0) {
            return -1;
        }
        return 0;
    }

    //Para comparar DNI
    public boolean equals(Object c) {
        return getNif().equals(((Persona) c).getNif());
    }

    //toString
    public String toString() {
        return "CorePersona [nombre=" + nombre + ", apellidos=" + apellidos + ", dirección=" + direccion + "Fecha nacimiento: " + fechanac + ", Nif: " + nif + ", tlfno="
                + tlfno + ", correo=" + correo + "]";
    }

    public abstract float calcularsueldo();

}
