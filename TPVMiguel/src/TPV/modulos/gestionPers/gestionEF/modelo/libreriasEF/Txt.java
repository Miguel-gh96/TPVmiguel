package TPV.modulos.gestionPers.gestionEF.modelo.libreriasEF;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import TPV.librerias.Funciones;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.EmpleadosFijos;

public class Txt {

	public static void generatxtEF() {
        String PATH = null;
        try {
            File f;
            JFileChooser fileChooser = new JFileChooser();
            
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Texto (*.txt)", "txt"));

            
            int seleccion = fileChooser.showSaveDialog(null);
            if (seleccion == JFileChooser.APPROVE_OPTION) {
                File JFC = fileChooser.getSelectedFile();
                PATH = JFC.getAbsolutePath();
                PATH=PATH+ ".txt";
                f = new File(PATH);
                
                FileOutputStream fo=new FileOutputStream(f);
				ObjectOutputStream o=new ObjectOutputStream(fo);
				o.writeObject(ArraylistEF.efi);
				o.close();
                Funciones.resultado( "Archivo TXT guardado con exito", "Archivo TXT");
            }
        } catch (Exception e) {
        	Funciones.resultado("Error al grabar el TXT", "Error");
        }
    }

	public static void generatxtOcultoEF() {
        String PATH = null;
        try {
            File f;
            PATH = new java.io.File(".").getCanonicalPath()+"/src/TPV/modulos/gestionPers/gestionEF/modelo/archivos/ef.txt";

                f = new File(PATH);
                
                FileOutputStream fo=new FileOutputStream(f);
				ObjectOutputStream o=new ObjectOutputStream(fo);
				o.writeObject(ArraylistEF.efi);
				o.close();
            
        } catch (Exception e) {
        }
    }

	public static ArrayList<EmpleadosFijos> abrir_txtEF() {
    	String PATH = null;
        try {
            File f;
            JFileChooser fileChooser = new JFileChooser();
            
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Texto (*.txt)", "txt"));

            
            int seleccion = fileChooser.showOpenDialog(null);
            if (seleccion == JFileChooser.APPROVE_OPTION) {
                File JFC = fileChooser.getSelectedFile();
                PATH = JFC.getAbsolutePath();
                f = new File(PATH);
                
                FileInputStream fi=new FileInputStream(f);
    			ObjectInputStream oi=new ObjectInputStream(fi);
    			ArraylistEF.efi = (ArrayList<EmpleadosFijos>)oi.readObject();
    			oi.close();
            }
        } catch (Exception e) {
        	Funciones.resultado( "Error al leer el TXT", "Error");
        }
    	return ArraylistEF.efi;
    }

	 public static ArrayList<EmpleadosFijos> abrir_txtOcultoEF() {
	    	String PATH = null;
	        try {
	            File f;
	            PATH = new java.io.File(".").getCanonicalPath()+"/src/TPV/modulos/gestionPers/gestionEF/modelo/archivos/ef.txt";

	                f = new File(PATH);
	                
	                FileInputStream fi=new FileInputStream(f);
	    			ObjectInputStream oi=new ObjectInputStream(fi);
	    			ArraylistEF.efi = (ArrayList<EmpleadosFijos>)oi.readObject();
	    			oi.close();
	            
	        } catch (Exception e) {
	        	
	        }
	    	return ArraylistEF.efi;
	    }
	
}
