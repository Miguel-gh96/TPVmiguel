package TPV.modulos.gestionPers.gestionEF.modelo.libreriasEF;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import TPV.librerias.Funciones;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.EmpleadosFijos;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.Annotations;

public class Xml {
	
	private static final String ENCODING = "UTF-8";

	public static void generaxmlEF() {
        String PATH=null;
		try {
			OutputStream os = new ByteArrayOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			XStream xstream = new XStream();
			Annotations.configureAliases(xstream, EmpleadosFijos.class);

            String header = "<?xml version=\"1.0\" encoding=\"" + ENCODING + "\"?>\n";
            xstream.toXML(ArraylistEF.efi, osw);
            StringBuffer xml = new StringBuffer();
            xml.append(header);
            xml.append(os.toString());
	    
            JFileChooser fileChooser = new JFileChooser();
            
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("XML (*.xml)", "xml"));

            
            int seleccion = fileChooser.showSaveDialog(null);
            if (seleccion == JFileChooser.APPROVE_OPTION) {
            	File JFC = fileChooser.getSelectedFile();
                PATH = JFC.getAbsolutePath();
                PATH = PATH+".xml";
                
                FileWriter fileXml = new FileWriter(PATH);
                fileXml.write(xml.toString());
                fileXml.close();
                osw.close();
                os.close();
                Funciones.resultado("Archivo XML guardado con exito", "Archivo XML");

            }
	    }catch (Exception e1){
	    	Funciones.resultado("Error al grabar el XML", "Error");
	    } 
    }

	public static void generaxmlOcultoEF() {
        String PATH=null;
		try {
			OutputStream os = new ByteArrayOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			XStream xstream = new XStream();
			Annotations.configureAliases(xstream, EmpleadosFijos.class);

            String header = "<?xml version=\"1.0\" encoding=\"" + ENCODING + "\"?>\n";
            xstream.toXML(ArraylistEF.efi, osw);
            StringBuffer xml = new StringBuffer();
            xml.append(header);
            xml.append(os.toString());
	    
            PATH = new java.io.File(".").getCanonicalPath()+"/src/TPV/modulos/gestionPers/gestionEF/modelo/archivos/ef.xml";

                
                FileWriter fileXml = new FileWriter(PATH);
                fileXml.write(xml.toString());
                fileXml.close();
                osw.close();
                os.close();

            
	    }catch (Exception e1){
	    } 
    }

	public static ArrayList<EmpleadosFijos> abrir_xmlEF() {
    	String PATH=null;
    	try {
            XStream xstream = new XStream();
            Annotations.configureAliases(xstream, EmpleadosFijos.class);
 
            JFileChooser fileChooser = new JFileChooser();
            
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("XML (*.xml)", "xml"));

            
            int seleccion = fileChooser.showOpenDialog(null);
            if (seleccion == JFileChooser.APPROVE_OPTION) {
            	File JFC = fileChooser.getSelectedFile();
                PATH = JFC.getAbsolutePath();
                ArraylistEF.efi = (ArrayList <EmpleadosFijos>)xstream.fromXML(new FileReader(PATH));
            }
            
        } catch (Exception e1) {
        	Funciones.resultado( "Error al leer el XML", "Error");
        }
        return ArraylistEF.efi;
    }

	 public static ArrayList<EmpleadosFijos> abrir_xmlOcultoEF() {
	    	String PATH=null;
	    	try {
	            XStream xstream = new XStream();
	            Annotations.configureAliases(xstream, EmpleadosFijos.class);
	 
	            PATH = new java.io.File(".").getCanonicalPath()+"/src/TPV/modulos/gestionPers/gestionEF/modelo/archivos/ef.xml";
	            ArraylistEF.efi = (ArrayList <EmpleadosFijos>)xstream.fromXML(new FileReader(PATH));
	            
	        } catch (Exception e1) {
	        	
	        }
	        return ArraylistEF.efi;
	    }
	
}
