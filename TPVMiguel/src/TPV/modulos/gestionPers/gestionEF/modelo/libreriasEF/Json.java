package TPV.modulos.gestionPers.gestionEF.modelo.libreriasEF;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import TPV.librerias.Funciones;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.EmpleadosFijos;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

public class Json {

	public static void generajsonEF() {// Guarda Json de empleado fijo

		String PATH = null;

		try {
			XStream xstreamjson = new XStream(new JettisonMappedXmlDriver());
			xstreamjson.setMode(XStream.NO_REFERENCES);
			xstreamjson.alias("EmpleadosFijos", EmpleadosFijos.class);

			JFileChooser fileChooser = new JFileChooser();
			
			 fileChooser.setAcceptAllFileFilterUsed(false);
			 fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("JSON (*.json)", "json"));
			
			int seleccion = fileChooser.showSaveDialog(null);
			if (seleccion == JFileChooser.APPROVE_OPTION) {
				File JFC = fileChooser.getSelectedFile();
				PATH = JFC.getAbsolutePath();
				PATH = PATH + ".json";

				Gson gson1 = new Gson();
				String json = gson1.toJson(ArraylistEF.efi);
				FileWriter fileXml = new FileWriter(PATH);
				fileXml.write(json.toString());
				fileXml.close();

				Funciones.resultado("Archivo JSON guardado con exito", "Archivo JSON");
			}

		} catch (Exception e) {
			Funciones.resultado("Archivo JSON guardado con exito", "Archivo JSON");
		}

	}

	public static void generajsonocultoEF() {//Guarda Json de empleadosfijos de forma silenciosa
		
		String PATH = null;

		try {
			XStream xstreamjson = new XStream(new JettisonMappedXmlDriver());
			xstreamjson.setMode(XStream.NO_REFERENCES);
			xstreamjson.alias("EmpleadosFijos", EmpleadosFijos.class);

			PATH = new java.io.File(".").getCanonicalPath()+"/src/TPV/modulos/gestionPers/gestionEF/modelo/archivos/ef.json";

			Gson gson1 = new Gson();
			String json = gson1.toJson(ArraylistEF.efi);
			FileWriter fileXml = new FileWriter(PATH);
			fileXml.write(json.toString());
			fileXml.close();

		} catch (Exception e) {}
		
		
		
	}

	 public static ArrayList<EmpleadosFijos> abrir_jsonEF(){//Abre Json de empleados fijos
	    	String PATH = null;
	    	EmpleadosFijos e1=new EmpleadosFijos("");
	    	
	        try {
	        	  XStream xstream = new XStream(new JettisonMappedXmlDriver());
		          xstream.setMode(XStream.NO_REFERENCES);
				  xstream.alias("EmpleasdosFijos", EmpleadosFijos.class);
				  
				  JFileChooser fileChooser = new JFileChooser();
				  
				  fileChooser.setAcceptAllFileFilterUsed(false);
				  fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("JSON (*.json)", "json"));
				  
		          int seleccion = fileChooser.showOpenDialog(null);
		          if (seleccion == JFileChooser.APPROVE_OPTION) {
		                File JFC = fileChooser.getSelectedFile();
		                PATH = JFC.getAbsolutePath();
		               
		                ArraylistEF.efi.clear();
		                JsonReader lector = new JsonReader(new FileReader(PATH));
		                JsonParser parseador = new JsonParser();
		                JsonElement raiz = parseador.parse(lector);
		            		  
		            	Gson json = new Gson();
		            	JsonArray lista = raiz.getAsJsonArray();
		            	for (JsonElement elemento : lista) {
		            		e1 = json.fromJson(elemento, EmpleadosFijos.class);
		            		ArraylistEF.efi.add(e1);
		            	}
		          } 
	        } catch (Exception e) {
	        	JOptionPane.showMessageDialog(null, "Error al leer el JSON", "Error", JOptionPane.ERROR_MESSAGE);
	        }
	        return ArraylistEF.efi;
	    }

	 public static ArrayList<EmpleadosFijos> abrir_jsonOcultoEF(){//Abre json de empleados fijos de forma silenciosa
	    	String PATH = null;
	    	EmpleadosFijos e1=new EmpleadosFijos("");
	    	
	        try {
	            PATH = new java.io.File(".").getCanonicalPath()+"/src/TPV/modulos/gestionPers/gestionEF/modelo/archivos/ef.json";

	            ArraylistEF.efi.clear();
	            JsonReader lector = new JsonReader(new FileReader(PATH));
	            JsonParser parseador = new JsonParser();
	            JsonElement raiz = parseador.parse(lector);
		            		  
	            Gson json = new Gson();
	            JsonArray lista = raiz.getAsJsonArray();
	            for (JsonElement elemento : lista) {
	            	e1 = json.fromJson(elemento, EmpleadosFijos.class);
	            	ArraylistEF.efi.add(e1);
	            }
	            
	        } catch (Exception e) {

	        }
	        return ArraylistEF.efi;
	 }
	 
	 
	 
}
