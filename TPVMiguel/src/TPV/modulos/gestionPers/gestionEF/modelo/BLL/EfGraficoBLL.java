/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.modelo.BLL;

import TPV.clases.JavaMail;
import TPV.librerias.Encriptar;
import TPV.librerias.FileUpload;
import TPV.librerias.Funciones;
import static TPV.modulos.gestionPers.gestionEF.controlador.EmpF_Controlador.frmPageref;
import TPV.modulos.gestionPers.gestionEF.modelo.DAO.EfDAOfitxers;
import TPV.modulos.gestionPers.gestionEF.modelo.DAO.EfGraficoDAO;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.EmpleadosFijos;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.miniSimpleTableModel_EmpF;
import TPV.modulos.gestionPers.gestionEF.pager.pagina;
import TPV.modulos.gestionPers.gestionEF.vista.CrearEF;
import TPV.modulos.gestionPers.gestionEF.vista.PagerEF;
import TPV.modulos.gestionPers.gestionEF.vista.UpdateEF;
import TPV.modulos.moduleLogin.controlador.Controlador_Login;
import TPV.modulos.moduleLogin.modelo.BLL.BLL_Login;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import TPV.modulos.moduleLogin.vista.VentanaConfirm;
import TPV.modulos.moduleLogin.vista.VentanaInicio;
import com.toedter.calendar.JTextFieldDateEditor;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author miguel
 */
public class EfGraficoBLL {

    public static void pideNombre() {
        EfGraficoDAO.pideNombre();
        if (CrearEF.labErrorNom.isVisible()) {
            CrearEF.txtNombre.requestFocus();
        }
    }

    

   
    public static void pideApellidos() {
        EfGraficoDAO.pideApellidos();
        if (CrearEF.labErrorApellido.isVisible()) {
            CrearEF.txtApellidos.requestFocus();
        }

    }

    public static void pideDirec() {
        EfGraficoDAO.pideDireccion();
        if (CrearEF.labErrorDirec.isVisible()) {
            CrearEF.txtDireccion.requestFocus();
        }
    }

    public static void pideDNI() {
        int pos;

        EfGraficoDAO.pideDNI();

        if (CrearEF.labErrorDNI.isVisible()) {
            CrearEF.txtDNI.requestFocus();
        } else {
            EmpleadosFijos emp = new EmpleadosFijos(CrearEF.txtDNI.getText());
            pos = buscardniEF(emp);
            if (pos != -1) {

                CrearEF.labErrorDNI.setText("El dni ya se encuentra en la base de datos");
                CrearEF.labErrorDNI.setVisible(true);
                CrearEF.txtDNI.requestFocus();
                return;
            } else {
                CrearEF.labErrorDNI.setVisible(false);
            }

        }

    }

    public static void pideFechanac() {
        EfGraficoDAO.pideFechanac();
        if (CrearEF.labErrorFechanac.isVisible()) {
            CrearEF.dateFechanac.requestFocus();
        }

    }

    public static void pideTlfno() {
        EfGraficoDAO.pideTlfno();
        if (CrearEF.labErrorTelefono.isVisible()) {
            CrearEF.txtTlfno.requestFocus();
        }
    }

    public static void pideCorreo() {
           EfGraficoDAO.pideCorreo();
        if (CrearEF.labErrorCorreo.isVisible()) {
            CrearEF.txtCorreo.requestFocus();
        } else {

            if (buscaremailEF(CrearEF.txtCorreo.getText()) != -1) {

                CrearEF.labErrorCorreo.setText("El email ya se encuentra en la base de datos");
                CrearEF.labErrorCorreo.setVisible(true);
                CrearEF.txtCorreo.requestFocus();
                return;
            } else {
                CrearEF.labErrorCorreo.setVisible(false);
            }

        }

    }

    public static void pideFechaCont() {
        EfGraficoDAO.pideFechaCont();
        if (CrearEF.labErrorFechaCont.isVisible()) {
            CrearEF.dateFechacont.requestFocus();
        }
    }

    

    public static void pideEmpF() {
        pideNombre();
        pideApellidos();
        pideDirec();
        pideDNI();
        pideFechanac();
        pideTlfno();
        pideCorreo();
        pideFechaCont();
        

        EfGraficoDAO.pideEmpF();

        if (CrearEF.labErrorNom.isShowing() == false && CrearEF.labErrorApellido.isShowing() == false
                && CrearEF.labErrorDirec.isShowing() == false && CrearEF.labErrorDNI.isShowing() == false
                && CrearEF.labErrorFechanac.isShowing() == false && CrearEF.labErrorTelefono.isShowing() == false
                && CrearEF.labErrorCorreo.isShowing() == false && CrearEF.labErrorFechaCont.isShowing() == false) {

            if (ArraylistEF.ef != null) {
                ArraylistEF.efi.add(ArraylistEF.ef);
                EmpfBLL_DB _empf = new EmpfBLL_DB();
                _empf.nuevoEmpfBLL();
                ((miniSimpleTableModel_EmpF) frmPageref.tablaFijos.getModel()).cargar();

                //Etiqueta de  usuario succesfull
                CrearEF.labCorrecto.setVisible(true);

                //Clear
                CrearEF.txtNombre.setText("");
                CrearEF.txtApellidos.setText("");
                CrearEF.txtDireccion.setText("");
                CrearEF.txtDNI.setText("");
                ((JTextFieldDateEditor) CrearEF.dateFechanac.getDateEditor()).setText("");
                CrearEF.txtTlfno.setText("");
                CrearEF.txtCorreo.setText("");
                ((JTextFieldDateEditor) CrearEF.dateFechacont.getDateEditor()).setText("");

            }

        }

    }

    public static int buscardniEF(EmpleadosFijos ef) {
        int aux = -1;
        EmpfBLL_DB _empf = new EmpfBLL_DB();
        _empf.listAllEmpfBLL();

        for (int i = 0; i <= (ArraylistEF.efi.size() - 1); i++) {
            if ((ArraylistEF.efi.get(i)).equals(ef))//buclea hasta que encuentra un dni que concuadre con el comparator de la madre y lo devuelve como aux
            {
                aux = i;
            }
        }
        return aux;//retorna aux, si lo ha encontrado dara la posicion, si no devolvera -1 lo cual significa que no hay dni que concuadre
    }

    /**
     * Función que busca coincidencias con el email introducido.Para evitar
     * repeticiones o validar el email
     *
     *
     * @param email
     * @return
     */
    public static int buscaremailEF(String email) {
        int aux = -1;
        EmpfBLL_DB _empf = new EmpfBLL_DB();
        _empf.listAllEmpfBLL();

        for (int i = 0; i <= (ArraylistEF.efi.size() - 1); i++) {
            if ((ArraylistEF.efi.get(i)).getCorreo().equals(email))//buclea hasta que encuentra un email que concuadre con el comparator de la madre y lo devuelve como aux
            {
                ArraylistEF.ef = ArraylistEF.efi.get(i);
                aux = i;
            }
        }
        return aux;//retorna aux, si lo ha encontrado dara la posicion, si no devolvera -1 lo cual significa que no hay dni que concuadre
    }

    /**
     * Función que busca coincidencias con el login introducido para evitar
     * repeticiones o validar el user. olvidar contraseña.
     *
     * @param email
     * @return
     */
    public static int buscarUserEF(String email) {
        int aux = -1;
        EmpfBLL_DB _empf = new EmpfBLL_DB();
        _empf.listAllEmpfBLL();

        for (int i = 0; i <= (ArraylistEF.efi.size() - 1); i++) {
            if ((ArraylistEF.efi.get(i)).getLogin().equals(email))//buclea hasta que encuentra un email que concuadre con el comparator de la madre y lo devuelve como aux
            {
                ArraylistEF.ef = ArraylistEF.efi.get(i);
                aux = i;
            }
        }
        return aux;//retorna aux, si lo ha encontrado dara la posicion, si no devolvera -1 lo cual significa que no hay dni que concuadre
    }

    public static void cambiaNombre() {
        EfGraficoDAO.cambiaNombre();
        if (UpdateEF.labErrorNom.isVisible()) {
            UpdateEF.txtNombre.requestFocus();
        }
    }

   

    public static void cambiaApellidos() {
        EfGraficoDAO.cambiaApellidos();
        if (UpdateEF.labErrorApellido.isVisible()) {
            UpdateEF.txtApellidos.requestFocus();
        }

    }

    public static void cambiaDirec() {

        EfGraficoDAO.cambiaDireccion();
        if (UpdateEF.labErrorDNI.isVisible()) {
            UpdateEF.txtNombre.requestFocus();
        }

    }

    public static void cambiaFechanac() {
        EfGraficoDAO.cambiaFechanac();
        if (UpdateEF.labErrorFechanac.isVisible()) {
            UpdateEF.dateFechanac.requestFocus();
        }

    }

    public static void cambiaTlfno() {
        EfGraficoDAO.cambiaTlfno();
        if (UpdateEF.labErrorTelefono.isVisible()) {
            UpdateEF.txtTlfno.requestFocus();
        }
    }

    public static void cambiaCorreo() {
        EfGraficoDAO.cambiaCorreo();
        String emailOld = ArraylistEF.ef.getCorreo();
        if (UpdateEF.labErrorCorreo.isVisible()) {
            UpdateEF.txtCorreo.requestFocus();
        } else {

            if (buscaremailEF(UpdateEF.txtCorreo.getText()) != -1) {
                if (!(UpdateEF.txtCorreo.getText().equals(emailOld))) {

                    UpdateEF.labErrorCorreo.setText("El email ya se encuentra en la base de datos");
                    UpdateEF.labErrorCorreo.setVisible(true);
                    UpdateEF.txtCorreo.requestFocus();
                    return;
                } else {
                    UpdateEF.labErrorCorreo.setVisible(false);
                }
            }

        }

    }

    public static void cambiaFechaCont() {
        EfGraficoDAO.cambiaFechaCont();
        if (UpdateEF.labErrorFechaCont.isVisible()) {
            UpdateEF.dateFechacont.requestFocus();
        }
    }

    public static void cambiaEmpF() {
        String cod, nif = "";

        cambiaNombre();
        cambiaApellidos();
        cambiaDirec();
        cambiaFechanac();
        cambiaTlfno();
        cambiaCorreo();
        cambiaFechaCont();
        

        EfGraficoDAO.cambiaEmpF(ArraylistEF.ef);
        if (UpdateEF.labErrorNom.isShowing() == false && UpdateEF.labErrorApellido.isShowing() == false
                && UpdateEF.labErrorDNI.isShowing() == false && UpdateEF.labErrorFechanac.isShowing() == false
                && UpdateEF.labErrorTelefono.isShowing() == false && UpdateEF.labErrorCorreo.isShowing() == false
                && UpdateEF.labErrorFechaCont.isShowing() == false) {
            EmpfBLL_DB _empf = new EmpfBLL_DB();
            if (_empf.modificarEmpfBLL() != 0) {
                
                ((miniSimpleTableModel_EmpF) frmPageref.tablaFijos.getModel()).cargar();

                //Para informar al usuario de que se ha producido la actualización con éxito
                UpdateEF.labUpdate.setVisible(true);
            }

        }

    }

    public static void deleteEmpF() {
        int n = ((miniSimpleTableModel_EmpF) PagerEF.tablaFijos.getModel()).getRowCount();
        String dni;
        int pos = -1, opc, selec, selec1, inicio;

        if (n != 0) {
            inicio = (pagina.currentPageIndex - 1) * pagina.itemsPerPage;
            selec = PagerEF.tablaFijos.getSelectedRow();
            selec1 = inicio + selec;
            if (selec == -1) {
                JOptionPane.showMessageDialog(null, "No hay una persona seleccionada", "Error!", 2);
            } else {
                dni = (String) PagerEF.tablaFijos.getModel().getValueAt(selec1, 0);
                opc = Funciones.valida_confirmado("Deseas borrar a la persona con DNI: " + dni, "Borrar EmpF");
                if (opc == 0) {
                    ArraylistEF.ef = new EmpleadosFijos(dni);
                    pos = EfGraficoBLL.buscardniEF(ArraylistEF.ef);
                    ArraylistEF.efi.remove(pos);
                    EmpfBLL_DB _empf = new EmpfBLL_DB();
                    _empf.borrarEmpfBLL();
                    ((miniSimpleTableModel_EmpF) PagerEF.tablaFijos.getModel()).removeRow(selec1);
                    ((miniSimpleTableModel_EmpF) frmPageref.tablaFijos.getModel()).cargar();
                }
            }
        } else {
            Funciones.resultado("lista vacía", "Error!");
        }
    }

    public static int updateEmpF() {
        int n = ((miniSimpleTableModel_EmpF) PagerEF.tablaFijos.getModel()).getRowCount();
        int selec, selec1, inicio;

        if (n != 0) {
            inicio = (pagina.currentPageIndex - 1) * pagina.itemsPerPage;
            selec = PagerEF.tablaFijos.getSelectedRow();
            selec1 = inicio + selec;

            if (selec == -1) {
                JOptionPane.showMessageDialog(null, "No hay una persona seleccionada", "Error!", 2);
            } else {
                ArraylistEF.ef = new EmpleadosFijos((String) PagerEF.tablaFijos.getModel().getValueAt(selec1, 0));
                return 1;
            }

        } else {
            JOptionPane.showMessageDialog(null, "lista vacía", "Error!", 2);

        }

        return 0;

    }

    public static void rellenarEF() {
        int pos = -1;

        pos = EfGraficoBLL.buscardniEF(ArraylistEF.ef);

        UpdateEF.txtNombre.setText(ArraylistEF.efi.get(pos).getNombre());
        UpdateEF.txtApellidos.setText(ArraylistEF.efi.get(pos).getApellidos());
        UpdateEF.txtDireccion.setText(ArraylistEF.efi.get(pos).getDireccion());
        UpdateEF.txtDNI.setText(ArraylistEF.efi.get(pos).getNif());
        ((JTextFieldDateEditor) UpdateEF.dateFechanac.getDateEditor()).setText(ArraylistEF.efi.get(pos).getFechanac().toString());
        UpdateEF.txtTlfno.setText(ArraylistEF.efi.get(pos).getTlfno());
        UpdateEF.txtCorreo.setText(ArraylistEF.efi.get(pos).getCorreo());
        ((JTextFieldDateEditor) UpdateEF.dateFechacont.getDateEditor()).setText(ArraylistEF.efi.get(pos).getFechacontratacion().toString());
        UpdateEF.labImprimeEdad.setText("" + ArraylistEF.efi.get(pos).getEdad());
        UpdateEF.labImprimeAntig.setText("" + ArraylistEF.efi.get(pos).getAntiguedad());
        FileUpload.pintar(UpdateEF.labPrintAvatar, 60, 40, 1, ArraylistEF.efi.get(pos).getAvatar());

        //Se guarda en el singleton el correo y el user por si hay modificaciones que al buscar en la BD me permita cambiar los campos.
        ArraylistEF.ef.setCorreo(ArraylistEF.efi.get(pos).getCorreo());
        ArraylistEF.ef.setLogin(ArraylistEF.efi.get(pos).getLogin());
        ArraylistEF.ef.setAvatar(ArraylistEF.efi.get(pos).getAvatar());

    }

}
