/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.modelo.BLL;

import TPV.modulos.gestionPers.gestionEF.modelo.DAO.EfDAOfitxers;

/**
 *
 * @author miguel
 */
public class EfBLLfitxers {
    
    
    public static void GuardarJSON() {
        EmpfBLL_DB _empf = new EmpfBLL_DB();
        _empf.listAllEmpfBLL();
        EfDAOfitxers.GuardarJSON();
    }
    
    public static void GuardarXML() {
        EmpfBLL_DB _empf = new EmpfBLL_DB();
        _empf.listAllEmpfBLL();
        EfDAOfitxers.GuardarXML();
    }
    
    public static void GuardarTXT() {
        EmpfBLL_DB _empf = new EmpfBLL_DB();
        _empf.listAllEmpfBLL();
        EfDAOfitxers.GuardarTXT();
    }
    
	
    
    
}
