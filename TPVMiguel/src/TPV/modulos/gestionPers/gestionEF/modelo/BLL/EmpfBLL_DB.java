/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.modelo.BLL;

import TPV.clases.ConexionBD;
import TPV.modulos.gestionPers.gestionEF.modelo.DAO.EmpfDAO_DB;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.EmpleadosFijos;
import java.sql.Connection;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class EmpfBLL_DB {
    
    public void nuevoEmpfBLL() {


        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();
        EmpfDAO_DB empf_DAO = new EmpfDAO_DB();

          empf_DAO.nuevoEmpfDAO(_con);
        _conexion_DB.CerrarConexion(_con);

    }
    
    
    public void listAllEmpfBLL() {

        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();
        EmpfDAO_DB empf_DAO = new EmpfDAO_DB();
        try {
           empf_DAO.listAllempFDAO(_con);     
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");

        }
        _conexion_DB.CerrarConexion(_con);

    }
    
    
    public int modificarEmpfBLL() {
        int resultado = 0;
        Connection _con;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();

        EmpfDAO_DB _empfDAO = new EmpfDAO_DB();

        resultado=_empfDAO.modificarUsuarioDAO(_con);

        _conexion_DB.CerrarConexion(_con);
        
        return resultado;
    }
    
    
    public void borrarEmpfBLL() {


        Connection _con;
        ConexionBD _conexion_DB = new ConexionBD();

        _con = _conexion_DB.AbrirConexion();

        EmpfDAO_DB _empfDAO = new EmpfDAO_DB();

        _empfDAO.borrarEmpleadofijoDAO(_con);

        _conexion_DB.CerrarConexion(_con);
    }
    
    
    /**
     * Obtener la edad del empleado mayor y del menor
     */
    public void empfMenorMayorBLL() {

        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();

        EmpfDAO_DB _empfDAO = new EmpfDAO_DB();
        _empfDAO.empfMenorMayorDAO(_con);
        _conexion_DB.CerrarConexion(_con);

    }
    
    /**
     * obtener la edad media de todos los clientes
     */
    public void EdadMediaBLL() {

        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();
        EmpfDAO_DB _empfDAO = new EmpfDAO_DB();
        _empfDAO.EdadMediaDAO(_con);
        _conexion_DB.CerrarConexion(_con);
    }
    
}
