  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.modelo.DAO;

import TPV.clases.Fecha;
import TPV.librerias.Encriptar;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.EmpleadosFijos;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class EmpfDAO_DB {

    public void listAllempFDAO(Connection con) {
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Fecha fechanac, fechacont;

        ArraylistEF.efi.clear();
        try {
            stmt = con.prepareStatement("SELECT * FROM TPV.EF");
            rs = stmt.executeQuery();
            EmpleadosFijos _empf = null;
           

            while (rs.next()) {

                _empf = new EmpleadosFijos();
                _empf.setNombre(rs.getString("nombre"));
                _empf.setApellidos(rs.getString("apellidos"));
                _empf.setDireccion(rs.getString("direccion"));
                _empf.setNif(rs.getString("nif"));
                fechanac = new Fecha(rs.getString("fecha_nacimiento"),1);
                _empf.setFechanac(fechanac);
                _empf.cambiaredad(fechanac);
                _empf.setTlfno(rs.getString("telefono"));
                _empf.setCorreo(rs.getString("correo"));
                _empf.setAvatar(rs.getString("avatar"));
                fechacont = new Fecha(rs.getString("fecha_contratacion"),1);
                _empf.setFechacontratacion(fechacont);
                _empf.calcularAntiguedad(fechacont);
                _empf.setSueldo(_empf.calcularsueldo());
                ArraylistEF.efi.add(_empf);

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un problema al obtener los empleados fijos!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }

    }
    
    
    public int nuevoEmpfDAO(Connection con) {
        PreparedStatement stmt = null;
        int resultado=0;
        try {
            
            stmt = con.prepareStatement("INSERT INTO TPV.EF"
                    + "(nombre,apellidos,direccion,nif,fecha_nacimiento,telefono"
                    + ",correo,edad,sueldo,avatar,Antiguedad,fecha_contratacion) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
            stmt.setString(1, ArraylistEF.ef.getNombre());
            stmt.setString(2, ArraylistEF.ef.getApellidos());
            stmt.setString(3, ArraylistEF.ef.getDireccion());
            stmt.setString(4, ArraylistEF.ef.getNif());
            stmt.setString(5, ArraylistEF.ef.getFechanac().toString());
            stmt.setString(6, ArraylistEF.ef.getTlfno());
            stmt.setString(7, ArraylistEF.ef.getCorreo());
            stmt.setInt(8, ArraylistEF.ef.getEdad());
            stmt.setFloat(9, ArraylistEF.ef.getSueldo());
            stmt.setString(10, ArraylistEF.ef.getAvatar());
            stmt.setInt(11, ArraylistEF.ef.getAntiguedad());
            stmt.setString(12, ArraylistEF.ef.getFechacontratacion().toString());
            //JOptionPane.showMessageDialog(null,ArraylistEF.ef.toString());
            resultado=stmt.executeUpdate();
            
            
           
        } catch (SQLException ex) {
            //JOptionPane.showMessageDialog(null, "Ha habido un problema al insertar un nuevo empleado fijo!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }
        return resultado;
    }
    
    public int modificarUsuarioDAO(Connection con){

        PreparedStatement stmt = null;
        int resultado=0;
  
        try {
            stmt = con.prepareStatement("UPDATE EF SET nombre=?,apellidos=?,direccion=?,fecha_nacimiento=?,telefono=?"
                    +",correo=?,edad=?,sueldo=?,avatar=?,Antiguedad=?,fecha_contratacion=? WHERE nif=?");
            stmt.setString(1, ArraylistEF.ef.getNombre());
            stmt.setString(2, ArraylistEF.ef.getApellidos());
            stmt.setString(3, ArraylistEF.ef.getDireccion());
            stmt.setString(4, ArraylistEF.ef.getFechanac().toString());
            stmt.setString(5, ArraylistEF.ef.getTlfno());
            stmt.setString(6, ArraylistEF.ef.getCorreo());
            stmt.setInt(7, ArraylistEF.ef.getEdad());
            stmt.setFloat(8, ArraylistEF.ef.getSueldo());  
            stmt.setString(9, ArraylistEF.ef.getAvatar());
            stmt.setInt(10, ArraylistEF.ef.getAntiguedad());
            stmt.setString(11, ArraylistEF.ef.getFechacontratacion().toString());
            stmt.setString(12, ArraylistEF.ef.getNif());
            resultado = stmt.executeUpdate();
            
            


        } catch (SQLException ex) {
           // throw new Exception("Ha habido un problema al insertar la factura "+ex.getMessage());
            //JOptionPane.showMessageDialog(null, "Ha habido un problema al actualizar el empleado fijo!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    //Logger.getLogger(alumnoPresencialDAO.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }
        return resultado;

    }
    
    public int borrarEmpleadofijoDAO(Connection con) {
        int resultado=0;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("DELETE FROM EF WHERE nif=?");
            stmt.setString(1, ArraylistEF.ef.getNif());
            resultado = stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un error al eliminar el empleado fijo!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en el Logger!");
                }
            }
        }
        return resultado;
    }
    
    public void empfMenorMayorDAO(Connection conexion) {

        CallableStatement cstmt = null;
        String cadena = "";
        try {
            cstmt = (CallableStatement) conexion.prepareCall("{call procedure_MaxMinEdad(?,?)}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(2, java.sql.Types.INTEGER);
            cstmt.execute();
            cadena = cadena + "Empleado fijo menor: " + cstmt.getInt(1) + " años" + "\n";
            cadena = cadena + "Empleado fijo mayor: " + cstmt.getInt(2) + " años";
            JOptionPane.showMessageDialog(null, cadena, "Empleado fijo menor/mayor", 1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error Logger!");
        }

    }
    
    public void EdadMediaDAO(Connection conexion) {

        CallableStatement cstmt = null;
        String cadena = "";
        try {
            cstmt = (CallableStatement) conexion.prepareCall("{call pocedure_EdadMedia(?)}");
            cstmt.registerOutParameter(1, java.sql.Types.DOUBLE);
            cstmt.execute();
            cadena = cadena + "Edad media de los empleados: " + (int)cstmt.getDouble(1) + " años";
            JOptionPane.showMessageDialog(null, cadena, "Edad media", 1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error Logger!");
        }

    }
    
    

}
