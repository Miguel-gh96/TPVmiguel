/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.modelo.DAO;

import static TPV.Ppal.config;
import TPV.clases.Fecha;
import TPV.librerias.FileUpload;
import TPV.librerias.Validate;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.EmpleadosFijos;
import TPV.modulos.gestionPers.gestionEF.vista.CrearEF;
import TPV.modulos.gestionPers.gestionEF.vista.UpdateEF;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import com.toedter.calendar.JTextFieldDateEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;


/**
 *
 * @author miguel
 */
public class EfGraficoDAO {

    public static void pideNombre() {

        if (CrearEF.txtNombre.getText().isEmpty()) {
            CrearEF.labErrorNom.setText("El campo del nombre no puede estar vacio ");
            CrearEF.labErrorNom.setVisible(true);
            return;
        } else {
            CrearEF.labErrorNom.setVisible(false);

            if (Validate.palabraSoloLet(CrearEF.txtNombre.getText()) == false) {
                CrearEF.labErrorNom.setText("El campo del nombre solo puede tener letras");
                CrearEF.labErrorNom.setVisible(true);
                return;
            } else {
                CrearEF.labErrorNom.setVisible(false);
            }
        }

    }
    
    
    
    
    

    public static void pideApellidos() {

        if (CrearEF.txtApellidos.getText().isEmpty()) {
            CrearEF.labErrorApellido.setText("El campo de los apellidos no puede estar vacio");
            CrearEF.labErrorApellido.setVisible(true);
            return;

        } else {
            CrearEF.labErrorApellido.setVisible(false);

            if (Validate.palabraSoloLet(CrearEF.txtApellidos.getText()) == false) {
                CrearEF.labErrorApellido.setText("El campo de los apellidos solo puede tener letras");
                CrearEF.labErrorApellido.setVisible(true);
                return;
            } else {
                CrearEF.labErrorApellido.setVisible(false);
            }
        }

    }

    public static void pideDireccion() {

        if (CrearEF.txtDireccion.getText().isEmpty()) {
            CrearEF.labErrorDirec.setText("El campo de la dirección no puede estar vacio");
            CrearEF.labErrorDirec.setVisible(true);
            return;
        } else {
            CrearEF.labErrorDirec.setVisible(false);
        }

    }

    public static void pideDNI() {

        if (CrearEF.txtDNI.getText().isEmpty()) {
            CrearEF.labErrorDNI.setText("El campo del DNI no puede estar vacio");
            CrearEF.labErrorDNI.setVisible(true);
            return;
        } else {
            CrearEF.labErrorDNI.setVisible(false);

            if (Validate.niformat(CrearEF.txtDNI.getText()) == false) {
                CrearEF.labErrorDNI.setText("No ha introducido correctamente el NIF");
                CrearEF.labErrorDNI.setVisible(true);
                return;
            } else {
                CrearEF.labErrorDNI.setVisible(false);
                if (Validate.nifcompruebaletra(CrearEF.txtDNI.getText()) == false) {
                    CrearEF.labErrorDNI.setText("Los numeros introducidos no se corresponden con la letra");
                    CrearEF.labErrorDNI.setVisible(true);
                    return;
                } else {
                    CrearEF.labErrorDNI.setVisible(false);
                }

            }

        }

    }

    public static void pideFechanac() {

        String date1;
        Fecha fechanac;

        if (CrearEF.dateFechanac.getDate() == null) {
            CrearEF.labErrorFechanac.setText("El campo fecha de nacimiento no puede estar vacio");
            CrearEF.labErrorFechanac.setVisible(true);
            return;
        } else {
            CrearEF.labErrorFechanac.setVisible(false);
            date1 = ((JTextFieldDateEditor) CrearEF.dateFechanac.getDateEditor()).getText();

            if (Validate.DateFormat(date1) == false) {
                CrearEF.labErrorFechanac.setText("El formato de la fecha introducida no es el correcto." + "(" + config.getFormFecha() + ")");
                CrearEF.labErrorFechanac.setVisible(true);
                return;
            } else {
                CrearEF.labErrorFechanac.setVisible(false);
                fechanac = new Fecha(date1);
                if (fechanac.compruebafecha() == false) {
                    CrearEF.labErrorFechanac.setText("La fecha introducida no es válida");
                    CrearEF.labErrorFechanac.setVisible(true);
                    return;
                } else {
                    CrearEF.labErrorFechanac.setVisible(false);
                    if (fechanac.calcularedad() < 16) {
                        CrearEF.labErrorFechanac.setText("No pueden haber empleados menores de 16 años");
                        CrearEF.labErrorFechanac.setVisible(true);
                        return;
                    } else {
                        CrearEF.labErrorFechanac.setVisible(false);
                    }

                }

            }

        }

    }

    public static void pideTlfno() {
        if (CrearEF.txtTlfno.getText().isEmpty()) {
            CrearEF.labErrorTelefono.setText("El campo del telefono no puede estar vacio");
            CrearEF.labErrorTelefono.setVisible(true);
            return;
        } else {
            CrearEF.labErrorTelefono.setVisible(false);

            if (Validate.phone(CrearEF.txtTlfno.getText()) == false) {
                CrearEF.labErrorTelefono.setText("No ha introducido un número correcto");
                CrearEF.labErrorTelefono.setVisible(true);
                return;
            } else {
                CrearEF.labErrorTelefono.setVisible(false);
            }
        }

    }

    public static void pideCorreo() {
        if (CrearEF.txtCorreo.getText().isEmpty()) {
            CrearEF.labErrorCorreo.setText("El campo del correo no puede estar vacio");
            CrearEF.labErrorCorreo.setVisible(true);
            return;
        } else {
            CrearEF.labErrorCorreo.setVisible(false);

            if (Validate.Email(CrearEF.txtCorreo.getText()) == false) {
                CrearEF.labErrorCorreo.setText("No ha introducido correctamente el correo");
                CrearEF.labErrorCorreo.setVisible(true);
                CrearEF.txtCorreo.requestFocus();
                return;
            } else {
                CrearEF.labErrorCorreo.setVisible(false);
            }
        }

    }

    public static void pideFechaCont() {
        String date1, date2;
        Fecha fechaCont, fechaNac;

        if (CrearEF.dateFechacont.getDate() == null) {
            CrearEF.labErrorFechaCont.setText("<html>El campo fecha de contrato no <br/> puede estar vacio</html>");
            CrearEF.labErrorFechaCont.setVisible(true);
            return;
        } else {
            CrearEF.labErrorFechaCont.setVisible(false);
            date1 = ((JTextFieldDateEditor) CrearEF.dateFechacont.getDateEditor()).getText();

            if (Validate.DateFormat(date1) == false) {
                CrearEF.labErrorFechaCont.setText("El formato de la fecha introducida no es el correcto " + "(" + config.getFormFecha() + ")");
                CrearEF.labErrorFechaCont.setVisible(true);
                return;
            } else {
                CrearEF.labErrorFechaCont.setVisible(false);
                fechaCont = new Fecha(date1);
                if (fechaCont.compruebafecha() == false) {
                    CrearEF.labErrorFechaCont.setText("La fecha introducida no es válida");
                    CrearEF.labErrorFechaCont.setVisible(true);
                    return;
                } else {
                    CrearEF.labErrorFechaCont.setVisible(false);
                    date2 = ((JTextFieldDateEditor) CrearEF.dateFechanac.getDateEditor()).getText();
                    if(date2 != null){
                    fechaNac = new Fecha(date2);
                    if (fechaNac.compararFechas(fechaCont) == 0) {
                        CrearEF.labErrorFechaCont.setText("<html> La fecha de nacimiento <br/> no puede ser mayor a la fecha de contratación </html>");
                        CrearEF.labErrorFechaCont.setVisible(true);
                        return;
                    } else {
                        CrearEF.labErrorFechaCont.setVisible(false);

                        if (fechaCont.restafecha(fechaNac) < 16) {
                            CrearEF.labErrorFechaCont.setText("<html> No pueden haber empleados <br/> menores de 16 años </html>");
                            CrearEF.labErrorFechaCont.setVisible(true);
                            return;
                        } else {
                            CrearEF.labErrorFechaCont.setVisible(false);
                        }

                    }
                    
                    }else{
                            CrearEF.labErrorFechaCont.setText("<html>No ha introducido la fecha <br/> de nacimiento</html>");
                            CrearEF.labErrorFechaCont.setVisible(true);
                            return;
                    }

                }
            }

        }

    }

    public static void pideEmpF() {
        String nombre, apellidos, direccion, nif, tlfno, correo, date1, date2,user,pass;
        Fecha fechanac, fechacontratacion;
        EmpleadosFijos empF = null;

        if (CrearEF.labErrorNom.isShowing() == false && CrearEF.labErrorApellido.isShowing() == false
                && CrearEF.labErrorDirec.isShowing() == false && CrearEF.labErrorDNI.isShowing() == false
                && CrearEF.labErrorFechanac.isShowing() == false && CrearEF.labErrorTelefono.isShowing() == false
                && CrearEF.labErrorCorreo.isShowing() == false && CrearEF.labErrorFechaCont.isShowing() == false) {

            nombre = CrearEF.txtNombre.getText();
            apellidos = CrearEF.txtApellidos.getText();
            direccion = CrearEF.txtDireccion.getText();
            nif = CrearEF.txtDNI.getText();
            date1 = ((JTextFieldDateEditor) CrearEF.dateFechanac.getDateEditor()).getText();
            fechanac = new Fecha(date1);
            date2 = ((JTextFieldDateEditor) CrearEF.dateFechacont.getDateEditor()).getText();
            fechacontratacion = new Fecha(date2);
            tlfno = CrearEF.txtTlfno.getText();
            correo = CrearEF.txtCorreo.getText();
            

           ArraylistEF.ef = new EmpleadosFijos(nombre, apellidos, direccion, nif, fechanac, fechacontratacion, tlfno, correo,"","");
           ArraylistEF.ef.setAvatar("src/TPV/imag/default-avatar.jpg");

        }
    }

    public static DefaultComboBoxModel generaEmpledosF() {
        EmpleadosFijos f;
        String fs = "";
        int n = ArraylistEF.efi.size();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();

        for (int i = 0; i < n; i++) {
            f = ArraylistEF.efi.get(i);
            fs = f.getNif() + " - " + f.getNombre() + " " + f.getApellidos();
            modelo.addElement(fs);
        }

        return modelo;
    }

    public static void cambiaNombre() {

        if (UpdateEF.txtNombre.getText().isEmpty()) {
            UpdateEF.labErrorNom.setText("<html>El campo de la contraseña no <br/> puede estar vacio</html>"    );
            UpdateEF.labErrorNom.setVisible(true);
            return;
        } else {
            UpdateEF.labErrorNom.setVisible(false);

            if (Validate.palabraSoloLet(UpdateEF.txtNombre.getText()) == false) {
                UpdateEF.labErrorNom.setText("El campo del nombre solo puede tener letras");
                UpdateEF.labErrorNom.setVisible(true);
                return;
            } else {
                UpdateEF.labErrorNom.setVisible(false);
            }
        }

    }
    
    
    

    public static void cambiaApellidos() {

        if (UpdateEF.txtApellidos.getText().isEmpty()) {
            UpdateEF.labErrorApellido.setText("El campo de los apellidos no puede estar vacio");
            UpdateEF.labErrorApellido.setVisible(true);
            return;

        } else {
            UpdateEF.labErrorApellido.setVisible(false);

            if (Validate.palabraSoloLet(UpdateEF.txtApellidos.getText()) == false) {
                UpdateEF.labErrorApellido.setText("El campo de los apellidos solo puede tener letras");
                UpdateEF.labErrorApellido.setVisible(true);
                return;
            } else {
                UpdateEF.labErrorApellido.setVisible(false);
            }
        }

    }

    public static void cambiaDireccion() {

        if (UpdateEF.txtDireccion.getText().isEmpty()) {
            UpdateEF.labErrorDNI.setText("El campo de la dirección no puede estar vacio");
            UpdateEF.labErrorDNI.setVisible(true);
            return;
        } else {
            UpdateEF.labErrorDNI.setVisible(false);
        }

    }

    public static void cambiaFechanac() {

        String date1;
        Fecha fechanac;

        if (UpdateEF.dateFechanac.getDate() == null) {
            UpdateEF.labErrorFechanac.setText("El campo fecha de nacimiento no puede estar vacio");
            UpdateEF.labErrorFechanac.setVisible(true);
            return;
        } else {
            UpdateEF.labErrorFechanac.setVisible(false);
            date1 = ((JTextFieldDateEditor) UpdateEF.dateFechanac.getDateEditor()).getText();

            fechanac = new Fecha(date1);
            if (fechanac.compruebafecha() == false) {
                UpdateEF.labErrorFechanac.setText("La fecha introducida no es válida");
                UpdateEF.labErrorFechanac.setVisible(true);
                return;
            } else {
                UpdateEF.labErrorFechanac.setVisible(false);
                if (fechanac.calcularedad() < 16) {
                    UpdateEF.labErrorFechanac.setText("No pueden haber empleados menores de 16 años");
                    UpdateEF.labErrorFechanac.setVisible(true);
                    return;
                } else {
                    UpdateEF.labErrorFechanac.setVisible(false);
                }

            }

        }

    }

    public static void cambiaTlfno() {
        if (UpdateEF.txtTlfno.getText().isEmpty()) {
            UpdateEF.labErrorTelefono.setText("El campo del telefono no puede estar vacio");
            UpdateEF.labErrorTelefono.setVisible(true);
            return;
        } else {
            UpdateEF.labErrorTelefono.setVisible(false);

            if (Validate.phone(UpdateEF.txtTlfno.getText()) == false) {
                UpdateEF.labErrorTelefono.setText("No ha introducido un número correcto");
                UpdateEF.labErrorTelefono.setVisible(true);
                return;
            } else {
                UpdateEF.labErrorTelefono.setVisible(false);
            }
        }

    }

    public static void cambiaCorreo() {
        if (UpdateEF.txtCorreo.getText().isEmpty()) {
            UpdateEF.labErrorCorreo.setText("El campo del correo no puede estar vacio");
            UpdateEF.labErrorCorreo.setVisible(true);
            return;
        } else {
            UpdateEF.labErrorCorreo.setVisible(false);

            if (Validate.Email(UpdateEF.txtCorreo.getText()) == false) {
                UpdateEF.labErrorCorreo.setText("No ha introducido correctamente el correo");
                UpdateEF.labErrorCorreo.setVisible(true);
                return;
            } else {
                UpdateEF.labErrorCorreo.setVisible(false);
            }
        }

    }

    public static void cambiaFechaCont() {
        String date1, date2;
        Fecha fechaCont, fechaNac;

        if (UpdateEF.dateFechacont.getDate() == null) {
            UpdateEF.labErrorFechaCont.setText("<html>El campo fecha de contrato no  puede <br/>estar vacio</html>");
            UpdateEF.labErrorFechaCont.setVisible(true);
            return;
        } else {
            UpdateEF.labErrorFechaCont.setVisible(false);
            date1 = ((JTextFieldDateEditor) UpdateEF.dateFechacont.getDateEditor()).getText();
            fechaCont = new Fecha(date1);
            if (fechaCont.compruebafecha() == false) {
                UpdateEF.labErrorFechaCont.setText("<html>La fecha introducida no <br/> es válida</html>");
                UpdateEF.labErrorFechaCont.setVisible(true);
                return;
            } else {
                UpdateEF.labErrorFechaCont.setVisible(false);
                date2 = ((JTextFieldDateEditor) UpdateEF.dateFechanac.getDateEditor()).getText();
                fechaNac = new Fecha(date2);
                if (fechaNac.compararFechas(fechaCont) == 0) {
                    UpdateEF.labErrorFechaCont.setText("<html>La fecha de nacimiento <br/> no puede ser mayor a la fecha de contratación<html>");
                    UpdateEF.labErrorFechaCont.setVisible(true);
                    return;
                } else {
                    UpdateEF.labErrorFechaCont.setVisible(false);
                    if (fechaCont.restafecha(fechaNac) < 16) {
                        UpdateEF.labErrorFechaCont.setText("<html>No pueden haber empleados <br/> menores de 16 años</html>");
                        UpdateEF.labErrorFechaCont.setVisible(true);
                        return;
                    } else {
                        UpdateEF.labErrorFechaCont.setVisible(false);
                    }

                }

            }

        }

    }

    public static void cambiaEmpF(EmpleadosFijos o) {
        Fecha fechanac = null, fechacont = null;
        String date1, date2;

        if (UpdateEF.labErrorNom.isShowing() == false && UpdateEF.labErrorApellido.isShowing() == false
                && UpdateEF.labErrorDNI.isShowing() == false && UpdateEF.labErrorFechanac.isShowing() == false
                && UpdateEF.labErrorTelefono.isShowing() == false && UpdateEF.labErrorCorreo.isShowing() == false
                && UpdateEF.labErrorFechaCont.isShowing() == false) {


            o.setNombre(UpdateEF.txtNombre.getText());
            

            o.setApellidos(UpdateEF.txtApellidos.getText());

            o.setDireccion(UpdateEF.txtDireccion.getText());

            o.setTlfno(UpdateEF.txtTlfno.getText());

            o.setCorreo(UpdateEF.txtCorreo.getText());
            


            date1 = ((JTextFieldDateEditor) UpdateEF.dateFechanac.getDateEditor()).getText();
            fechanac = new Fecha(date1);
            o.setFechanac(fechanac);
            o.cambiaredad(o.getFechanac());

            date2 = ((JTextFieldDateEditor) UpdateEF.dateFechacont.getDateEditor()).getText();
            fechacont = new Fecha(date2);
            o.setFechacontratacion(fechacont);
            o.calcularAntiguedad(o.getFechacontratacion());
            o.calcularsueldo();
            o.setAvatar(FileUpload.PATH_auto);
            
            

        }

    }

    

}
