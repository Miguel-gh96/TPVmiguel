/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.modelo.DAO;

import TPV.clases.ConexionBD;
import TPV.clases.Fecha;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.EmpleadosFijos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author miguel
 */
public class DummiesEfDAO {
    
    
    public static void cargarDatosEF() throws SQLException {
        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();

        PreparedStatement stmt = null;
        int resultado = 0;

        String nombre[] = {"Antonio", "Miguel", "Sergio", "Paco", "Ana"};
        String apellidos[] = {"Garcia", "Tortosa", "Gonzalez", "Muñoz", "Sanchez"};
        String dni[] = {"73099467Q", "00000000T", "00000001R", "00000002W", "00000003A"};
        String telefono[] = {"623456789", "655280807", "624567890", "625678901", "630363231"};
        String direccion[] = {"Ramon y Cajal 71", "Mayor 18", "Los Abedules 30", "Padre Fullana 10", "Doctor Pelayo 40"};
        String eMail[] = {"punkato89@hotmail.com", "miguelet@gmail.com", "sergiogonzalez89@hotmail.com", "pacomube@yahoo.com", "anasanchez7@gmail.com"};
        Fecha fechaNac[] = {new Fecha("30/08/1989", 1), new Fecha("18/10/1992", 1), new Fecha("11/07/1989", 1), new Fecha("22/06/1979", 1), new Fecha("11/08/1992", 1)};
        Fecha fechaCont[] = {new Fecha("30/08/2014", 1), new Fecha("18/10/2014", 1), new Fecha("11/07/2014", 1), new Fecha("22/06/2014", 1), new Fecha("11/08/2014", 1)};
        String login[] = {"antonio", "miguel", "sergio", "paco", "ana"};
        String password[] = {"12345", "12345", "12345", "12345", "12345"};

        _con = _conexion_DB.AbrirConexion();
        for (int i = 0; i < 5; i++) {

            EmpleadosFijos empF1 = new EmpleadosFijos(nombre[i], apellidos[i], direccion[i], dni[i], fechaNac[i], fechaCont[i], telefono[i], eMail[i], login[i], password[i]);
            stmt = _con.prepareStatement("INSERT INTO TPV.EF"
                    + "(nombre,apellidos,direccion,nif,fecha_nacimiento,telefono"
                    + ",correo,edad,sueldo,login,password,avatar,tipo,estado,Antiguedad,fecha_contratacion) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            stmt.setString(1, empF1.getNombre());
            stmt.setString(2, empF1.getApellidos());
            stmt.setString(3, empF1.getDireccion());
            stmt.setString(4, empF1.getNif());
            stmt.setString(5, empF1.getFechanac().toString());
            stmt.setString(6, empF1.getTlfno());
            stmt.setString(7, empF1.getCorreo());
            stmt.setInt(8, empF1.getEdad());
            stmt.setFloat(9, empF1.getSueldo());
            stmt.setString(10, empF1.getLogin());
            stmt.setString(11, empF1.getPassword());
            stmt.setString(12, empF1.getAvatar());
            stmt.setString(13, empF1.getTipo());
            stmt.setInt(14, empF1.getEstado());
            stmt.setInt(15, empF1.getAntiguedad());
            stmt.setString(16, empF1.getFechacontratacion().toString());

            stmt.executeUpdate();

        }
        _conexion_DB.CerrarConexion(_con);

    }
    
    
}
