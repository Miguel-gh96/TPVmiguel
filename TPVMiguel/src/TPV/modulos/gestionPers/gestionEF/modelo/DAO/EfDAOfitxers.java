/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.modelo.DAO;
import TPV.Ppal;
import TPV.librerias.Funciones;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EfBLLfitxers;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;

/**
 *
 * @author miguel
 */
public class EfDAOfitxers {
    
    
    
    public static void GuardarJSON(){
        TPV.modulos.gestionPers.gestionEF.modelo.libreriasEF.Json.generajsonEF();//Genera un archivo json con todos los datos del Arraylist de empleados fijos
       
    }
    
    public static void GuardarTXT() {
        TPV.modulos.gestionPers.gestionEF.modelo.libreriasEF.Txt.generatxtEF();//Lo mismo que antes pero con un txt
     
    }
    
    public static void GuardarXML() {
        TPV.modulos.gestionPers.gestionEF.modelo.libreriasEF.Xml.generaxmlEF();//Ídem pero para un xml
      
    }
    

	

    
}
