package TPV.modulos.gestionPers.gestionEF.modelo.clases;

import TPV.Ppal;
import TPV.clases.Fecha;
import static TPV.modulos.gestionPers.gestionEF.controlador.EmpF_Controlador.combo;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EfBLLfitxers;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EmpfBLL_DB;
import TPV.modulos.gestionPers.gestionEF.vista.PagerEF;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import TPV.modulos.gestionPers.gestionEF.pager.pagina;
import java.awt.Color;
import javax.swing.JComboBox;

public class miniSimpleTableModel_EmpF extends AbstractTableModel {

    public static ArrayList<EmpleadosFijos> datos = new ArrayList<EmpleadosFijos>();
    public static ArrayList<EmpleadosFijos> datosaux = new ArrayList<EmpleadosFijos>();
    String[] columnas = {"DNI", "Nombre", "Apellidos", "Teléfono", "Fecha Nacimiento", "Fecha Contrato", "Correo", "Dirección", "Edad", "Antigüedad", "Sueldo"};

    ////////////////////estos métodos son necesarios para que jtable funcione/////////////////////
    @Override
    public String getColumnName(int col) {
        return columnas[col].toString();
    }

    //Devuelve el numero de filas
    @Override
    public int getRowCount() {
        return datos.size();
    }

    //Devuelve el numero de columnas
    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    //Devuelve el valor del objeto en la fila y columna
    @Override
    public Object getValueAt(int row, int col) {

        Object dev = null;
        EmpleadosFijos fila = (EmpleadosFijos) datos.get(row);

        switch (col) {
            case 0:
                dev = fila.getNif();
                break;

            case 1:
                dev = fila.getNombre();
                break;

            case 2:
                dev = fila.getApellidos();
                break;

            case 3:
                dev = fila.getTlfno();
                break;

            case 4:
                dev = fila.getFechanac().toString();
                break;

            case 5:
                dev = fila.getFechacontratacion().toString();
                break;
            case 6:
                dev = fila.getCorreo();
                break;
            case 7:
                dev = fila.getDireccion();
                break;
            case 8:
                String edad = "" + fila.getEdad();
                dev = edad;
                break;
            case 9:
                String antig = "" + fila.getAntiguedad();
                dev = antig;
                break;
            case 10:
                String sueldo = "" + fila.getSueldo() + Ppal.config.getMoneda();
                dev = sueldo;
                break;
        }
        return dev;
    }

    //Determina si una fila y columna ha de ser editable
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    //Actualiza un objeto de una fila y columna
    @Override
    public void setValueAt(Object value, int row, int col) {
        EmpleadosFijos fila = (EmpleadosFijos) datos.get(row);
        Fecha nac = null, contrat = null;

        switch (col) {
            case 0:
                fila.setNif(value.toString());
                break;

            case 1:
                fila.setNombre(value.toString());
                break;

            case 2:
                fila.setApellidos(value.toString());
                break;

            case 3:
                fila.setTlfno(value.toString());
                break;

            case 4:
                nac = new Fecha(value.toString());
                fila.setFechanac(nac);
                break;

            case 5:
                contrat = new Fecha(value.toString());
                fila.setFechacontratacion(contrat);
                break;
            case 6:
                fila.setCorreo(value.toString());
                break;
            case 7:
                fila.setDireccion(value.toString());
                break;
        }
        fireTableCellUpdated(row, col);
    }

    public void addRow(EmpleadosFijos usu) {
        datos.add(usu);
        fireTableDataChanged();
    }

    public void cargar() {
        datos.clear();
        datosaux.clear();
        //ArraylistEF.efi.clear();

        EmpleadosFijos emp = null;
        EmpfBLL_DB _empf = new EmpfBLL_DB();
        _empf.listAllEmpfBLL();

        for (int i = 0; i < ArraylistEF.efi.size(); i++) {
  
            emp = ArraylistEF.efi.get(i);
            addRow(emp);
            datosaux.add(emp);
            try {
                Thread.sleep(1); //1 milliseconds
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public void filtrar() {
        datos.clear();
        int cont = 0;

        String nom = (String) ((JComboBox) combo).getSelectedItem();

        if (nom != null) {
            for (int i = 0; i < datosaux.size(); i++) {
                if (datosaux.get(i).getNombre().toLowerCase().startsWith(nom.toLowerCase())) {
                    addRow(datosaux.get(i));
                    cont++;
                }
            }

            if (cont > 0) {
                PagerEF.labResultado.setText(" " + cont);
                PagerEF.labResultado.setForeground(Color.green);
            } else {
                PagerEF.labResultado.setText(" " + cont);
                PagerEF.labResultado.setForeground(Color.red);
            }

            System.out.println("word selected: " + nom);
            pagina.initLinkBox();
        }
    }

    public EmpleadosFijos buscar(String u) {
        datos.clear();
        cargar();

        String res;
        for (int i = 0; i < datos.size(); i++) {
            res = datos.get(i).toString();
            if (res.contains(u)) {
                return datos.get(i);
            }
        }
        return null;
    }

    public int buscaUsuario(EmpleadosFijos u) {
        datos.clear();
        cargar();

        for (int i = 0; i < datos.size(); i++) {
            if (datos.get(i).equals(u)) {
                return i;
            }
        }
        return -1;
    }

    public void removeRow(int fila) {
        datos.remove(fila);
        fireTableDataChanged();
    }

    //para pintar ImageIcon
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }
}
