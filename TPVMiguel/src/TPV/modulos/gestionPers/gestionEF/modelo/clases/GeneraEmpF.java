/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.modelo.clases;

import TPV.clases.Fecha;
import java.util.Random;

/**
 *
 * @author miguel
 */
public class GeneraEmpF {
    
    public static String getCadenaAleatoria1 (int longitud){
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while ( i < longitud){
			char c = (char)r.nextInt(255);
			if ( (c >= 'A' && c <='Z') || (c >='a' && c <='z') ){
				cadenaAleatoria += c;
				i ++;
			}
		}
		return cadenaAleatoria;
    }
    public static String getCadenaAleatoria2 (int longitud){
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while ( i < longitud){
			char c = (char)r.nextInt(255);
			if ( (c >= '0' && c <='9') || (c >='a' && c <='z') ){
				cadenaAleatoria += c;
				i ++;
			}
		}
		return cadenaAleatoria;
    }
    
    public static Fecha getFechaAleatoria() {
        String cad;
        int d = 0, m = 0, y = 0, i = 0;
        Random r = new Random();
        while (i < 3) {
            if (i == 0) {
                d = r.nextInt(28);
                if ((d > 0) && (d < 29)) {
                    i++;
                }
            }
            if (i == 1) {
                m = r.nextInt(12);
                if ((m > 0) && (m < 13)) {
                    i++;
                }
            }
            if (i == 2) {
                y = r.nextInt(2015);
                if ((y > 1950) && (y < 2016)) {
                    i++;
                }
            }
        }
        cad = +d + "/" + m + "/" + y;
        Fecha f = new Fecha(cad,1);
        
        return f;
    }
    
    
}
