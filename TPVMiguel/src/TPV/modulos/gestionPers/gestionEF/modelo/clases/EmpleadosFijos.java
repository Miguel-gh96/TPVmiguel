package TPV.modulos.gestionPers.gestionEF.modelo.clases;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import TPV.Ppal;
import TPV.clases.Fecha;
import TPV.librerias.Format;
import TPV.modulos.gestionPers.Clases.Persona;

/**
 * El core específico de empleados
 *
 * @author miguel
 *
 */
@XStreamAlias("EmpleadosFijos")
public class EmpleadosFijos extends Persona implements Serializable {

    @XStreamAlias("antiguedad")
    private int antiguedad;
    @XStreamAlias("fechacontratacion")
    private Fecha fechacontratacion;

    /**
     * Constructor
     *
     * @param nombre
     * @param apellidos
     * @param direccion
     * @param nif
     * @param fechanac
     * @param fechacontratacion
     * @param tlfno
     * @param correo
     */
    //Eliminar de empleadosFijos login,pass,tipo,estado i avatar afegir foto empleat
    public EmpleadosFijos(String nombre, String apellidos, String direccion, String nif, Fecha fechanac,
            Fecha fechacontratacion, String tlfno, String correo, String login, String password) {
        super(nombre, apellidos, direccion, nif, fechanac, tlfno, correo, login, password);

        this.setAntiguedad(fechacontratacion.calcularedad());
        this.setSueldo(this.calcularsueldo());
        this.fechacontratacion = fechacontratacion;
    }

    // constructor para ordenar los dni de empleados fijos
    public EmpleadosFijos(String nif) {
        super(nif);
    }

    public EmpleadosFijos() {
        
    }

    /**
     * Getters and Setters
     *
     * @return
     */
    public int getAntiguedad() {
        return antiguedad;
    }

    public void calcularAntiguedad(Fecha fechacontratacion) {
        this.setAntiguedad(fechacontratacion.calcularedad());
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    public Fecha getFechacontratacion() {
        return fechacontratacion;
    }

    public void setFechacontratacion(Fecha fechacontratacion) {
        this.fechacontratacion = fechacontratacion;
    }

    // Modifica la cantidad de decimales de sueldo dependiendo de la
    // configuración que el usuario haya seleccionado
    public String formatoDecimalesMoneda() { // formatea el saldo con los
        // decimales y moneda elegidos
        String s = "";

        if (Ppal.config == null) {
            s = Format.format2d(getSueldo());
        } else {
            if (Ppal.config.getDecimales() == 2) {
                s = Format.format2d(getSueldo());
            } else {
                s = Format.format3d(getSueldo());
            }
            s = s + Ppal.config.getMoneda();
        }

        return s;
    }

    /**
     * toString
     *
     * @return toString
     */
    public String toString() {
        return "------Datos Personales-------" + "\n"
                + "Nombre: " + this.getNombre() + "\n"
                + "Apellidos: " + this.getApellidos() + "\n"
                + "Dirección: " + this.getDireccion() + "\n"
                + "NIF: " + this.getNif() + "\n"
                + "Teléfono: " + this.getTlfno() + "\n"
                + "Correo: " + this.getCorreo() + "\n"
                + "Fecha de nacimiento: " + this.getFechanac().toString() + "\n"
                + "Edad: " + this.getEdad() + "\n"
                + "\n" + "------Datos Empleado---------" + "\n"
                + "Fecha de contratación: " + this.fechacontratacion
                + "\n" + "Antigüedad: " + this.getAntiguedad() + "\n"
                + "Sueldo: " + formatoDecimalesMoneda() + "/mes"+"\n"
                +"avatar: "+this.getAvatar()+"\n"
                +"pass:"+this.getPassword()+"\n"
                +"tipo: "+this.getTipo();
                
    }

    /**
     * Calcular el sueldo del empleado fijo la funció fa el següent: comença a
     * sumar 50€ ,als 1000€ que cobren els empleats d'antiguetat menor a 10, a
     * partir del 10 anys d'antiguetat incloguent el any 10, quan arriva a 2200€
     * ya no aumenta més.
     *
     * @return sueldo
     */
    public float calcularsueldo() {

        if (this.getAntiguedad() < 10) {
            this.setSueldo(1000);
        } else {
            this.setSueldo(1000);
            for (int i = this.getAntiguedad(); i >= 10; i--) {
                if (this.getSueldo() < 2200) {
                    this.setSueldo(this.getSueldo() + 50);
                }
            }
        }

        return this.getSueldo();
    }

    public void setFechanac(String fechanac1) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
