/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.controlador;

import TPV.Ppal;
import TPV.librerias.FileUpload;
import TPV.modulos.gestionPers.gestionEF.autocomplete.AutocompleteJComboBox;
import TPV.modulos.gestionPers.gestionEF.autocomplete.StringSearchable;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EfBLLfitxers;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EfGraficoBLL;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EmpfBLL_DB;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.miniSimpleTableModel_EmpF;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.renderizaEF;
import TPV.modulos.gestionPers.gestionEF.pager.pagina;
import TPV.modulos.gestionPers.gestionEF.vista.Configuration;
import TPV.modulos.gestionPers.gestionEF.vista.CrearEF;
import TPV.modulos.gestionPers.gestionEF.vista.PagerEF;
import TPV.modulos.gestionPers.gestionEF.vista.UpdateEF;
import TPV.modulos.moduleLogin.controlador.Controlador_Login;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import TPV.librerias.Theme;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author miguel
 */
public class EmpF_Controlador implements ActionListener, KeyListener, MouseListener {

    public static CrearEF frmCrearef = new CrearEF();
    public static UpdateEF frmUpdateef = new UpdateEF();
    public static PagerEF frmPageref = new PagerEF();
    public static Configuration frmConfig = new Configuration();
    private int[] cont = new int[9];
    public static AutocompleteJComboBox combo = null;
    public static TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(new miniSimpleTableModel_EmpF());

    public enum Accion {

        //Acciones del PagerEF
        _labCreate,
        _labUpdate,
        _labDelete,
        _jComboBox1,
        _labJSON,
        _labXML,
        _labTXT,
        _labConfig,
        _btnPrimero,
        _btnSiguiente,
        _btnAnterior,
        _btnUltimo,
        _labMaxmin,
        _labEdadMedia,
        //Acciones del CrearEF
        _txtNombre,
        _txtApellidos,
        _txtDireccion,
        _txtDNI,
        _txtTlfno,
        _txtCorreo,
        _labAnadir,

        
        //Acciones del UpdateEF
        _txtNombre1,
        _txtApellidos1,
        _txtDireccion1,
        _txtDNI1,
        _txtTlfno1,
        _txtCorreo1,
        _labActualizar,
        _labChangeAvatar,
        //Acciones del Config
        _rbtnMetal,
        _rbtnWindows,
        _rbtnMotif,
        _rbtnNimbus,
        _labAplicar,

    }

    public EmpF_Controlador(JFrame empEF, int i) {

        if (i == 0) {//PagerEF
            this.frmPageref = (PagerEF) empEF;
        }

        if (i == 1) {//CrearEF
            this.frmCrearef = (CrearEF) empEF;
        }

        if (i == 2) {//UpdateEF
            this.frmUpdateef = (UpdateEF) empEF;
        }

        if (i == 3) {//Configuration
            this.frmConfig = (Configuration) empEF;
        }

    }

    public void iniciar(int i) {

        if (i == 0) {//PagerEF 

            /**
             * Filtro para buscar empleados por nombre + automcompletar
             */
            List<String> myWords = new ArrayList<String>();
            for (int j = 0; j <= ArraylistEF.efi.size() - 1; j++) {
                myWords.add(ArraylistEF.efi.get(j).getNombre());
            }

            StringSearchable searchable = new StringSearchable(myWords);
            combo = new AutocompleteJComboBox(searchable);
            frmPageref.panelFiltro.setLayout(new java.awt.BorderLayout());
            frmPageref.panelFiltro.add(combo);

            combo.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboActionPerformed(evt);
                }
            });
            
            /**
             * Deshabilitar botones para los usuarios no admin
             */
            if(Singleton.efLog.getTipo().equals("admin")){
                frmPageref.labCreate.setVisible(true);
                frmPageref.labDelete.setVisible(true);
                frmPageref.labUpdate.setVisible(true);
                frmPageref.labConfig.setVisible(true); 
                
            }else{
               frmPageref.labCreate.setVisible(false);
                frmPageref.labDelete.setVisible(false);
                frmPageref.labUpdate.setVisible(false);
                frmPageref.labConfig.setVisible(false);
            }

            /**
             * Cargar tabla de empleados fijos
             */
            frmPageref.tablaFijos.setModel(new miniSimpleTableModel_EmpF());
            ((miniSimpleTableModel_EmpF) frmPageref.tablaFijos.getModel()).cargar();
            frmPageref.tablaFijos.setFillsViewportHeight(true);
            frmPageref.tablaFijos.setRowSorter(sorter);
            renderizaEF renderizador = new renderizaEF();
            frmPageref.tablaFijos.setDefaultRenderer(String.class, renderizador);
            frmPageref.txtCaja.setEditable(false);

            pagina.inicializa();
            pagina.initLinkBox();

            /**
             * Componentes ActionPerformed
             */
            this.frmPageref.jComboBox1.setActionCommand("_jComboBox1");
            this.frmPageref.jComboBox1.addActionListener(this);

            this.frmPageref.btnPrimero.setActionCommand("_btnPrimero");
            this.frmPageref.btnPrimero.addActionListener(this);

            this.frmPageref.btnUltimo.setActionCommand("_btnUltimo");
            this.frmPageref.btnUltimo.addActionListener(this);

            this.frmPageref.btnSiguiente.setActionCommand("_btnSiguiente");
            this.frmPageref.btnSiguiente.addActionListener(this);

            this.frmPageref.btnAnterior.setActionCommand("_btnAnterior");
            this.frmPageref.btnAnterior.addActionListener(this);

            /**
             * Complementos Mouse Event
             */
            this.frmPageref.labMaxMin.setName("_labMaxmin");
            this.frmPageref.labMaxMin.addMouseListener(this);

            this.frmPageref.labEdadMedia.setName("_labEdadMedia");
            this.frmPageref.labEdadMedia.addMouseListener(this);

            this.frmPageref.labConfig.setName("_labConfig");
            this.frmPageref.labConfig.addMouseListener(this);

            this.frmPageref.labJSON.setName("_labJSON");
            this.frmPageref.labJSON.addMouseListener(this);

            this.frmPageref.labXML.setName("_labXML");
            this.frmPageref.labXML.addMouseListener(this);

            this.frmPageref.labTXT.setName("_labTXT");
            this.frmPageref.labTXT.addMouseListener(this);

            this.frmPageref.labCreate.setName("_labCreate");
            this.frmPageref.labCreate.addMouseListener(this);

            this.frmPageref.labUpdate.setName("_labUpdate");
            this.frmPageref.labUpdate.addMouseListener(this);

            this.frmPageref.labDelete.setName("_labDelete");
            this.frmPageref.labDelete.addMouseListener(this);

        }

        if (i == 1) {//CrearEF

            /**
             * labels de errores al introducir datos el usuario
             */
            frmCrearef.labErrorNom.setVisible(false);
            frmCrearef.labErrorApellido.setVisible(false);
            frmCrearef.labErrorCorreo.setVisible(false);
            frmCrearef.labErrorDNI.setVisible(false);
            frmCrearef.labErrorDirec.setVisible(false);
            frmCrearef.labErrorFechaCont.setVisible(false);
            frmCrearef.labErrorFechanac.setVisible(false);
            frmCrearef.labErrorTelefono.setVisible(false);

            /**
             * Etiqueta de usuarios correctamente añadidios
             */
            frmCrearef.labCorrecto.setVisible(false);

            /**
             * Rellenar datos logueo
             */
            //this.frmCrearef.labPrintUser.setText(Singleton.efLog.getLogin());
            //FileUpload.pintar(this.frmCrearef.labPrintAvatarLog,104,76,1,Singleton.efLog.getAvatar());
            /**
             * Evitamos que el ususario edite la fecha por teclado
             */
            ((JTextFieldDateEditor) frmCrearef.dateFechanac.getDateEditor()).setEditable(false);
            ((JTextFieldDateEditor) frmCrearef.dateFechacont.getDateEditor()).setEditable(false);

            /**
             * Cuando el usuario haga click en X iremos al menu pricipal
             */
            this.frmCrearef.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            this.frmCrearef.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    frmCrearef.dispose();
                    new EmpF_Controlador(new PagerEF(), 0).iniciar(0);
                }
            });

            /**
             * Generamos un vector de contadores para cada uno de los labels de
             * errores.
             */
            for (int j = 0; j < 9; j++) {
                cont[j] = 0;
            }
            

            

            /**
             * Componentes ActionListener , MouseListener and KeyListener
             */

            this.frmCrearef.labAnadir.setName("_labAnadir");
            this.frmCrearef.labAnadir.addMouseListener(this);

            this.frmCrearef.txtNombre.setActionCommand("_txtNombre");
            this.frmCrearef.txtNombre.setName("_txtNombre");
            this.frmCrearef.txtNombre.addActionListener(this);
            this.frmCrearef.txtNombre.addKeyListener(this);
            this.frmCrearef.txtNombre.addMouseListener(this);

            this.frmCrearef.txtApellidos.setActionCommand("_txtApellidos");
            this.frmCrearef.txtApellidos.setName("_txtApellidos");
            this.frmCrearef.txtApellidos.addActionListener(this);
            this.frmCrearef.txtApellidos.addKeyListener(this);
            this.frmCrearef.txtApellidos.addMouseListener(this);

            this.frmCrearef.txtDireccion.setActionCommand("_txtDireccion");
            this.frmCrearef.txtDireccion.setName("_txtDireccion");
            this.frmCrearef.txtDireccion.addActionListener(this);
            this.frmCrearef.txtDireccion.addKeyListener(this);
            this.frmCrearef.txtDireccion.addMouseListener(this);

            this.frmCrearef.txtDNI.setActionCommand("_txtDNI");
            this.frmCrearef.txtDNI.setName("_txtDNI");
            this.frmCrearef.txtDNI.addActionListener(this);
            this.frmCrearef.txtDNI.addKeyListener(this);
            this.frmCrearef.txtDNI.addMouseListener(this);

            this.frmCrearef.txtCorreo.setActionCommand("_txtCorreo");
            this.frmCrearef.txtCorreo.setName("_txtCorreo");
            this.frmCrearef.txtCorreo.addActionListener(this);
            this.frmCrearef.txtCorreo.addKeyListener(this);
            this.frmCrearef.txtCorreo.addMouseListener(this);

            this.frmCrearef.txtTlfno.setActionCommand("_txtTlfno");
            this.frmCrearef.txtTlfno.setName("_txtTlfno");
            this.frmCrearef.txtTlfno.addActionListener(this);
            this.frmCrearef.txtTlfno.addKeyListener(this);
            this.frmCrearef.txtTlfno.addMouseListener(this);

            

        }

        if (i == 2) {//UpdateEF

            /**
             * El dni no se puede modificar ni las fechas
             */
            frmUpdateef.txtDNI.setEditable(false);
            ((JTextFieldDateEditor) frmUpdateef.dateFechanac.getDateEditor()).setEditable(false);
            ((JTextFieldDateEditor) frmUpdateef.dateFechacont.getDateEditor()).setEditable(false);

            /**
             * labels de errores al introducir datos el usuario
             */
            frmUpdateef.labErrorNom.setVisible(false);
            frmUpdateef.labErrorApellido.setVisible(false);
            frmUpdateef.labErrorCorreo.setVisible(false);
            frmUpdateef.labErrorDirec.setVisible(false);
            frmUpdateef.labErrorDNI.setVisible(false);
            frmUpdateef.labErrorFechaCont.setVisible(false);
            frmUpdateef.labErrorFechanac.setVisible(false);
            frmUpdateef.labErrorTelefono.setVisible(false);

            /**
             * Cuando el usuario haga click en X iremos al menu pricipal
             */
            this.frmUpdateef.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            this.frmUpdateef.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    frmUpdateef.dispose();
                    new EmpF_Controlador(new PagerEF(), 0).iniciar(0);
                }
            });

            /**
             * Generamos un vector de contadores para cada uno de los labels de
             * errores.
             */
            for (int j = 0; j < 8; j++) {
                cont[j] = 0;
            }

            /**
             * Rellenamos los txt con los datos del empleado seleccionado
             */
            EfGraficoBLL.rellenarEF();

            /**
             * lab de aviso de actualizacion desactivado
             */
            frmUpdateef.labUpdate.setVisible(false);

            /**
             * Componentes ActionListener, KeyListener, MouseListener
             */
            this.frmUpdateef.txtNombre.setActionCommand("_txtNombre1");
            this.frmUpdateef.txtNombre.setName("_txtNombre1");
            this.frmUpdateef.txtNombre.addActionListener(this);
            this.frmUpdateef.txtNombre.addKeyListener(this);
            this.frmUpdateef.txtNombre.addMouseListener(this);

            this.frmUpdateef.txtApellidos.setActionCommand("_txtApellidos1");
            this.frmUpdateef.txtApellidos.setName("_txtApellidos1");
            this.frmUpdateef.txtApellidos.addActionListener(this);
            this.frmUpdateef.txtApellidos.addKeyListener(this);
            this.frmUpdateef.txtApellidos.addMouseListener(this);

            this.frmUpdateef.txtDireccion.setActionCommand("_txtDireccion1");
            this.frmUpdateef.txtDireccion.setName("_txtDireccion1");
            this.frmUpdateef.txtDireccion.addActionListener(this);
            this.frmUpdateef.txtDireccion.addKeyListener(this);
            this.frmUpdateef.txtDireccion.addMouseListener(this);

            this.frmUpdateef.txtDNI.setActionCommand("_txtDNI1");
            this.frmUpdateef.txtDNI.setName("_txtDNI1");
            this.frmUpdateef.txtDNI.addActionListener(this);
            this.frmUpdateef.txtDNI.addKeyListener(this);
            this.frmUpdateef.txtDNI.addMouseListener(this);

            this.frmUpdateef.txtCorreo.setActionCommand("_txtDireccion1");
            this.frmUpdateef.txtCorreo.setName("_txtDireccion1");
            this.frmUpdateef.txtCorreo.addActionListener(this);
            this.frmUpdateef.txtCorreo.addKeyListener(this);
            this.frmUpdateef.txtCorreo.addMouseListener(this);

            this.frmUpdateef.txtTlfno.setActionCommand("_txtTlfno1");
            this.frmUpdateef.txtTlfno.setName("_txtTlfno1");
            this.frmUpdateef.txtTlfno.addActionListener(this);
            this.frmUpdateef.txtTlfno.addKeyListener(this);
            this.frmUpdateef.txtTlfno.addMouseListener(this);



            this.frmUpdateef.labActualizar.setName("_labActualizar");
            this.frmUpdateef.labActualizar.addMouseListener(this);
            
            this.frmUpdateef.labChangeAvatar.setName("_labChangeAvatar");
            this.frmUpdateef.labChangeAvatar.addMouseListener(this);


        }

        if (i == 3) {//Configuration

            /**
             * Cuando el usuario haga click en X iremos al menu pricipal
             */
            this.frmConfig.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            this.frmConfig.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    frmConfig.dispose();
                    new EmpF_Controlador(new PagerEF(), 0).iniciar(0);
                }
            });
            
            /**
             * Ocultar label aviso
             */
            this.frmConfig.labCorrecto.setVisible(false);

            /**
             * Para que aparezca seleccionado por defecto el que se este usando
             * en config
             */
            if (Ppal.config.getMoneda() == '€') {
                frmConfig.rbtnEuro.setSelected(true);
            } else if (Ppal.config.getMoneda() == '$') {
                frmConfig.rbtnDollar.setSelected(true);
            } else if (Ppal.config.getMoneda() == '£') {
                frmConfig.rbtnLibra.setSelected(true);
            }

            if (Ppal.config.getFormFecha() == "dd/MM/yyyy") {
                frmConfig.rbtnddMMyyyy1.setSelected(true);
            } else if (Ppal.config.getFormFecha() == "yyyy/MM/dd") {
                frmConfig.rbtnyyyyMMdd1.setSelected(true);
            } else if (Ppal.config.getFormFecha() == "MM/dd/yyyy") {
                frmConfig.rbtnMMddyyyy1.setSelected(true);
            } else if (Ppal.config.getFormFecha() == "dd-MM-yyyy") {
                frmConfig.rbtnddMMyyyy2.setSelected(true);
            } else if (Ppal.config.getFormFecha() == "yyyy-MM-dd") {
                frmConfig.rbtnyyyyMMdd2.setSelected(true);
            } else if (Ppal.config.getFormFecha() == "MM-dd-yyyy") {
                frmConfig.rbtnMMddyyyy2.setSelected(true);
            }

            if (Ppal.config.getDecimales() == 2) {
                frmConfig.rbtnDos.setSelected(true);
            } else if (Ppal.config.getDecimales() == 3) {
                frmConfig.rbtnTres.setSelected(true);
            }

            /**
             * Componentes ActionListener and MouseListener
             */
            frmConfig.rbtnMetal.setActionCommand("_rbtnMetal");
            frmConfig.rbtnMetal.addActionListener(this);

            frmConfig.rbtnWindows.setActionCommand("_rbtnWindows");
            frmConfig.rbtnWindows.addActionListener(this);

            frmConfig.rbtnMotif.setActionCommand("_rbtnMotif");
            frmConfig.rbtnMotif.addActionListener(this);

            frmConfig.rbtnNimbus.setActionCommand("_rbtnNimbus");
            frmConfig.rbtnNimbus.addActionListener(this);

            frmConfig.labAplicar.setName("_labAplicar");
            frmConfig.labAplicar.addMouseListener(this);


        }

    }

    public static void comboActionPerformed(java.awt.event.ActionEvent evt) {
        System.out.println("word selected: " + ((JComboBox) combo).getSelectedItem());
        pagina.currentPageIndex = 1;
        ((miniSimpleTableModel_EmpF) frmPageref.tablaFijos.getModel()).filtrar();
    }

    public void actionPerformed(ActionEvent e) {

        switch (Accion.valueOf(e.getActionCommand())) {

            case _jComboBox1:
                pagina.itemsPerPage = Integer.parseInt(frmPageref.jComboBox1.getSelectedItem().toString());
                pagina.currentPageIndex = 1;
                pagina.initLinkBox();
                break;

            case _btnPrimero:
                pagina.currentPageIndex = 1;
                pagina.initLinkBox();
                break;
            case _btnUltimo:
                pagina.currentPageIndex = pagina.maxPageIndex;
                pagina.initLinkBox();
                break;
            case _btnSiguiente:
                pagina.currentPageIndex += 1;
                pagina.initLinkBox();
                break;
            case _btnAnterior:
                pagina.currentPageIndex -= 1;
                pagina.initLinkBox();
                break;
            case _txtNombre:
                EfGraficoBLL.pideNombre();
                break;
            case _txtApellidos:
                EfGraficoBLL.pideApellidos();
                break;
            case _txtDireccion:
                EfGraficoBLL.pideDirec();
                break;
            case _txtDNI:
                EfGraficoBLL.pideDNI();
                break;
            case _txtTlfno:
                EfGraficoBLL.pideTlfno();
                break;
            case _txtCorreo:
                EfGraficoBLL.pideCorreo();
                break;
            case _txtNombre1:
                EfGraficoBLL.cambiaNombre();
                break;
            case _txtApellidos1:
                EfGraficoBLL.cambiaApellidos();
                break;
            case _txtDireccion1:
                EfGraficoBLL.cambiaDirec();
                break;
            case _txtTlfno1:
                EfGraficoBLL.cambiaTlfno();
                break;
            case _txtCorreo1:
                EfGraficoBLL.cambiaCorreo();
                break;
            case _rbtnMetal:
                Theme.temaElegido(0);
                SwingUtilities.updateComponentTreeUI(this.frmConfig);
                break;
            case _rbtnWindows:
                Theme.temaElegido(1);
                SwingUtilities.updateComponentTreeUI(this.frmConfig);
                break;
            case _rbtnMotif:
                Theme.temaElegido(2);
                SwingUtilities.updateComponentTreeUI(this.frmConfig);
                break;
            case _rbtnNimbus:
                Theme.temaElegido(3);
                SwingUtilities.updateComponentTreeUI(this.frmConfig);
                break;

        }

    }

    @Override
    public void mouseClicked(MouseEvent me) {

        switch (Accion.valueOf(me.getComponent().getName())) {

            case _labChangeAvatar:
                FileUpload.pintar_guardar_imag(this.frmUpdateef.labPrintAvatar,90,70);
                break;
            case _labConfig:
                /**
                 * Activar config
                 */
                this.frmPageref.dispose();
                new EmpF_Controlador(new Configuration(), 3).iniciar(3);
                JPanel p3 = new JPanel();
                p3.add(this.frmConfig.pnlConfig);
                Controlador_Login.frmInicio.internalFrame.setContentPane(p3);
                break;
            case _labMaxmin:
                EmpfBLL_DB empf = new EmpfBLL_DB();
                empf.empfMenorMayorBLL();
                break;
            case _labEdadMedia:
                EmpfBLL_DB _empf = new EmpfBLL_DB();
                _empf.EdadMediaBLL();
                break;
            case _labJSON:
                EfBLLfitxers.GuardarJSON();
                break;
            case _labXML:
                EfBLLfitxers.GuardarXML();
                break;
            case _labTXT:
                EfBLLfitxers.GuardarTXT();
                break;
            case _labCreate:

                /**
                 * Activar crear
                 */
                this.frmPageref.dispose();
                new EmpF_Controlador(new CrearEF(), 1).iniciar(1);
                JPanel p1 = new JPanel();
                p1.add(this.frmCrearef.pnlEmpfCrear);
                Controlador_Login.frmInicio.internalFrame.setContentPane(p1);
                break;
            case _labUpdate:
                int correcto = EfGraficoBLL.updateEmpF();
                if (correcto != 0) {
                     String emailOld = ArraylistEF.ef.getCorreo();
                  
                    /**
                     * Activar config
                     */
                    this.frmPageref.dispose();
                    new EmpF_Controlador(new UpdateEF(), 2).iniciar(2);
                    JPanel p2 = new JPanel();
                    p2.add(this.frmUpdateef.pnlEmpfMod);
                    Controlador_Login.frmInicio.internalFrame.setContentPane(p2);
                }
                break;
            case _labDelete:
                EfGraficoBLL.deleteEmpF();
                break;
            case _txtNombre:
                if (cont[0] == 0) {
                    frmCrearef.txtNombre.setText("");
                    cont[0]++;
                }
                if (frmCrearef.labErrorNom.isVisible() == true) {
                    frmCrearef.txtNombre.setText("");
                }

                frmCrearef.labCorrecto.setVisible(false);
                break;
            case _txtApellidos:
                if (cont[1] == 0) {
                    frmCrearef.txtApellidos.setText("");
                    cont[1]++;
                }
                if (frmCrearef.labErrorApellido.isVisible() == true) {
                    frmCrearef.txtApellidos.setText("");
                }

                frmCrearef.labCorrecto.setVisible(false);
                break;
            case _txtDireccion:
                if (cont[2] == 0) {
                    frmCrearef.txtDireccion.setText("");
                    cont[2]++;
                }
                if (frmCrearef.labErrorDirec.isVisible() == true) {
                    frmCrearef.txtDireccion.setText("");
                }

                frmCrearef.labCorrecto.setVisible(false);
                break;
            case _txtDNI:
                if (cont[3] == 0) {
                    frmCrearef.txtDNI.setText("");
                    cont[3]++;
                }
                if (frmCrearef.labErrorDNI.isVisible() == true) {
                    frmCrearef.txtDNI.setText("");
                }

                frmCrearef.labCorrecto.setVisible(false);
                break;
            case _txtTlfno:
                if (cont[4] == 0) {
                    frmCrearef.txtTlfno.setText("");
                    cont[4]++;
                }
                if (frmCrearef.labErrorTelefono.isVisible() == true) {
                    frmCrearef.txtTlfno.setText("");
                }

                frmCrearef.labCorrecto.setVisible(false);
                break;
            case _txtCorreo:
                if (cont[5] == 0) {
                    frmCrearef.txtCorreo.setText("");
                    cont[5]++;
                }
                if (frmCrearef.labErrorCorreo.isVisible() == true) {
                    frmCrearef.txtCorreo.setText("");
                }

                frmCrearef.labCorrecto.setVisible(false);
                break;

            case _txtNombre1:
                if (cont[0] == 0) {
                    frmUpdateef.txtNombre.setText("");
                    cont[0]++;
                }
                if (frmUpdateef.labErrorNom.isVisible() == true) {
                    frmUpdateef.txtNombre.setText("");
                }

                frmUpdateef.labUpdate.setVisible(false);
                break;
            case _txtApellidos1:
                if (cont[1] == 0) {
                    frmUpdateef.txtApellidos.setText("");
                    cont[1]++;
                }
                if (frmUpdateef.labErrorApellido.isVisible() == true) {
                    frmUpdateef.txtApellidos.setText("");
                }

                frmUpdateef.labUpdate.setVisible(false);
                break;
            case _txtDireccion1:
                if (cont[2] == 0) {
                    frmUpdateef.txtDireccion.setText("");
                    cont[2]++;
                }
                if (frmUpdateef.labErrorDirec.isVisible() == true) {
                    frmUpdateef.txtDireccion.setText("");
                }

                frmUpdateef.labUpdate.setVisible(false);
                break;
            case _txtTlfno1:
                if (cont[4] == 0) {
                    frmUpdateef.txtTlfno.setText("");
                    cont[4]++;
                }
                if (frmUpdateef.labErrorTelefono.isVisible() == true) {
                    frmUpdateef.txtTlfno.setText("");
                }

                frmUpdateef.labUpdate.setVisible(false);
                break;
            case _txtCorreo1:
                if (cont[5] == 0) {
                    frmUpdateef.txtCorreo.setText("");
                    cont[5]++;
                }
                if (frmUpdateef.labErrorCorreo.isVisible() == true) {
                    frmUpdateef.txtCorreo.setText("");
                }

                frmUpdateef.labUpdate.setVisible(false);
                break;
            case _labAplicar:

                //activar etiqueta de aviso al cambio
                this.frmConfig.labCorrecto.setVisible(true);

                //Tipo moneda
                if (frmConfig.grupoMoneda.isSelected(frmConfig.rbtnEuro.getModel())) {
                    Ppal.config.setMoneda('€');
                } else if (frmConfig.grupoMoneda.isSelected(frmConfig.rbtnDollar.getModel())) {
                    Ppal.config.setMoneda('$');
                } else if (frmConfig.grupoMoneda.isSelected(frmConfig.rbtnLibra.getModel())) {
                    Ppal.config.setMoneda('£');
                }

                //Tipo Formato Fecha
                if (frmConfig.grupoFecha.isSelected(frmConfig.rbtnddMMyyyy1.getModel())) {
                    Ppal.config.setFormFecha("dd/MM/yyyy");
                } else if (frmConfig.grupoFecha.isSelected(frmConfig.rbtnyyyyMMdd1.getModel())) {
                    Ppal.config.setFormFecha("yyyy/MM/dd");
                } else if (frmConfig.grupoFecha.isSelected(frmConfig.rbtnMMddyyyy1.getModel())) {
                    Ppal.config.setFormFecha("MM/dd/yyyy");
                } else if (frmConfig.grupoFecha.isSelected(frmConfig.rbtnddMMyyyy2.getModel())) {
                    Ppal.config.setFormFecha("dd-MM-yyyy");
                } else if (frmConfig.grupoFecha.isSelected(frmConfig.rbtnyyyyMMdd2.getModel())) {
                    Ppal.config.setFormFecha("yyyy-MM-dd");
                } else if (frmConfig.grupoFecha.isSelected(frmConfig.rbtnMMddyyyy2.getModel())) {
                    Ppal.config.setFormFecha("MM-dd-yyyy");
                }

                //Decimales
                if (frmConfig.grupoDecimales.isSelected(frmConfig.rbtnDos.getModel())) {
                    Ppal.config.setDecimales(2);
                } else if (frmConfig.grupoDecimales.isSelected(frmConfig.rbtnTres.getModel())) {
                    Ppal.config.setDecimales(3);
                }
                this.frmConfig.labCorrecto.setVisible(true);
                break;
            case _labActualizar:
                EfGraficoBLL.cambiaEmpF();
                break;
            case _labAnadir:
                EfGraficoBLL.pideEmpF();         
                break;

        }

    }

    @Override
    public void mouseEntered(MouseEvent me) {

        switch (Accion.valueOf(me.getComponent().getName())) {

            case _labCreate:
                 ImageIcon icon = new ImageIcon("src/TPV/imag/anadir.png");
                 Image img = icon.getImage();
                 ImageIcon newIcon = new ImageIcon(img);
                frmPageref.labCreate.setIcon(newIcon);
                break;
            case _labUpdate:
                 ImageIcon icon1 = new ImageIcon("src/TPV/imag/actualizar.png");
                 Image img1 = icon1.getImage();
                 ImageIcon newIcon1 = new ImageIcon(img1);
                frmPageref.labUpdate.setIcon(newIcon1);
                break;
            case _labDelete:
                ImageIcon icon2 = new ImageIcon("src/TPV/imag/borrar.png");
                 Image img2 = icon2.getImage();
                 ImageIcon newIcon2 = new ImageIcon(img2);
                frmPageref.labDelete.setIcon(newIcon2);
                break;
            case _labConfig:
                ImageIcon icon3 = new ImageIcon("src/TPV/imag/config.png");
                 Image img3 = icon3.getImage();
                 ImageIcon newIcon3 = new ImageIcon(img3);
                frmPageref.labConfig.setIcon(newIcon3);
                break;
            case _labJSON:
                ImageIcon icon4 = new ImageIcon("src/TPV/imag/json.png");
                 Image img4 = icon4.getImage();
                 ImageIcon newIcon4 = new ImageIcon(img4);
                frmPageref.labJSON.setIcon(newIcon4);
                break;
            case _labXML:
                ImageIcon icon5 = new ImageIcon("src/TPV/imag/xml.png");
                 Image img5 = icon5.getImage();
                 ImageIcon newIcon5 = new ImageIcon(img5);
                frmPageref.labXML.setIcon(newIcon5);
                break;
            case _labTXT:
                ImageIcon icon6 = new ImageIcon("src/TPV/imag/txt.png");
                 Image img6 = icon6.getImage();
                 ImageIcon newIcon6 = new ImageIcon(img6);
                frmPageref.labTXT.setIcon(newIcon6);
                break;
            case _labAplicar:
                frmConfig.labAplicar.setText("<html><font color=green>Aplicar</font></html>");
                break;

            case _labActualizar:
                frmUpdateef.labActualizar.setText("<html><font color=green>Actualizar</font></html>");
                break;
            case _labAnadir:
                frmCrearef.labAnadir.setText("<html><font color=green>Añadir</font></html>");
                break;
            case _labEdadMedia:
                frmPageref.labEdadMedia.setText("<html><font color=green>Edad Media</font></html>");
                break;
            case _labMaxmin:
                frmPageref.labMaxMin.setText("<html><font color=green>Max/Min</font></html>");
                break;

        }

    }

    @Override
    public void mouseExited(MouseEvent me) {

        switch (Accion.valueOf(me.getComponent().getName())) {

            case _labCreate:
                 ImageIcon icon = new ImageIcon("src/TPV/imag/anadirB.png");
                 Image img = icon.getImage();
                 ImageIcon newIcon = new ImageIcon(img);
                frmPageref.labCreate.setIcon(newIcon);
                break;
            case _labUpdate:
                ImageIcon icon1 = new ImageIcon("src/TPV/imag/actualizarB.png");
                 Image img1 = icon1.getImage();
                 ImageIcon newIcon1 = new ImageIcon(img1);
                frmPageref.labUpdate.setIcon(newIcon1);
                break;
            case _labDelete:
                ImageIcon icon2 = new ImageIcon("src/TPV/imag/borrarB.png");
                 Image img2 = icon2.getImage();
                 ImageIcon newIcon2 = new ImageIcon(img2);
                frmPageref.labDelete.setIcon(newIcon2);
                break;
            case _labConfig:
                ImageIcon icon3 = new ImageIcon("src/TPV/imag/configB.png");
                 Image img3 = icon3.getImage();
                 ImageIcon newIcon3 = new ImageIcon(img3);
                frmPageref.labConfig.setIcon(newIcon3);
                break;
            case _labJSON:
                ImageIcon icon4 = new ImageIcon("src/TPV/imag/JSONB.png");
                 Image img4 = icon4.getImage();
                 ImageIcon newIcon4 = new ImageIcon(img4);
                frmPageref.labJSON.setIcon(newIcon4);
                break;
            case _labXML:
                ImageIcon icon5 = new ImageIcon("src/TPV/imag/XMLB.png");
                 Image img5 = icon5.getImage();
                 ImageIcon newIcon5 = new ImageIcon(img5);
                frmPageref.labXML.setIcon(newIcon5);
                break;
            case _labTXT:
                ImageIcon icon6 = new ImageIcon("src/TPV/imag/TXTB.png");
                 Image img6 = icon6.getImage();
                 ImageIcon newIcon6 = new ImageIcon(img6);
                frmPageref.labTXT.setIcon(newIcon6);
                break;
            case _labEdadMedia:
                frmPageref.labEdadMedia.setText("<html><font color=black>Edad Media</font></html>");
                break;
            case _labMaxmin:
                frmPageref.labMaxMin.setText("<html><font color=black>Max/Min</font></html>");
                break;
            case _labAplicar:
                frmConfig.labAplicar.setText("<html><font color=black>Aplicar</font></html>");
                break;
            case _labActualizar:
                frmUpdateef.labActualizar.setText("<html><font color=black>Actualizar</font></html>");
                break;
            case _labAnadir:
                frmCrearef.labAnadir.setText("<html><font color=black>Añadir</font></html>");
                break;
        }

    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (Accion.valueOf(e.getComponent().getName())) {

            case _txtNombre:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearef.txtApellidos.setText("");
                    frmCrearef.txtApellidos.requestFocus();
                    cont[0] = 1;
                    frmCrearef.labCorrecto.setVisible(false);
                }
                break;
            case _txtApellidos:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearef.txtDireccion.setText("");
                    frmCrearef.txtDireccion.requestFocus();
                    cont[1] = 1;
                    frmCrearef.labCorrecto.setVisible(false);
                }
                break;
            case _txtDireccion:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearef.txtDNI.setText("");
                    frmCrearef.txtDNI.requestFocus();
                    cont[2] = 1;
                    frmCrearef.labCorrecto.setVisible(false);
                }
                break;
            case _txtDNI:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearef.txtTlfno.setText("");
                    frmCrearef.txtTlfno.requestFocus();
                    cont[3] = 1;
                    frmCrearef.labCorrecto.setVisible(false);
                }
                break;
            case _txtTlfno:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearef.txtCorreo.setText("");
                    frmCrearef.txtCorreo.requestFocus();
                    cont[4] = 1;
                    frmCrearef.labCorrecto.setVisible(false);
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

        switch (Accion.valueOf(e.getComponent().getName())) {

            case _txtNombre:
                EfGraficoBLL.pideNombre();
                break;
            case _txtApellidos:
                EfGraficoBLL.pideApellidos();
                break;
            case _txtDireccion:
                EfGraficoBLL.pideDirec();
                break;
            case _txtDNI:
                EfGraficoBLL.pideDNI();
                break;
            case _txtTlfno:
                EfGraficoBLL.pideTlfno();
                break;
            case _txtCorreo:
                EfGraficoBLL.pideCorreo();
                break;
            case _txtNombre1:
                EfGraficoBLL.cambiaNombre();
                break;
            case _txtApellidos1:
                EfGraficoBLL.cambiaApellidos();
                break;
            case _txtDireccion1:
                EfGraficoBLL.cambiaDirec();
                break;
            case _txtTlfno1:
                EfGraficoBLL.cambiaTlfno();
                break;
            case _txtCorreo1:
                EfGraficoBLL.cambiaCorreo();
                break;

        }
    }

}
