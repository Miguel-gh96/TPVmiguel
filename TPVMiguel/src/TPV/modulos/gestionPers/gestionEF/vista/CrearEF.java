/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.vista;


import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EfGraficoBLL;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

/**
 *
 * @author miguel
 */
public class CrearEF extends javax.swing.JFrame {
    
    /**
     * Creates new form EfDAOagreg
     */
    public CrearEF() {
        initComponents();    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoSexo = new javax.swing.ButtonGroup();
        pnlEmpfCrear = new javax.swing.JPanel();
        labTitulo = new javax.swing.JLabel();
        labNombre = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtApellidos = new javax.swing.JTextField();
        labDireccion = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        labDNI = new javax.swing.JLabel();
        txtDNI = new javax.swing.JTextField();
        labFechanac = new javax.swing.JLabel();
        labTlfno = new javax.swing.JLabel();
        txtTlfno = new javax.swing.JTextField();
        labCorreo = new javax.swing.JLabel();
        txtCorreo = new javax.swing.JTextField();
        labFechaContrat = new javax.swing.JLabel();
        labErrorNom = new javax.swing.JLabel();
        labErrorDirec = new javax.swing.JLabel();
        labErrorDNI = new javax.swing.JLabel();
        labErrorFechanac = new javax.swing.JLabel();
        labErrorTelefono = new javax.swing.JLabel();
        labErrorCorreo = new javax.swing.JLabel();
        labErrorFechaCont = new javax.swing.JLabel();
        labErrorApellido = new javax.swing.JLabel();
        labCorrecto = new javax.swing.JLabel();
        dateFechanac = new com.toedter.calendar.JDateChooser();
        dateFechacont = new com.toedter.calendar.JDateChooser();
        labAnadir = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlEmpfCrear.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        labTitulo.setText("EMPLEADOS FIJOS");
        pnlEmpfCrear.add(labTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 12, -1, -1));

        labNombre.setText("Nombre");
        pnlEmpfCrear.add(labNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 47, 55, -1));

        txtNombre.setText("Nombre");
        pnlEmpfCrear.add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 69, 93, 28));

        txtApellidos.setText("Apellidos");
        pnlEmpfCrear.add(txtApellidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(136, 69, 157, 28));

        labDireccion.setText("Dirección");
        pnlEmpfCrear.add(labDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 147, -1, -1));

        txtDireccion.setText("Calle/Piso/Nº/Pta");
        pnlEmpfCrear.add(txtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 169, 256, 28));

        labDNI.setText("DNI");
        pnlEmpfCrear.add(labDNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 231, -1, -1));

        txtDNI.setText("00000000P");
        pnlEmpfCrear.add(txtDNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 253, 256, 28));

        labFechanac.setText("Fecha de nacimiento");
        pnlEmpfCrear.add(labFechanac, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 315, -1, -1));

        labTlfno.setText("Teléfono");
        pnlEmpfCrear.add(labTlfno, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 50, -1, -1));
        pnlEmpfCrear.add(txtTlfno, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 70, 256, 28));

        labCorreo.setText("Correo");
        pnlEmpfCrear.add(labCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 150, -1, -1));
        pnlEmpfCrear.add(txtCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 170, 256, 28));

        labFechaContrat.setText("Fecha de contratación");
        pnlEmpfCrear.add(labFechaContrat, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 230, -1, -1));

        labErrorNom.setForeground(new java.awt.Color(255, 0, 0));
        labErrorNom.setText("El campo  es incorrecto.");
        pnlEmpfCrear.add(labErrorNom, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 103, -1, -1));

        labErrorDirec.setForeground(new java.awt.Color(255, 0, 0));
        labErrorDirec.setText("El campo  es incorrecto.");
        pnlEmpfCrear.add(labErrorDirec, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 203, -1, -1));

        labErrorDNI.setForeground(new java.awt.Color(255, 0, 0));
        labErrorDNI.setText("El campo  es incorrecto.");
        pnlEmpfCrear.add(labErrorDNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 287, -1, -1));

        labErrorFechanac.setForeground(new java.awt.Color(255, 0, 0));
        labErrorFechanac.setText("El campo  es incorrecto.");
        pnlEmpfCrear.add(labErrorFechanac, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 371, -1, -1));

        labErrorTelefono.setForeground(new java.awt.Color(255, 0, 0));
        labErrorTelefono.setText("El campo  es incorrecto.");
        pnlEmpfCrear.add(labErrorTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 100, -1, -1));

        labErrorCorreo.setForeground(new java.awt.Color(255, 0, 0));
        labErrorCorreo.setText("El campo  es incorrecto.");
        pnlEmpfCrear.add(labErrorCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 200, -1, -1));

        labErrorFechaCont.setForeground(new java.awt.Color(255, 0, 0));
        labErrorFechaCont.setText("El campo  es incorrecto.");
        pnlEmpfCrear.add(labErrorFechaCont, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 290, -1, -1));

        labErrorApellido.setForeground(new java.awt.Color(255, 0, 0));
        labErrorApellido.setText("El campo  es incorrecto.");
        pnlEmpfCrear.add(labErrorApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 125, -1, -1));

        labCorrecto.setForeground(java.awt.Color.green);
        labCorrecto.setText("Empleado añadido correctamente!");
        pnlEmpfCrear.add(labCorrecto, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 420, -1, 20));

        dateFechanac.setDateFormatString("dd/MM/yyyy");
        pnlEmpfCrear.add(dateFechanac, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 340, 250, 20));

        dateFechacont.setDateFormatString("dd/MM/yyyy");
        pnlEmpfCrear.add(dateFechacont, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 250, 250, 30));

        labAnadir.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        labAnadir.setText("Añadir");
        pnlEmpfCrear.add(labAnadir, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 420, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(pnlEmpfCrear, javax.swing.GroupLayout.PREFERRED_SIZE, 980, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(171, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(pnlEmpfCrear, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(53, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static com.toedter.calendar.JDateChooser dateFechacont;
    public static com.toedter.calendar.JDateChooser dateFechanac;
    private javax.swing.ButtonGroup grupoSexo;
    public static javax.swing.JLabel labAnadir;
    public static javax.swing.JLabel labCorrecto;
    private javax.swing.JLabel labCorreo;
    private javax.swing.JLabel labDNI;
    private javax.swing.JLabel labDireccion;
    public static javax.swing.JLabel labErrorApellido;
    public static javax.swing.JLabel labErrorCorreo;
    public static javax.swing.JLabel labErrorDNI;
    public static javax.swing.JLabel labErrorDirec;
    public static javax.swing.JLabel labErrorFechaCont;
    public static javax.swing.JLabel labErrorFechanac;
    public static javax.swing.JLabel labErrorNom;
    public static javax.swing.JLabel labErrorTelefono;
    private javax.swing.JLabel labFechaContrat;
    private javax.swing.JLabel labFechanac;
    private javax.swing.JLabel labNombre;
    private javax.swing.JLabel labTitulo;
    private javax.swing.JLabel labTlfno;
    public static javax.swing.JPanel pnlEmpfCrear;
    public static javax.swing.JTextField txtApellidos;
    public static javax.swing.JTextField txtCorreo;
    public static javax.swing.JTextField txtDNI;
    public static javax.swing.JTextField txtDireccion;
    public static javax.swing.JTextField txtNombre;
    public static javax.swing.JTextField txtTlfno;
    // End of variables declaration//GEN-END:variables
}
