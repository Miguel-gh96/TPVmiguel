/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionEF.vista;


/**
 *
 * @author miguel
 */
public class Configuration extends javax.swing.JFrame {

    /**
     * Creates new form Configuration
     */
    public Configuration() {
        initComponents();
                    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoMoneda = new javax.swing.ButtonGroup();
        grupoFecha = new javax.swing.ButtonGroup();
        grupoLookAFeel = new javax.swing.ButtonGroup();
        grupoDecimales = new javax.swing.ButtonGroup();
        grupoLibreria = new javax.swing.ButtonGroup();
        pnlConfig = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        rbtnEuro = new javax.swing.JRadioButton();
        rbtnDollar = new javax.swing.JRadioButton();
        rbtnLibra = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        rbtnddMMyyyy1 = new javax.swing.JRadioButton();
        rbtnyyyyMMdd1 = new javax.swing.JRadioButton();
        rbtnMMddyyyy1 = new javax.swing.JRadioButton();
        rbtnddMMyyyy2 = new javax.swing.JRadioButton();
        rbtnyyyyMMdd2 = new javax.swing.JRadioButton();
        rbtnMMddyyyy2 = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        rbtnMetal = new javax.swing.JRadioButton();
        rbtnWindows = new javax.swing.JRadioButton();
        rbtnMotif = new javax.swing.JRadioButton();
        rbtnNimbus = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        rbtnDos = new javax.swing.JRadioButton();
        rbtnTres = new javax.swing.JRadioButton();
        labAplicar = new javax.swing.JLabel();
        labCorrecto = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlConfig.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Configuración");
        pnlConfig.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 27, 289, 32));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Tipo de moneda:");
        pnlConfig.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 117, -1, -1));

        grupoMoneda.add(rbtnEuro);
        rbtnEuro.setText("€");
        pnlConfig.add(rbtnEuro, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 138, -1, -1));

        grupoMoneda.add(rbtnDollar);
        rbtnDollar.setText("$");
        pnlConfig.add(rbtnDollar, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 140, -1, -1));

        grupoMoneda.add(rbtnLibra);
        rbtnLibra.setText("£");
        pnlConfig.add(rbtnLibra, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 140, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Formato Fecha:");
        pnlConfig.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(52, 179, -1, -1));

        grupoFecha.add(rbtnddMMyyyy1);
        rbtnddMMyyyy1.setText("dd/MM/yyyy");
        pnlConfig.add(rbtnddMMyyyy1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 210, -1, -1));

        grupoFecha.add(rbtnyyyyMMdd1);
        rbtnyyyyMMdd1.setText("yyyy/MM/dd");
        pnlConfig.add(rbtnyyyyMMdd1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 270, -1, -1));

        grupoFecha.add(rbtnMMddyyyy1);
        rbtnMMddyyyy1.setText("MM/dd/yyyy");
        pnlConfig.add(rbtnMMddyyyy1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 290, -1, -1));

        grupoFecha.add(rbtnddMMyyyy2);
        rbtnddMMyyyy2.setText("dd-MM-yyyy");
        pnlConfig.add(rbtnddMMyyyy2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 230, -1, -1));

        grupoFecha.add(rbtnyyyyMMdd2);
        rbtnyyyyMMdd2.setText("yyyy-MM-dd");
        pnlConfig.add(rbtnyyyyMMdd2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 250, -1, -1));

        grupoFecha.add(rbtnMMddyyyy2);
        rbtnMMddyyyy2.setText("MM-dd-yyyy");
        pnlConfig.add(rbtnMMddyyyy2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 310, -1, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Look&Feel");
        pnlConfig.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 179, -1, -1));

        grupoLookAFeel.add(rbtnMetal);
        rbtnMetal.setText("Metal");
        pnlConfig.add(rbtnMetal, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 200, -1, -1));

        grupoLookAFeel.add(rbtnWindows);
        rbtnWindows.setText("Windows");
        pnlConfig.add(rbtnWindows, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 200, -1, -1));

        grupoLookAFeel.add(rbtnMotif);
        rbtnMotif.setText("Motif");
        pnlConfig.add(rbtnMotif, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 230, -1, -1));

        grupoLookAFeel.add(rbtnNimbus);
        rbtnNimbus.setText("Nimbus");
        pnlConfig.add(rbtnNimbus, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 226, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("Número de decimales:");
        pnlConfig.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 117, -1, -1));

        grupoDecimales.add(rbtnDos);
        rbtnDos.setText("dos");
        pnlConfig.add(rbtnDos, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 138, -1, -1));

        grupoDecimales.add(rbtnTres);
        rbtnTres.setText("tres");
        pnlConfig.add(rbtnTres, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 140, -1, -1));

        labAplicar.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        labAplicar.setText("Aplicar");
        pnlConfig.add(labAplicar, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 330, -1, -1));

        labCorrecto.setForeground(java.awt.Color.green);
        labCorrecto.setText("Cambios aplicados correctamente");
        pnlConfig.add(labCorrecto, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 330, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlConfig, javax.swing.GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlConfig, javax.swing.GroupLayout.DEFAULT_SIZE, 490, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.ButtonGroup grupoDecimales;
    public static javax.swing.ButtonGroup grupoFecha;
    public static javax.swing.ButtonGroup grupoLibreria;
    public static javax.swing.ButtonGroup grupoLookAFeel;
    public static javax.swing.ButtonGroup grupoMoneda;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    public static javax.swing.JLabel labAplicar;
    public static javax.swing.JLabel labCorrecto;
    public static javax.swing.JPanel pnlConfig;
    public static javax.swing.JRadioButton rbtnDollar;
    public static javax.swing.JRadioButton rbtnDos;
    public static javax.swing.JRadioButton rbtnEuro;
    public static javax.swing.JRadioButton rbtnLibra;
    public static javax.swing.JRadioButton rbtnMMddyyyy1;
    public static javax.swing.JRadioButton rbtnMMddyyyy2;
    public static javax.swing.JRadioButton rbtnMetal;
    public static javax.swing.JRadioButton rbtnMotif;
    public static javax.swing.JRadioButton rbtnNimbus;
    public static javax.swing.JRadioButton rbtnTres;
    public static javax.swing.JRadioButton rbtnWindows;
    public static javax.swing.JRadioButton rbtnddMMyyyy1;
    public static javax.swing.JRadioButton rbtnddMMyyyy2;
    public static javax.swing.JRadioButton rbtnyyyyMMdd1;
    public static javax.swing.JRadioButton rbtnyyyyMMdd2;
    // End of variables declaration//GEN-END:variables
}
