/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionUser.modelo.DAO;

import static TPV.Ppal.config;
import TPV.clases.Fecha;
import TPV.librerias.FileUpload;
import TPV.librerias.Validate;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.SingletonUser;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.Usuario;
import TPV.modulos.gestionPers.gestionUser.vista.CrearUser;
import TPV.modulos.gestionPers.gestionUser.vista.UpdateUser;
import com.toedter.calendar.JTextFieldDateEditor;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class DAO_User {
    
    public static void pideNombre() {

        if (CrearUser.txtNombre.getText().isEmpty()) {
            CrearUser.labErrorNom.setText("El campo del nombre no puede estar vacio ");
            CrearUser.labErrorNom.setVisible(true);
            return;
        } else {
            CrearUser.labErrorNom.setVisible(false);

            if (Validate.palabraSoloLet(CrearUser.txtNombre.getText()) == false) {
                CrearUser.labErrorNom.setText("El campo del nombre solo puede tener letras");
                CrearUser.labErrorNom.setVisible(true);
                return;
            } else {
                CrearUser.labErrorNom.setVisible(false);
            }
        }

    }
    
    public static void pideUserName() {

        if (CrearUser.txtUser.getText().isEmpty()) {
            CrearUser.labErrorUsuario.setText("<html>El campo de usuario no puede <br/>estar vacio</html>");
            CrearUser.labErrorUsuario.setVisible(true);
            return;

        } else {
            CrearUser.labErrorUsuario.setVisible(false);
            
            if(Validate.usuario(CrearUser.txtUser.getText()) == false){
                CrearUser.labErrorUsuario.setText("<html>El campo debe  <br/> contener letras y números</html>");
                CrearUser.labErrorUsuario.setVisible(true);
                return;
            }else
                CrearUser.labErrorUsuario.setVisible(false);
            
        }

    }
    
    public static void pidePass() {

        if (CrearUser.txtPass.getText().isEmpty()) {
            CrearUser.labErrorPass.setText("<html>El campo de la contraseña no <br/> puede estar vacio</html>");
            CrearUser.labErrorPass.setVisible(true);
            return;

        } else {
            CrearUser.labErrorPass.setVisible(false);
            
            if(Validate.pass(CrearUser.txtPass.getText()) == false){
                CrearUser.labErrorPass.setText("<html>El campo de la contraseña solo puede<br/> contener letras y numeros tiene que<br/> tener entre 8 y 10 </html>");
                CrearUser.labErrorPass.setVisible(true);
                return;            
            }else
                CrearUser.labErrorPass.setVisible(false);
            
        }

    }
    
    public static void pideConfirmPass() {

        if (CrearUser.txtConfirmPass.getText().isEmpty()) {
            CrearUser.labErrorConfirmPass.setText("<html>El campo de la contraseña<br/> no puede estar vacio</html>");
            CrearUser.labErrorConfirmPass.setVisible(true);
            return;

        } else {
            CrearUser.labErrorConfirmPass.setVisible(false);
            if(!(CrearUser.txtConfirmPass.getText().equals(CrearUser.txtPass.getText()))){
                CrearUser.labErrorConfirmPass.setText("Las contraseñas no coinciden");
                CrearUser.labErrorConfirmPass.setVisible(true);
                return;            
            }else
                CrearUser.labErrorConfirmPass.setVisible(false);
            
        }

    }
    

    public static void pideApellidos() {

        if (CrearUser.txtApellidos.getText().isEmpty()) {
            CrearUser.labErrorApellido.setText("El campo de los apellidos no puede estar vacio");
            CrearUser.labErrorApellido.setVisible(true);
            return;

        } else {
            CrearUser.labErrorApellido.setVisible(false);

            if (Validate.palabraSoloLet(CrearUser.txtApellidos.getText()) == false) {
                CrearUser.labErrorApellido.setText("El campo de los apellidos solo puede tener letras");
                CrearUser.labErrorApellido.setVisible(true);
                return;
            } else {
                CrearUser.labErrorApellido.setVisible(false);
            }
        }

    }

    public static void pideDireccion() {

        if (CrearUser.txtDireccion.getText().isEmpty()) {
            CrearUser.labErrorDirec.setText("El campo de la dirección no puede estar vacio");
            CrearUser.labErrorDirec.setVisible(true);
            return;
        } else {
            CrearUser.labErrorDirec.setVisible(false);
        }

    }

    public static void pideDNI() {

        if (CrearUser.txtDNI.getText().isEmpty()) {
            CrearUser.labErrorDNI.setText("El campo del DNI no puede estar vacio");
            CrearUser.labErrorDNI.setVisible(true);
            return;
        } else {
            CrearUser.labErrorDNI.setVisible(false);

            if (Validate.niformat(CrearUser.txtDNI.getText()) == false) {
                CrearUser.labErrorDNI.setText("No ha introducido correctamente el NIF");
                CrearUser.labErrorDNI.setVisible(true);
                return;
            } else {
                CrearUser.labErrorDNI.setVisible(false);
                if (Validate.nifcompruebaletra(CrearUser.txtDNI.getText()) == false) {
                    CrearUser.labErrorDNI.setText("Los numeros introducidos no se corresponden con la letra");
                    CrearUser.labErrorDNI.setVisible(true);
                    return;
                } else {
                    CrearUser.labErrorDNI.setVisible(false);
                }

            }

        }

    }

    public static void pideFechanac() {

        String date1;
        Fecha fechanac;

        if (CrearUser.dateFechanac.getDate() == null) {
            CrearUser.labErrorFechanac.setText("El campo fecha de nacimiento no puede estar vacio");
            CrearUser.labErrorFechanac.setVisible(true);
            return;
        } else {
            CrearUser.labErrorFechanac.setVisible(false);
            date1 = ((JTextFieldDateEditor) CrearUser.dateFechanac.getDateEditor()).getText();

            if (Validate.DateFormat(date1) == false) {
                CrearUser.labErrorFechanac.setText("El formato de la fecha introducida no es el correcto." + "(" + config.getFormFecha() + ")");
                CrearUser.labErrorFechanac.setVisible(true);
                return;
            } else {
                CrearUser.labErrorFechanac.setVisible(false);
                fechanac = new Fecha(date1);
                if (fechanac.compruebafecha() == false) {
                    CrearUser.labErrorFechanac.setText("La fecha introducida no es válida");
                    CrearUser.labErrorFechanac.setVisible(true);
                    return;
                } else {
                    CrearUser.labErrorFechanac.setVisible(false);
                    if (fechanac.calcularedad() < 16) {
                        CrearUser.labErrorFechanac.setText("No pueden haber empleados menores de 16 años");
                        CrearUser.labErrorFechanac.setVisible(true);
                        return;
                    } else {
                        CrearUser.labErrorFechanac.setVisible(false);
                    }

                }

            }

        }

    }

    public static void pideTlfno() {
        if (CrearUser.txtTlfno.getText().isEmpty()) {
            CrearUser.labErrorTelefono.setText("El campo del telefono no puede estar vacio");
            CrearUser.labErrorTelefono.setVisible(true);
            return;
        } else {
            CrearUser.labErrorTelefono.setVisible(false);

            if (Validate.phone(CrearUser.txtTlfno.getText()) == false) {
                CrearUser.labErrorTelefono.setText("No ha introducido un número correcto");
                CrearUser.labErrorTelefono.setVisible(true);
                return;
            } else {
                CrearUser.labErrorTelefono.setVisible(false);
            }
        }

    }

    public static void pideCorreo() {
        if (CrearUser.txtCorreo.getText().isEmpty()) {
            CrearUser.labErrorCorreo.setText("El campo del correo no puede estar vacio");
            CrearUser.labErrorCorreo.setVisible(true);
            return;
        } else {
            CrearUser.labErrorCorreo.setVisible(false);

            if (Validate.Email(CrearUser.txtCorreo.getText()) == false) {
                CrearUser.labErrorCorreo.setText("No ha introducido correctamente el correo");
                CrearUser.labErrorCorreo.setVisible(true);
                CrearUser.txtCorreo.requestFocus();
                return;
            } else {
                CrearUser.labErrorCorreo.setVisible(false);
            }
        }

    }
    
    
    //CrearUser
    public static void pideUser() {
        String nombre, apellidos, direccion, nif, tlfno, correo, date1, date2,user,pass;
        Fecha fechanac;

        if (CrearUser.labErrorNom.isShowing() == false && CrearUser.labErrorApellido.isShowing() == false
                && CrearUser.labErrorDirec.isShowing() == false && CrearUser.labErrorDNI.isShowing() == false
                && CrearUser.labErrorFechanac.isShowing() == false && CrearUser.labErrorTelefono.isShowing() == false
                && CrearUser.labErrorCorreo.isShowing() == false&& CrearUser.labErrorUsuario.isShowing() == false 
                && CrearUser.labErrorPass.isShowing() == false && CrearUser.labErrorConfirmPass.isShowing() == false) {

            nombre = CrearUser.txtNombre.getText();
            apellidos = CrearUser.txtApellidos.getText();
            direccion = CrearUser.txtDireccion.getText();
            nif = CrearUser.txtDNI.getText();
            date1 = ((JTextFieldDateEditor) CrearUser.dateFechanac.getDateEditor()).getText();
            fechanac = new Fecha(date1);
            tlfno = CrearUser.txtTlfno.getText();
            correo = CrearUser.txtCorreo.getText();
            user = CrearUser.txtUser.getText();
            pass = CrearUser.txtPass.getText();
            

           SingletonUser.usr = new Usuario(nombre, apellidos, direccion, nif, fechanac, tlfno, correo,user,pass);
           SingletonUser.usr.setAvatar("src/TPV/imag/default-avatar.jpg");
           SingletonUser.usr.setTipo(CrearUser.ComboxTipo.getSelectedItem().toString());

        }
    }
    
    //cambiarUser
     public static void cambiaUser(Usuario o) {
        Fecha fechanac = null, fechacont = null;
        String date1, date2;

        if (UpdateUser.labErrorNom.isShowing() == false && UpdateUser.labErrorApellido.isShowing() == false
                && UpdateUser.labErrorDNI.isShowing() == false && UpdateUser.labErrorFechanac.isShowing() == false
                && UpdateUser.labErrorTelefono.isShowing() == false && UpdateUser.labErrorCorreo.isShowing() == false
                && UpdateUser.labErrorUsuario.isShowing() == false  && UpdateUser.labErrorPass.isShowing() == false 
                && UpdateUser.labErrorConfirmPass.isShowing() == false) {

            

            o.setNombre(UpdateUser.txtNombre.getText());
            o.setApellidos(UpdateUser.txtApellidos.getText());
            o.setDireccion(UpdateUser.txtDireccion.getText());
            o.setTlfno(UpdateUser.txtTlfno.getText());
            o.setCorreo(UpdateUser.txtCorreo.getText());          
            o.setLogin(UpdateUser.txtUser.getText());
            o.setPassword(UpdateUser.txtPass.getText());
            date1 = ((JTextFieldDateEditor) UpdateUser.dateFechanac.getDateEditor()).getText();
            fechanac = new Fecha(date1);
            o.setFechanac(fechanac);
            o.cambiaredad(o.getFechanac());
            o.setAvatar(FileUpload.PATH_auto);
            o.setTipo(UpdateUser.ComboxTipo.getSelectedItem().toString());
            

        }

    }
    
     public static void cambiaNombre() {

        if (UpdateUser.txtNombre.getText().isEmpty()) {
            UpdateUser.labErrorNom.setText("<html>El campo de la contraseña no <br/> puede estar vacio</html>"    );
            UpdateUser.labErrorNom.setVisible(true);
            return;
        } else {
            UpdateUser.labErrorNom.setVisible(false);

            if (Validate.palabraSoloLet(UpdateUser.txtNombre.getText()) == false) {
                UpdateUser.labErrorNom.setText("El campo del nombre solo puede tener letras");
                UpdateUser.labErrorNom.setVisible(true);
                return;
            } else {
                UpdateUser.labErrorNom.setVisible(false);
            }
        }

    }
    
    public static void CambiaUserName() {

        if (UpdateUser.txtUser.getText().isEmpty()) {
            UpdateUser.labErrorUsuario.setText("El campo de usuario no puede estar vacio");
            UpdateUser.labErrorUsuario.setVisible(true);
            return;

        } else {
            UpdateUser.labErrorUsuario.setVisible(false);
            
            if(Validate.usuario(UpdateUser.txtUser.getText()) == false){
                UpdateUser.labErrorUsuario.setText("El campo debe contener letras y números");
                UpdateUser.labErrorUsuario.setVisible(true);
                return;
            }else
                UpdateUser.labErrorUsuario.setVisible(false);
            
        }

    }
    
    public static void CambiaPass() {

        if (UpdateUser.txtPass.getText().isEmpty()) {
            UpdateUser.labErrorPass.setText("El campo de la contraseña no puede estar vacio");
            UpdateUser.labErrorPass.setVisible(true);
            return;

        } else {
            UpdateUser.labErrorPass.setVisible(false);
            
            if(Validate.pass(UpdateUser.txtPass.getText()) == false){
                UpdateUser.labErrorPass.setText("<html>El campo de la contraseña solo puede<br/> contener letras y numeros tiene que<br/> tener entre 8 y 10 </html>");
                UpdateUser.labErrorPass.setVisible(true);
                return;            
            }else
                UpdateUser.labErrorPass.setVisible(false);
            
        }

    }
    
    public static void CambiaConfirmPass() {

        if (UpdateUser.txtConfirmPass.getText().isEmpty()) {
            UpdateUser.labErrorConfirmPass.setText("El campo de la contraseña no puede estar vacio");
            UpdateUser.labErrorConfirmPass.setVisible(true);
            return;

        } else {
            UpdateUser.labErrorConfirmPass.setVisible(false);
            
            if(!(UpdateUser.txtConfirmPass.getText().equals(UpdateUser.txtPass.getText()))){
                UpdateUser.labErrorConfirmPass.setText("Las contraseñas no coinciden");
                UpdateUser.labErrorConfirmPass.setVisible(true);
                return;            
            }else
                UpdateUser.labErrorConfirmPass.setVisible(false);
            
        }

    }
    

    public static void cambiaApellidos() {

        if (UpdateUser.txtApellidos.getText().isEmpty()) {
            UpdateUser.labErrorApellido.setText("El campo de los apellidos no puede estar vacio");
            UpdateUser.labErrorApellido.setVisible(true);
            return;

        } else {
            UpdateUser.labErrorApellido.setVisible(false);

            if (Validate.palabraSoloLet(UpdateUser.txtApellidos.getText()) == false) {
                UpdateUser.labErrorApellido.setText("El campo de los apellidos solo puede tener letras");
                UpdateUser.labErrorApellido.setVisible(true);
                return;
            } else {
                UpdateUser.labErrorApellido.setVisible(false);
            }
        }

    }

    public static void cambiaDireccion() {

        if (UpdateUser.txtDireccion.getText().isEmpty()) {
            UpdateUser.labErrorDNI.setText("El campo de la dirección no puede estar vacio");
            UpdateUser.labErrorDNI.setVisible(true);
            return;
        } else {
            UpdateUser.labErrorDNI.setVisible(false);
        }

    }

    public static void cambiaFechanac() {

        String date1;
        Fecha fechanac;

        if (UpdateUser.dateFechanac.getDate() == null) {
            UpdateUser.labErrorFechanac.setText("El campo fecha de nacimiento no puede estar vacio");
            UpdateUser.labErrorFechanac.setVisible(true);
            return;
        } else {
            UpdateUser.labErrorFechanac.setVisible(false);
            date1 = ((JTextFieldDateEditor) UpdateUser.dateFechanac.getDateEditor()).getText();

            fechanac = new Fecha(date1);
            if (fechanac.compruebafecha() == false) {
                UpdateUser.labErrorFechanac.setText("La fecha introducida no es válida");
                UpdateUser.labErrorFechanac.setVisible(true);
                return;
            } else {
                UpdateUser.labErrorFechanac.setVisible(false);
                if (fechanac.calcularedad() < 16) {
                    UpdateUser.labErrorFechanac.setText("No pueden haber empleados menores de 16 años");
                    UpdateUser.labErrorFechanac.setVisible(true);
                    return;
                } else {
                    UpdateUser.labErrorFechanac.setVisible(false);
                }

            }

        }

    }

    public static void cambiaTlfno() {
        if (UpdateUser.txtTlfno.getText().isEmpty()) {
            UpdateUser.labErrorTelefono.setText("El campo del telefono no puede estar vacio");
            UpdateUser.labErrorTelefono.setVisible(true);
            return;
        } else {
            UpdateUser.labErrorTelefono.setVisible(false);

            if (Validate.phone(UpdateUser.txtTlfno.getText()) == false) {
                UpdateUser.labErrorTelefono.setText("No ha introducido un número correcto");
                UpdateUser.labErrorTelefono.setVisible(true);
                return;
            } else {
                UpdateUser.labErrorTelefono.setVisible(false);
            }
        }

    }

    public static void cambiaCorreo() {
        if (UpdateUser.txtCorreo.getText().isEmpty()) {
            UpdateUser.labErrorCorreo.setText("El campo del correo no puede estar vacio");
            UpdateUser.labErrorCorreo.setVisible(true);
            return;
        } else {
            UpdateUser.labErrorCorreo.setVisible(false);

            if (Validate.Email(UpdateUser.txtCorreo.getText()) == false) {
                UpdateUser.labErrorCorreo.setText("No ha introducido correctamente el correo");
                UpdateUser.labErrorCorreo.setVisible(true);
                return;
            } else {
                UpdateUser.labErrorCorreo.setVisible(false);
            }
        }

    }
    
    
}
