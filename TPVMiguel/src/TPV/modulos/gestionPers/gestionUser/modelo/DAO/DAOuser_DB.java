/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionUser.modelo.DAO;

import TPV.clases.Fecha;
import TPV.librerias.Encriptar;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.SingletonUser;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.Usuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class DAOuser_DB {
    
    public void listAlluserDAO(Connection con) {
        
        ResultSet rs = null;
        PreparedStatement stmt = null;
        Fecha fechanac;

        //SingletonUser.user.clear();
        try {
            //modificar consulta
            
            stmt = con.prepareStatement("SELECT * FROM TPV.User");   
            
            rs = stmt.executeQuery();
            
            Usuario user = null;
           

            while (rs.next()) {
                

                user = new Usuario();
                user.setNombre(rs.getString("nombre"));
                
                user.setApellidos(rs.getString("apellidos"));
                
                user.setDireccion(rs.getString("direccion"));
                
                user.setNif(rs.getString("nif"));
                
                fechanac = new Fecha(rs.getString("fecha_nacimiento"),1);
                
                user.setFechanac(fechanac);
                
                user.cambiaredad(fechanac);
                
                user.setTlfno(rs.getString("telefono"));
                
                user.setCorreo(rs.getString("correo"));
                
                user.setLogin(rs.getString("login"));
                
                user.setPassword(rs.getString("password"));
                user.setAvatar(rs.getString("avatar"));
                
                user.setTipo(rs.getString("tipo"));
                user.setEstado(rs.getInt("estado"));
                SingletonUser.user.add(user);
                

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un problema al obtener los empleados fijos!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }

    }
    
    
    public int nuevoEmpfDAO(Connection con) {
        PreparedStatement stmt = null;
        int resultado=0;
        try {
            
            stmt = con.prepareStatement("INSERT INTO TPV.User"
                    + "(nombre,apellidos,direccion,nif,fecha_nacimiento,telefono"
                    + ",correo,edad,login,password,avatar,tipo,estado) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
            stmt.setString(1, SingletonUser.usr.getNombre());
            stmt.setString(2, SingletonUser.usr.getApellidos());
            stmt.setString(3, SingletonUser.usr.getDireccion());
            stmt.setString(4, SingletonUser.usr.getNif());
            stmt.setString(5, SingletonUser.usr.getFechanac().toString());
            stmt.setString(6, SingletonUser.usr.getTlfno());
            stmt.setString(7, SingletonUser.usr.getCorreo());
            stmt.setInt(8, SingletonUser.usr.getEdad());
            stmt.setString(9, SingletonUser.usr.getLogin());
            stmt.setString(10,Encriptar.encriptarTokenMD5(SingletonUser.usr.getPassword()));
            stmt.setString(11,SingletonUser.usr.getAvatar());
            stmt.setString(12, SingletonUser.usr.getTipo());
            stmt.setInt(13, SingletonUser.usr.getEstado());
            //JOptionPane.showMessageDialog(null,SingletonUser.usr.toString());
            resultado=stmt.executeUpdate();
            
            
           
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un problema al insertar un nuevo empleado fijo!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }
        return resultado;
    }
    
    
    public int modificarUsuarioDAO(Connection con) {

        PreparedStatement stmt = null;
        int resultado=0;
  
        try {
            //modificar consulta
            stmt = con.prepareStatement("UPDATE User SET nombre=?,apellidos=?,direccion=?,fecha_nacimiento=?,telefono=?"
                    +",correo=?,edad=?,login=?,password=?,avatar=?,tipo=?,estado=? WHERE nif=?");
            stmt.setString(1, SingletonUser.usr.getNombre());
            stmt.setString(2, SingletonUser.usr.getApellidos());
            stmt.setString(3, SingletonUser.usr.getDireccion());
            stmt.setString(4, SingletonUser.usr.getFechanac().toString());
            stmt.setString(5, SingletonUser.usr.getTlfno());
            stmt.setString(6, SingletonUser.usr.getCorreo());
            stmt.setInt(7, SingletonUser.usr.getEdad());
            stmt.setString(8, SingletonUser.usr.getLogin());
            stmt.setString(9,Encriptar.encriptarTokenMD5(SingletonUser.usr.getPassword()));   
            stmt.setString(10, SingletonUser.usr.getAvatar());
            stmt.setString(11, SingletonUser.usr.getTipo());
            stmt.setInt(12, SingletonUser.usr.getEstado());
            stmt.setString(13, SingletonUser.usr.getNif());

            resultado = stmt.executeUpdate();
            
            


        } catch (SQLException ex) {
            //throw new Exception("Ha habido un problema al insertar la factura "+ex.getMessage());
            //JOptionPane.showMessageDialog(null, "Ha habido un problema al actualizar el empleado fijo!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    //Logger.getLogger(alumnoPresencialDAO.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }
        return resultado;

    }
    
    public int borrarEmpleadofijoDAO(Connection con) {
        int resultado=0;
        PreparedStatement stmt = null;

        try {
            //modificar consulta
            stmt = con.prepareStatement("DELETE FROM User WHERE nif=?");
            stmt.setString(1, SingletonUser.usr.getNif());
            resultado = stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un error al eliminar el empleado fijo!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en el Logger!");
                }
            }
        }
        return resultado;
    }
    
    
    public void userMenorMayorDAO(Connection conexion) {

        CallableStatement cstmt = null;
        String cadena = "";
        try {
            cstmt = (CallableStatement) conexion.prepareCall("{call procedure_MaxMinEdadUser(?,?)}");
            cstmt.registerOutParameter(1, java.sql.Types.INTEGER);
            cstmt.registerOutParameter(2, java.sql.Types.INTEGER);
            cstmt.execute();
            cadena = cadena + "Usuario menor: " + cstmt.getInt(1) + " años" + "\n";
            cadena = cadena + "Usuario mayor: " + cstmt.getInt(2) + " años";
            JOptionPane.showMessageDialog(null, cadena, "Empleado fijo menor/mayor", 1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error Logger!");
        }

    }
    
    public void EdadMediaDAO(Connection conexion) {

        CallableStatement cstmt = null;
        String cadena = "";
        try {
            cstmt = (CallableStatement) conexion.prepareCall("{call pocedure_EdadMediaUser(?)}");
            cstmt.registerOutParameter(1, java.sql.Types.DOUBLE);
            cstmt.execute();
            cadena = cadena + "Edad media de los usuarios: " + (int)cstmt.getDouble(1) + " años";
            JOptionPane.showMessageDialog(null, cadena, "Edad media", 1);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error Logger!");
        }

    }
    
     
    
}
