/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionUser.modelo.BLL;

import TPV.clases.JavaMail;
import TPV.librerias.Encriptar;
import TPV.librerias.FileUpload;
import TPV.librerias.Funciones;
import static TPV.modulos.gestionPers.gestionUser.controlador.Controlador_User.frmPagerUser;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.MiniSimpleTableModel_User;
import TPV.modulos.gestionPers.gestionUser.pager.pagina;
import TPV.modulos.gestionPers.gestionUser.modelo.DAO.DAO_User;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.SingletonUser;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.Usuario;
import TPV.modulos.gestionPers.gestionUser.vista.CrearUser;
import TPV.modulos.gestionPers.gestionUser.vista.PagerUser;
import TPV.modulos.gestionPers.gestionUser.vista.UpdateUser;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import TPV.modulos.moduleLogin.vista.VentanaInicio;
import com.toedter.calendar.JTextFieldDateEditor;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class BLL_User {
    
    public static void pideNombre() {
        DAO_User.pideNombre();
        if (CrearUser.labErrorNom.isVisible()) {
            CrearUser.txtNombre.requestFocus();
        }
    }

    public static void pideUserName() {
        DAO_User.pideUserName();

        if (CrearUser.labErrorUsuario.isVisible()) {
            CrearUser.txtUser.requestFocus();
        } else {

            if (buscarUser(CrearUser.txtUser.getText()) != -1) {
                CrearUser.labErrorUsuario.setText("El usuario ya se encuentra la base de datos");
                CrearUser.labErrorUsuario.setVisible(true);
                CrearUser.txtUser.requestFocus();
                return;
            } else {
                CrearUser.labErrorUsuario.setVisible(false);
            }

        }

    }

    public static void pidePass() {
        DAO_User.pidePass();
        if (CrearUser.labErrorPass.isVisible()) {
            CrearUser.txtPass.requestFocus();
        }
    }

    public static void pideConfirmPass() {
        DAO_User.pideConfirmPass();
        if (CrearUser.labErrorConfirmPass.isVisible()) {
            CrearUser.txtConfirmPass.requestFocus();
        }
    }

    public static void pideApellidos() {
        DAO_User.pideApellidos();
        if (CrearUser.labErrorApellido.isVisible()) {
            CrearUser.txtApellidos.requestFocus();
        }

    }

    public static void pideDirec() {
        DAO_User.pideDireccion();
        if (CrearUser.labErrorDirec.isVisible()) {
            CrearUser.txtDireccion.requestFocus();
        }
    }

    public static void pideDNI() {
        int pos;

        DAO_User.pideDNI();

        if (CrearUser.labErrorDNI.isVisible()) {
            CrearUser.txtDNI.requestFocus();
        } else {
            Usuario usr = new Usuario(CrearUser.txtDNI.getText());
            pos = buscardniUser(usr);
            if (pos != -1) {

                CrearUser.labErrorDNI.setText("El dni ya se encuentra en la base de datos");
                CrearUser.labErrorDNI.setVisible(true);
                CrearUser.txtDNI.requestFocus();
                return;
            } else {
                CrearUser.labErrorDNI.setVisible(false);
            }

        }

    }

    public static void pideFechanac() {
        DAO_User.pideFechanac();
        if (CrearUser.labErrorFechanac.isVisible()) {
            CrearUser.dateFechanac.requestFocus();
        }

    }

    public static void pideTlfno() {
        DAO_User.pideTlfno();
        if (CrearUser.labErrorTelefono.isVisible()) {
            CrearUser.txtTlfno.requestFocus();
        }
    }

    public static void pideCorreo() {
           DAO_User.pideCorreo();
        if (CrearUser.labErrorCorreo.isVisible()) {
            CrearUser.txtCorreo.requestFocus();
        } else {

            if (buscaremailUser(CrearUser.txtCorreo.getText()) != -1) {

                CrearUser.labErrorCorreo.setText("El email ya se encuentra en la base de datos");
                CrearUser.labErrorCorreo.setVisible(true);
                CrearUser.txtCorreo.requestFocus();
                return;
            } else {
                CrearUser.labErrorCorreo.setVisible(false);
            }

        }

    }


    public static void cambiaNombre() {
        DAO_User.cambiaNombre();
        if (UpdateUser.labErrorNom.isVisible()) {
            UpdateUser.txtNombre.requestFocus();
        }
    }

    public static void cambiaUserName() {
       // DAO_User.CambiaUser();
       // String userOld = SingletonUser.ef.getLogin();

        if (UpdateUser.labErrorUsuario.isVisible()) {
            UpdateUser.txtUser.requestFocus();
        } else {
/**
            if (buscarUserEF(UpdateUser.txtUser.getText()) != -1) {
                if (!(UpdateUser.txtUser.getText().equals(userOld))) {
                    UpdateUser.labErrorUsuario.setText("El usuario ya se encuentra la base de datos");
                    UpdateUser.labErrorUsuario.setVisible(true);
                    UpdateUser.txtUser.requestFocus();
                    return;
                } else {
                    UpdateUser.labErrorUsuario.setVisible(false);
                }
            }*/

        }

    }

    public static void cambiaPass() {
        DAO_User.CambiaPass();
        if (UpdateUser.labErrorPass.isVisible()) {
            UpdateUser.txtPass.requestFocus();
        }
    }

    public static void cambiaConfirmPass() {
        DAO_User.CambiaConfirmPass();
        if (UpdateUser.labErrorConfirmPass.isVisible()) {
            UpdateUser.txtConfirmPass.requestFocus();
        }
    }

    public static void cambiaApellidos() {
        DAO_User.cambiaApellidos();
        if (UpdateUser.labErrorApellido.isVisible()) {
            UpdateUser.txtApellidos.requestFocus();
        }

    }

    public static void cambiaDirec() {

        DAO_User.cambiaDireccion();
        if (UpdateUser.labErrorDNI.isVisible()) {
            UpdateUser.txtNombre.requestFocus();
        }

    }

    public static void cambiaFechanac() {
        DAO_User.cambiaFechanac();
        if (UpdateUser.labErrorFechanac.isVisible()) {
            UpdateUser.dateFechanac.requestFocus();
        }

    }

    public static void cambiaTlfno() {
        DAO_User.cambiaTlfno();
        if (UpdateUser.labErrorTelefono.isVisible()) {
            UpdateUser.txtTlfno.requestFocus();
        }
    }

    public static void cambiaCorreo() {
        DAO_User.cambiaCorreo();
        //String emailOld = SingletonUser.ef.getCorreo();
        if (UpdateUser.labErrorCorreo.isVisible()) {
            UpdateUser.txtCorreo.requestFocus();
        } else {
/**
            if (buscaremailEF(UpdateUser.txtCorreo.getText()) != -1) {
                if (!(UpdateUser.txtCorreo.getText().equals(emailOld))) {

                    UpdateUser.labErrorCorreo.setText("El email ya se encuentra en la base de datos");
                    UpdateUser.labErrorCorreo.setVisible(true);
                    UpdateUser.txtCorreo.requestFocus();
                    return;
                } else {
                    UpdateUser.labErrorCorreo.setVisible(false);
                }
            }*/

        }

    }
    
    public static void cambiaUser() {
        String cod, nif = "";

        cambiaNombre();
        cambiaApellidos();
        cambiaDirec();
        cambiaFechanac();
        cambiaTlfno();
        cambiaCorreo();
        cambiaUserName();
        cambiaPass();
        cambiaConfirmPass();

        DAO_User.cambiaUser(SingletonUser.usr);
        if (UpdateUser.labErrorNom.isShowing() == false && UpdateUser.labErrorApellido.isShowing() == false
                && UpdateUser.labErrorDNI.isShowing() == false && UpdateUser.labErrorFechanac.isShowing() == false
                && UpdateUser.labErrorTelefono.isShowing() == false && UpdateUser.labErrorCorreo.isShowing() == false
                && UpdateUser.labErrorUsuario.isShowing() == false && UpdateUser.labErrorPass.isShowing() == false 
                && UpdateUser.labErrorConfirmPass.isShowing() == false) {

            BLLuser_DB _usr = new BLLuser_DB();
            if (_usr.modificarUserBLL()!= 0) {

                //Para actualizar el singleton de logueo si el empleado que se actualiza es el que esta logueado
                if (Singleton.efLog.getNif().equals(SingletonUser.usr.getNif())) {
                    Singleton.efLog = SingletonUser.usr;
                    VentanaInicio.labPrintUser.setText(Singleton.efLog.getLogin());
                    FileUpload.pintar(VentanaInicio.labAvatar, 60, 40, 1, Singleton.efLog.getAvatar());
                }else
                    ((MiniSimpleTableModel_User) frmPagerUser.tablaFijos.getModel()).cargar();

                //Para informar al usuario de que se ha producido la actualización con éxito
                UpdateUser.labUpdate.setVisible(true);
            }

        }

    }
    
    
    public static void pideUser() {
        pideNombre();
        pideApellidos();
        pideDirec();
        pideDNI();
        pideFechanac();
        pideTlfno();
        pideCorreo();
        pidePass();
        pideConfirmPass();
        pideUserName();

        DAO_User.pideUser();

        if (CrearUser.labErrorNom.isShowing() == false && CrearUser.labErrorApellido.isShowing() == false
                && CrearUser.labErrorDirec.isShowing() == false && CrearUser.labErrorDNI.isShowing() == false
                && CrearUser.labErrorFechanac.isShowing() == false && CrearUser.labErrorTelefono.isShowing() == false
                && CrearUser.labErrorCorreo.isShowing() == false  && CrearUser.labErrorUsuario.isShowing() == false 
                && CrearUser.labErrorPass.isShowing() == false && CrearUser.labErrorConfirmPass.isShowing() == false) {

            if (SingletonUser.usr != null) {
                SingletonUser.user.add(SingletonUser.usr);
                BLLuser_DB _usr = new BLLuser_DB();
                _usr.nuevoUserBLL();
                ((MiniSimpleTableModel_User) frmPagerUser.tablaFijos.getModel()).cargar();

                //Etiqueta de  usuario succesfull
                CrearUser.labCorrecto.setVisible(true);

                //Clear
                CrearUser.txtNombre.setText("");
                CrearUser.txtApellidos.setText("");
                CrearUser.txtDireccion.setText("");
                CrearUser.txtDNI.setText("");
                ((JTextFieldDateEditor) CrearUser.dateFechanac.getDateEditor()).setText("");
                CrearUser.txtTlfno.setText("");
                CrearUser.txtCorreo.setText("");
                CrearUser.txtPass.setText("");
                CrearUser.txtConfirmPass.setText("");

            }

        }

    }
    
    
    /**
     * Función que busca coincidencias con el login introducido para evitar
     * repeticiones o validar el user. olvidar contraseña.
     *
     * @param email
     * @return
     */
    public static int buscarUser(String login) {
        int aux = -1;
        BLLuser_DB _user = new BLLuser_DB();
        _user.listAllUserBLL();

        for (int i = 0; i <= (SingletonUser.user.size() - 1); i++) {
            if ((SingletonUser.user.get(i)).getLogin().equals(login))//buclea hasta que encuentra un email que concuadre con el comparator de la madre y lo devuelve como aux
            {
                SingletonUser.usr = SingletonUser.user.get(i);
                aux = i;
            }
        }
        return aux;//retorna aux, si lo ha encontrado dara la posicion, si no devolvera -1 lo cual significa que no hay dni que concuadre
    }
    
    public static int buscardniUser(Usuario usr) {
        int aux = -1;
        BLLuser_DB _user = new BLLuser_DB();
        _user.listAllUserBLL();

        for (int i = 0; i <= (SingletonUser.user.size() - 1); i++) {
            if ((SingletonUser.user.get(i)).equals(usr))//buclea hasta que encuentra un dni que concuadre con el comparator de la madre y lo devuelve como aux
            {
                aux = i;
            }
        }
        return aux;//retorna aux, si lo ha encontrado dara la posicion, si no devolvera -1 lo cual significa que no hay dni que concuadre
    }

    /**
     * Función que busca coincidencias con el email introducido.Para evitar
     * repeticiones o validar el email
     *
     *
     * @param email
     * @return
     */
    public static int buscaremailUser(String email) {
        int aux = -1;
        BLLuser_DB _usr = new BLLuser_DB();
        _usr.listAllUserBLL();

        for (int i = 0; i <= (SingletonUser.user.size() - 1); i++) {
            if ((SingletonUser.user.get(i)).getCorreo().equals(email))//buclea hasta que encuentra un email que concuadre con el comparator de la madre y lo devuelve como aux
            {
                SingletonUser.usr = SingletonUser.user.get(i);
                aux = i;
            }
        }
        return aux;//retorna aux, si lo ha encontrado dara la posicion, si no devolvera -1 lo cual significa que no hay dni que concuadre
    }
    
    public static boolean validateUser() {
        String token, EncrypToken;
        boolean correcto = false;

        pideNombre();
        pideApellidos();
        pideDirec();
        pideDNI();
        pideFechanac();
        pideTlfno();
        pideCorreo();
        pidePass();
        pideConfirmPass();
        pideUserName();

        DAO_User.pideUser();

        if (SingletonUser.usr != null) {

            token = Encriptar.getCadenaAleatoria(25);
            EncrypToken = Encriptar.encriptarTokenMD5(token);
            Singleton.confirmToken = EncrypToken;

            JavaMail mail = new JavaMail(SingletonUser.usr.getCorreo(), "E-mail de confirmación", EncrypToken);
            //enviamos el mensaje
            String error = mail.send();
            if (error.equals("")) {
                correcto = true;
                JOptionPane.showMessageDialog(null, "Envio Correcto", "Correcto", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Error de envio:\n" + error, "Error", JOptionPane.ERROR_MESSAGE);
            }

        }

        return correcto;
    }
    
    //Per a agafar el usuari seleccionat al pager.
    public static int updateUsuario() {
        int n = ((MiniSimpleTableModel_User)PagerUser.tablaFijos.getModel()).getRowCount();
        int selec, selec1, inicio;

        if (n != 0) {
            inicio = (pagina.currentPageIndex - 1) * pagina.itemsPerPage;
            selec = PagerUser.tablaFijos.getSelectedRow();
            selec1 = inicio + selec;

            if (selec == -1) {
                JOptionPane.showMessageDialog(null, "No hay una persona seleccionada", "Error!", 2);
            } else {
                SingletonUser.usr = new Usuario((String) PagerUser.tablaFijos.getModel().getValueAt(selec1, 0));
                return 1;
            }

        } else {
            JOptionPane.showMessageDialog(null, "lista vacía", "Error!", 2);

        }

        return 0;

    }
    
    //Per a agafar el usuari seleccionat al pager.
    public static void deleteUser() {
        int n = ((MiniSimpleTableModel_User) PagerUser.tablaFijos.getModel()).getRowCount();
        String dni;
        int pos = -1, opc, selec, selec1, inicio;

        if (n != 0) {
            inicio = (pagina.currentPageIndex - 1) * pagina.itemsPerPage;
            selec = PagerUser.tablaFijos.getSelectedRow();
            selec1 = inicio + selec;
            if (selec == -1) {
                JOptionPane.showMessageDialog(null, "No hay una persona seleccionada", "Error!", 2);
            } else {
                dni = (String) PagerUser.tablaFijos.getModel().getValueAt(selec1, 0);
                opc = Funciones.valida_confirmado("Deseas borrar a la persona con DNI: " + dni, "Borrar EmpF");
                if (opc == 0) {
                    SingletonUser.usr = new Usuario(dni);
                    pos = buscardniUser(SingletonUser.usr);
                    SingletonUser.user.remove(pos);
                    BLLuser_DB _user = new BLLuser_DB();
                    _user.borrarUserBLL();
                    ((MiniSimpleTableModel_User) PagerUser.tablaFijos.getModel()).removeRow(selec1);
                    ((MiniSimpleTableModel_User) frmPagerUser.tablaFijos.getModel()).cargar();
                }
            }
        } else {
            Funciones.resultado("lista vacía", "Error!");
        }
    }
    
    
    
    public static void rellenarUser() {
        int pos = -1;

        pos = BLL_User.buscardniUser(SingletonUser.usr);

        UpdateUser.txtNombre.setText(SingletonUser.user.get(pos).getNombre());
        UpdateUser.txtApellidos.setText(SingletonUser.user.get(pos).getApellidos());
        UpdateUser.txtDireccion.setText(SingletonUser.user.get(pos).getDireccion());
        UpdateUser.txtDNI.setText(SingletonUser.user.get(pos).getNif());
        ((JTextFieldDateEditor) UpdateUser.dateFechanac.getDateEditor()).setText(SingletonUser.user.get(pos).getFechanac().toString());
        UpdateUser.txtTlfno.setText(SingletonUser.user.get(pos).getTlfno());
        UpdateUser.txtCorreo.setText(SingletonUser.user.get(pos).getCorreo());
        UpdateUser.labImprimeEdad.setText("" + SingletonUser.user.get(pos).getEdad());
        UpdateUser.txtUser.setText(SingletonUser.user.get(pos).getLogin());
        UpdateUser.txtPass.setText(SingletonUser.user.get(pos).getPassword());
        UpdateUser.txtConfirmPass.setText(SingletonUser.user.get(pos).getPassword());
        FileUpload.pintar(UpdateUser.labPrintAvatar, 60, 40, 1, SingletonUser.user.get(pos).getAvatar());
        if(SingletonUser.user.get(pos).getTipo().equals("admin")){
        UpdateUser.ComboxTipo.setSelectedIndex(1);
        }else{
        UpdateUser.ComboxTipo.setSelectedIndex(0);
        }

        //Se guarda en el singleton el correo y el user por si hay modificaciones que al buscar en la BD me permita cambiar los campos.
        SingletonUser.usr.setCorreo(SingletonUser.user.get(pos).getCorreo());
        SingletonUser.usr.setLogin(SingletonUser.user.get(pos).getLogin());
        SingletonUser.usr.setAvatar(SingletonUser.user.get(pos).getAvatar());
        

    }
    
}
