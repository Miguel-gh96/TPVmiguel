/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionUser.modelo.BLL;

import TPV.clases.ConexionBD;
import TPV.modulos.gestionPers.gestionUser.modelo.DAO.DAOuser_DB;
import java.sql.Connection;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class BLLuser_DB {
    
    public void nuevoUserBLL() {


        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();
        DAOuser_DB usrDAO = new DAOuser_DB();

          usrDAO.nuevoEmpfDAO(_con);
        _conexion_DB.CerrarConexion(_con);

    }
    
    
    public void listAllUserBLL() {

        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();
        DAOuser_DB userDAO = new DAOuser_DB();
        try {
           userDAO.listAlluserDAO(_con);     
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");

        }
        _conexion_DB.CerrarConexion(_con);

    }
    
    
    public int modificarUserBLL() {
        int resultado = 0;
        Connection _con;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();

        DAOuser_DB userDAO = new DAOuser_DB();

        resultado=userDAO.modificarUsuarioDAO(_con);

        _conexion_DB.CerrarConexion(_con);
        
        return resultado;
    }
    
    
    public void borrarUserBLL() {


        Connection _con;
        ConexionBD _conexion_DB = new ConexionBD();

        _con = _conexion_DB.AbrirConexion();

        DAOuser_DB userDAO = new DAOuser_DB();

        userDAO.borrarEmpleadofijoDAO(_con);

        _conexion_DB.CerrarConexion(_con);
    }
    
    /**
     * Obtener la edad del usuario mayor y del menor
     */
    public void userMenorMayorBLL() {

        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();

        DAOuser_DB _usrDAO = new DAOuser_DB();
        _usrDAO.userMenorMayorDAO(_con);
        _conexion_DB.CerrarConexion(_con);

    }
    
    /**
     * obtener la edad media de todos los usuarios
     */
    public void EdadMediaBLL() {

        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();
        DAOuser_DB _usrDAO = new DAOuser_DB();
        _usrDAO.EdadMediaDAO(_con);
        _conexion_DB.CerrarConexion(_con);
    }
    
    
}
