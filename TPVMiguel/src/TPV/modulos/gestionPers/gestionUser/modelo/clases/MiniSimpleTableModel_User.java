package TPV.modulos.gestionPers.gestionUser.modelo.clases;

import TPV.clases.Fecha;
import static TPV.modulos.gestionPers.gestionUser.controlador.Controlador_User.combo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

import TPV.modulos.gestionPers.gestionUser.modelo.BLL.BLLuser_DB;
import TPV.modulos.gestionPers.gestionUser.pager.pagina;
import TPV.modulos.gestionPers.gestionUser.vista.PagerUser;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class MiniSimpleTableModel_User extends AbstractTableModel {

    public static ArrayList<Usuario> datos = new ArrayList<Usuario>();
    public static ArrayList<Usuario> datosaux = new ArrayList<Usuario>();
    String[] columnas = {"DNI", "Nombre", "Apellidos", "Teléfono", "Fecha Nacimiento", "Correo", "Dirección","Edad","Usuario","Tipo","Estado"};

    ////////////////////estos métodos son necesarios para que jtable funcione/////////////////////
    @Override
    public String getColumnName(int col) {
        return columnas[col].toString();
    }

    //Devuelve el numero de filas
    @Override
    public int getRowCount() {
        return datos.size();
    }

    //Devuelve el numero de columnas
    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    //Devuelve el valor del objeto en la fila y columna
    @Override
    public Object getValueAt(int row, int col) {

        Object dev = null;
        Usuario fila = (Usuario) datos.get(row);

        switch (col) {
            case 0:
                dev = fila.getNif();
                break;

            case 1:
                dev = fila.getNombre();
                break;

            case 2:
                dev = fila.getApellidos();
                break;

            case 3:
                dev = fila.getTlfno();
                break;

            case 4:
                dev = fila.getFechanac().toString();
                break;
            case 5:
                dev = fila.getCorreo();
                break;
            case 6:
                dev = fila.getDireccion();
                break;
            case 7:
                String edad = "" + fila.getEdad();
                dev = edad;
                break;
            case 8:
                dev = fila.getLogin();
                break;
            case 9:
                dev = fila.getTipo();
                break;
            case 10:
                dev = fila.getEstado();
                break;
             
        }
        return dev;
    }

    //Determina si una fila y columna ha de ser editable
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    //Actualiza un objeto de una fila y columna
    @Override
    public void setValueAt(Object value, int row, int col) {
        Usuario fila = (Usuario) datos.get(row);
        Fecha nac = null, contrat = null;

        switch (col) {
            case 0:
                fila.setNif(value.toString());
                break;

            case 1:
                fila.setNombre(value.toString());
                break;

            case 2:
                fila.setApellidos(value.toString());
                break;

            case 3:
                fila.setTlfno(value.toString());
                break;

            case 4:
                nac = new Fecha(value.toString());
                fila.setFechanac(nac);
                break;
            case 5:
                fila.setCorreo(value.toString());
                break;
            case 6:
                fila.setDireccion(value.toString());
                break;
            case 7:
                fila.setLogin(value.toString());
                break;
            case 9:
                fila.setTipo(value.toString());
                break;
        }
        fireTableCellUpdated(row, col);
    }

    public void addRow(Usuario usu) {
        datos.add(usu);
        fireTableDataChanged();
    }

    public void cargar() {
        datos.clear();
        datosaux.clear();
        SingletonUser.user.clear();

        Usuario user = new Usuario();
        BLLuser_DB _user = new BLLuser_DB();
        _user.listAllUserBLL();
        for (int i = 0; i < SingletonUser.user.size(); i++) {
  
            user = SingletonUser.user.get(i);
            addRow(user);
            datosaux.add(user);
            try {
                Thread.sleep(1); //1 milliseconds
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public void filtrar() {
        datos.clear();
        int cont = 0;

        String nom = (String) ((JComboBox) combo).getSelectedItem();

        if (nom != null) {
            for (int i = 0; i < datosaux.size(); i++) {
                if (datosaux.get(i).getNombre().toLowerCase().startsWith(nom.toLowerCase())) {
                    addRow(datosaux.get(i));
                    cont++;
                }
            }

            if (cont > 0) {
                PagerUser.labResultado.setText(" " + cont);
                PagerUser.labResultado.setForeground(Color.green);
            } else {
                PagerUser.labResultado.setText(" " + cont);
                PagerUser.labResultado.setForeground(Color.red);
            }

            System.out.println("word selected: " + nom);
            pagina.initLinkBox();
        }
    }

    public Usuario buscar(String u) {
        datos.clear();
        cargar();

        String res;
        for (int i = 0; i < datos.size(); i++) {
            res = datos.get(i).toString();
            if (res.contains(u)) {
                return datos.get(i);
            }
        }
        return null;
    }

    public int buscaUsuario(Usuario u) {
        datos.clear();
        cargar();

        for (int i = 0; i < datos.size(); i++) {
            if (datos.get(i).equals(u)) {
                return i;
            }
        }
        return -1;
    }

    public void removeRow(int fila) {
        datos.remove(fila);
        fireTableDataChanged();
    }

    //para pintar ImageIcon
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }
}
