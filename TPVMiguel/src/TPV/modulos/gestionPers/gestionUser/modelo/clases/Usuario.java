/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionUser.modelo.clases;

import TPV.clases.Fecha;
import TPV.modulos.gestionPers.Clases.Persona;
import java.io.Serializable;

/**
 *
 * @author miguel
 */
public class Usuario extends Persona implements Serializable {
    
    
    public Usuario(String nombre, String apellidos, String direccion, String nif, Fecha fechanac,
             String tlfno, String correo, String login, String password) {
        super(nombre, apellidos, direccion, nif, fechanac, tlfno, correo, login, password);
    }

   public float calcularsueldo() {
        return this.getSueldo();
    }
        
    
    
    public Usuario (){
    
    }
    
    public Usuario (String nif){
        super(nif);
    }
    
    public String toString() {
        return "------Datos Personales-------" + "\n"
                + "Nombre: " + this.getNombre() + "\n"
                + "Apellidos: " + this.getApellidos() + "\n"
                + "Dirección: " + this.getDireccion() + "\n"
                + "NIF: " + this.getNif() + "\n"
                + "Teléfono: " + this.getTlfno() + "\n"
                + "Correo: " + this.getCorreo() + "\n"
                + "Fecha de nacimiento: " + this.getFechanac().toString() + "\n"
                + "Edad: " + this.getEdad() + "\n"
                + "\n" + "------Datos Empleado---------" + "\n"
                +"avatar: "+this.getAvatar()+"\n"
                +"pass:"+this.getPassword()+"\n"
                +"tipo: "+this.getTipo();
                
    }
    
    
}
