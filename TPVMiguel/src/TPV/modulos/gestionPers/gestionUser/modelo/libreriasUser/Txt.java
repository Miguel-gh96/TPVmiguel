package TPV.modulos.gestionPers.gestionUser.modelo.libreriasUser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import TPV.librerias.Funciones;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.SingletonUser;

public class Txt {

	public static void generatxtUser() {
        String PATH = null;
        try {
            File f;
            JFileChooser fileChooser = new JFileChooser();
            
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Texto (*.txt)", "txt"));

            
            int seleccion = fileChooser.showSaveDialog(null);
            if (seleccion == JFileChooser.APPROVE_OPTION) {
                File JFC = fileChooser.getSelectedFile();
                PATH = JFC.getAbsolutePath();
                PATH=PATH+ ".txt";
                f = new File(PATH);
                
                FileOutputStream fo=new FileOutputStream(f);
				ObjectOutputStream o=new ObjectOutputStream(fo);
				o.writeObject(SingletonUser.user);
				o.close();
                Funciones.resultado( "Archivo TXT guardado con exito", "Archivo TXT");
            }
        } catch (Exception e) {
        	Funciones.resultado("Error al grabar el TXT", "Error");
        }
    }

	
	
}
