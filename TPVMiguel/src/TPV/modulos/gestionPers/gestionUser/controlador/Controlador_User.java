/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.gestionPers.gestionUser.controlador;

import TPV.Ppal;
import TPV.librerias.FileUpload;
import TPV.librerias.Theme;
import TPV.modulos.gestionPers.gestionEF.controlador.EmpF_Controlador;
import static TPV.modulos.gestionPers.gestionEF.controlador.EmpF_Controlador.frmConfig;
import TPV.modulos.gestionPers.gestionEF.vista.Configuration;
import TPV.modulos.gestionPers.gestionUser.autocomplete.AutocompleteJComboBox;
import TPV.modulos.gestionPers.gestionUser.autocomplete.StringSearchable;
import TPV.modulos.gestionPers.gestionUser.modelo.BLL.BLL_User;
import TPV.modulos.gestionPers.gestionUser.modelo.BLL.BLL_User_Fitxers;
import TPV.modulos.gestionPers.gestionUser.modelo.BLL.BLLuser_DB;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.MiniSimpleTableModel_User;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.RenderizaUser;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.SingletonUser;
import TPV.modulos.gestionPers.gestionUser.pager.pagina;
import TPV.modulos.gestionPers.gestionUser.vista.CrearUser;
import TPV.modulos.gestionPers.gestionUser.vista.PagerUser;
import TPV.modulos.gestionPers.gestionUser.vista.UpdateUser;
import TPV.modulos.moduleLogin.controlador.Controlador_Login;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import TPV.modulos.moduleLogin.vista.VentanaConfirm;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author miguel
 */
public class Controlador_User implements ActionListener, KeyListener, MouseListener {

    public static CrearUser frmCrearUser = new CrearUser();
    public static UpdateUser frmUpdateUser = new UpdateUser();
    public static PagerUser frmPagerUser = new PagerUser();
    public static TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(new MiniSimpleTableModel_User());
    private int[] cont = new int[9];
    public static AutocompleteJComboBox combo = null;

    public enum Action {

        //Acciones del PagerEF
        _labCreate,
        _labUpdate,
        _labDelete,
        _jComboBox1,
        _labJSON,
        _labXML,
        _labTXT,
        _labConfig,
        _btnPrimero,
        _btnSiguiente,
        _btnAnterior,
        _btnUltimo,
        _labMaxmin,
        _labEdadMedia,
        //Acciones del CrearEF
        _txtNombre,
        _txtApellidos,
        _txtDireccion,
        _txtDNI,
        _txtTlfno,
        _txtCorreo,
        _labAnadir,
        _txtPass,
        _txtConfirmPass,
        _txtUser,
        //Acciones del UpdateEF
        _txtNombre1,
        _txtApellidos1,
        _txtDireccion1,
        _txtDNI1,
        _txtTlfno1,
        _txtCorreo1,
        _labActualizar,
        _txtPass1,
        _txtConfirmPass1,
        _txtUser1,
        _labChangeAvatar,

    }

    public Controlador_User(JFrame User, int i) {

        if (i == 0) {//CrearUser
            this.frmCrearUser = (CrearUser) User;
        }

        if (i == 1) {//UpdateUser
            this.frmUpdateUser = (UpdateUser) User;
        }
        if (i == 2) {//frmPagerUser
            this.frmPagerUser = (PagerUser) User;
        }

    }

    public void iniciar(int i) {

        if (i == 0) {//crear user
            /**
             * labels de errores al introducir datos el usuario
             */
            frmCrearUser.labErrorNom.setVisible(false);
            frmCrearUser.labErrorApellido.setVisible(false);
            frmCrearUser.labErrorCorreo.setVisible(false);
            frmCrearUser.labErrorDNI.setVisible(false);
            frmCrearUser.labErrorDirec.setVisible(false);
            frmCrearUser.labErrorFechanac.setVisible(false);
            frmCrearUser.labErrorTelefono.setVisible(false);
            frmCrearUser.labErrorUsuario.setVisible(false);
            frmCrearUser.labErrorPass.setVisible(false);
            frmCrearUser.labErrorConfirmPass.setVisible(false);

            /**
             * Etiqueta de usuarios correctamente añadidios
             */
            frmCrearUser.labCorrecto.setVisible(false);

            /**
             * Rellenar datos logueo
             */
            //this.frmCrearUser.labPrintUser.setText(Singleton.efLog.getLogin());
            //FileUpload.pintar(this.frmCrearUser.labPrintAvatarLog,104,76,1,Singleton.efLog.getAvatar());
            /**
             * Evitamos que el ususario edite la fecha por teclado
             */
            ((JTextFieldDateEditor) frmCrearUser.dateFechanac.getDateEditor()).setEditable(false);

            /**
             * Cuando el usuario haga click en X iremos al menu pricipal
             */
            this.frmCrearUser.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            this.frmCrearUser.addWindowListener(new WindowAdapter() {

                public void windowClosing(WindowEvent e) {
                    //frmCrearUser.dispose();
                    //MODIFICARnew EmpF_Controlador(new PagerEF(), 0).iniciar(0);
                }
            });

            /**
             * Generamos un vector de contadores para cada uno de los labels de
             * errores.
             */
            for (int j = 0; j < 9; j++) {
                cont[j] = 0;
            }

            /**
             * Componentes ActionListener , MouseListener and KeyListener
             */
            this.frmCrearUser.labAnadir.setName("_labAnadir");
            this.frmCrearUser.labAnadir.addMouseListener(this);

            this.frmCrearUser.txtNombre.setActionCommand("_txtNombre");
            this.frmCrearUser.txtNombre.setName("_txtNombre");
            this.frmCrearUser.txtNombre.addActionListener(this);
            this.frmCrearUser.txtNombre.addKeyListener(this);
            this.frmCrearUser.txtNombre.addMouseListener(this);

            this.frmCrearUser.txtApellidos.setActionCommand("_txtApellidos");
            this.frmCrearUser.txtApellidos.setName("_txtApellidos");
            this.frmCrearUser.txtApellidos.addActionListener(this);
            this.frmCrearUser.txtApellidos.addKeyListener(this);
            this.frmCrearUser.txtApellidos.addMouseListener(this);

            this.frmCrearUser.txtDireccion.setActionCommand("_txtDireccion");
            this.frmCrearUser.txtDireccion.setName("_txtDireccion");
            this.frmCrearUser.txtDireccion.addActionListener(this);
            this.frmCrearUser.txtDireccion.addKeyListener(this);
            this.frmCrearUser.txtDireccion.addMouseListener(this);

            this.frmCrearUser.txtDNI.setActionCommand("_txtDNI");
            this.frmCrearUser.txtDNI.setName("_txtDNI");
            this.frmCrearUser.txtDNI.addActionListener(this);
            this.frmCrearUser.txtDNI.addKeyListener(this);
            this.frmCrearUser.txtDNI.addMouseListener(this);

            this.frmCrearUser.txtCorreo.setActionCommand("_txtCorreo");
            this.frmCrearUser.txtCorreo.setName("_txtCorreo");
            this.frmCrearUser.txtCorreo.addActionListener(this);
            this.frmCrearUser.txtCorreo.addKeyListener(this);
            this.frmCrearUser.txtCorreo.addMouseListener(this);

            this.frmCrearUser.txtTlfno.setActionCommand("_txtTlfno");
            this.frmCrearUser.txtTlfno.setName("_txtTlfno");
            this.frmCrearUser.txtTlfno.addActionListener(this);
            this.frmCrearUser.txtTlfno.addKeyListener(this);
            this.frmCrearUser.txtTlfno.addMouseListener(this);

            this.frmCrearUser.txtPass.setActionCommand("_txtPass");
            this.frmCrearUser.txtPass.setName("_txtPass");
            this.frmCrearUser.txtPass.addActionListener(this);
            this.frmCrearUser.txtPass.addKeyListener(this);
            this.frmCrearUser.txtPass.addMouseListener(this);

            this.frmCrearUser.txtConfirmPass.setActionCommand("_txtConfirmPass");
            this.frmCrearUser.txtConfirmPass.setName("_txtConfirmPass");
            this.frmCrearUser.txtConfirmPass.addActionListener(this);
            this.frmCrearUser.txtConfirmPass.addKeyListener(this);
            this.frmCrearUser.txtConfirmPass.addMouseListener(this);

            this.frmCrearUser.txtUser.setActionCommand("_txtUser");
            this.frmCrearUser.txtUser.setName("_txtUser");
            this.frmCrearUser.txtUser.addActionListener(this);
            this.frmCrearUser.txtUser.addKeyListener(this);
            this.frmCrearUser.txtUser.addMouseListener(this);
        }

        if (i == 1) {//actualizar user

            /**
             * El dni no se puede modificar ni las fechas
             */
            frmUpdateUser.txtDNI.setEditable(false);
            ((JTextFieldDateEditor) frmUpdateUser.dateFechanac.getDateEditor()).setEditable(false);

            /**
             * labels de errores al introducir datos el usuario
             */
            frmUpdateUser.labErrorNom.setVisible(false);
            frmUpdateUser.labErrorApellido.setVisible(false);
            frmUpdateUser.labErrorCorreo.setVisible(false);
            frmUpdateUser.labErrorDirec.setVisible(false);
            frmUpdateUser.labErrorDNI.setVisible(false);
            frmUpdateUser.labErrorFechanac.setVisible(false);
            frmUpdateUser.labErrorTelefono.setVisible(false);
            frmUpdateUser.labErrorUsuario.setVisible(false);
            frmUpdateUser.labErrorPass.setVisible(false);
            frmUpdateUser.labErrorConfirmPass.setVisible(false);

            /**
             * Cuando el usuario haga click en X iremos al menu pricipal
             */
            this.frmUpdateUser.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            this.frmUpdateUser.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    //frmUpdateUser.dispose();
                    //MODIFICARnew EmpF_Controlador(new PagerEF(), 0).iniciar(0);
                }
            });

            /**
             * Generamos un vector de contadores para cada uno de los labels de
             * errores.
             */
            for (int j = 0; j < 8; j++) {
                cont[j] = 0;
            }

            /**
             * Rellenamos los txt con los datos del empleado seleccionado
             */
            BLL_User.rellenarUser();
            /**
             * lab de aviso de actualizacion desactivado
             */
            frmUpdateUser.labUpdate.setVisible(false);

            /**
             * Componentes ActionListener, KeyListener, MouseListener
             */
            this.frmUpdateUser.txtNombre.setActionCommand("_txtNombre1");
            this.frmUpdateUser.txtNombre.setName("_txtNombre1");
            this.frmUpdateUser.txtNombre.addActionListener(this);
            this.frmUpdateUser.txtNombre.addKeyListener(this);
            this.frmUpdateUser.txtNombre.addMouseListener(this);

            this.frmUpdateUser.txtApellidos.setActionCommand("_txtApellidos1");
            this.frmUpdateUser.txtApellidos.setName("_txtApellidos1");
            this.frmUpdateUser.txtApellidos.addActionListener(this);
            this.frmUpdateUser.txtApellidos.addKeyListener(this);
            this.frmUpdateUser.txtApellidos.addMouseListener(this);

            this.frmUpdateUser.txtDireccion.setActionCommand("_txtDireccion1");
            this.frmUpdateUser.txtDireccion.setName("_txtDireccion1");
            this.frmUpdateUser.txtDireccion.addActionListener(this);
            this.frmUpdateUser.txtDireccion.addKeyListener(this);
            this.frmUpdateUser.txtDireccion.addMouseListener(this);

            this.frmUpdateUser.txtDNI.setActionCommand("_txtDNI1");
            this.frmUpdateUser.txtDNI.setName("_txtDNI1");
            this.frmUpdateUser.txtDNI.addActionListener(this);
            this.frmUpdateUser.txtDNI.addKeyListener(this);
            this.frmUpdateUser.txtDNI.addMouseListener(this);

            this.frmUpdateUser.txtCorreo.setActionCommand("_txtDireccion1");
            this.frmUpdateUser.txtCorreo.setName("_txtDireccion1");
            this.frmUpdateUser.txtCorreo.addActionListener(this);
            this.frmUpdateUser.txtCorreo.addKeyListener(this);
            this.frmUpdateUser.txtCorreo.addMouseListener(this);

            this.frmUpdateUser.txtTlfno.setActionCommand("_txtTlfno1");
            this.frmUpdateUser.txtTlfno.setName("_txtTlfno1");
            this.frmUpdateUser.txtTlfno.addActionListener(this);
            this.frmUpdateUser.txtTlfno.addKeyListener(this);
            this.frmUpdateUser.txtTlfno.addMouseListener(this);

            this.frmUpdateUser.txtPass.setActionCommand("_txtPass1");
            this.frmUpdateUser.txtPass.setName("_txtPass1");
            this.frmUpdateUser.txtPass.addActionListener(this);
            this.frmUpdateUser.txtPass.addKeyListener(this);
            this.frmUpdateUser.txtPass.addMouseListener(this);

            this.frmUpdateUser.txtConfirmPass.setActionCommand("_txtConfirmPass1");
            this.frmUpdateUser.txtConfirmPass.setName("_txtConfirmPass1");
            this.frmUpdateUser.txtConfirmPass.addActionListener(this);
            this.frmUpdateUser.txtConfirmPass.addKeyListener(this);
            this.frmUpdateUser.txtConfirmPass.addMouseListener(this);

            this.frmUpdateUser.txtUser.setActionCommand("_txtUser1");
            this.frmUpdateUser.txtUser.setName("_txtUser1");
            this.frmUpdateUser.txtUser.addActionListener(this);
            this.frmUpdateUser.txtUser.addKeyListener(this);
            this.frmUpdateUser.txtUser.addMouseListener(this);

            this.frmUpdateUser.labActualizar.setName("_labActualizar");
            this.frmUpdateUser.labActualizar.addMouseListener(this);

            this.frmUpdateUser.labChangeAvatar.setName("_labChangeAvatar");
            this.frmUpdateUser.labChangeAvatar.addMouseListener(this);

        }

        if (i == 2) {//Pager User 

            /**
             * Filtro para buscar empleados por nombre + automcompletar
             */
            List<String> myWords = new ArrayList<String>();
            for (int j = 0; j <= SingletonUser.user.size() - 1; j++) {
                myWords.add(SingletonUser.user.get(j).getNombre());
            }

            StringSearchable searchable = new StringSearchable(myWords);
            combo = new AutocompleteJComboBox(searchable);
            frmPagerUser.panelFiltro.setLayout(new java.awt.BorderLayout());
            frmPagerUser.panelFiltro.add(combo);

            combo.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboActionPerformed(evt);
                }
            });

            /**
             * Deshabilitar botones para los usuarios no admin
             */
            if (Singleton.efLog.getTipo().equals("admin")) {
                frmPagerUser.labCreate.setVisible(true);
                frmPagerUser.labDelete.setVisible(true);
                frmPagerUser.labUpdate.setVisible(true);
                frmPagerUser.labConfig.setVisible(true);

            } else {
                frmPagerUser.labCreate.setVisible(false);
                frmPagerUser.labDelete.setVisible(false);
                frmPagerUser.labUpdate.setVisible(false);
                frmPagerUser.labConfig.setVisible(false);
            }

            /**
             * Cargar tabla de empleados fijos
             */
            frmPagerUser.tablaFijos.setModel(new MiniSimpleTableModel_User());
            ((MiniSimpleTableModel_User) frmPagerUser.tablaFijos.getModel()).cargar();
            frmPagerUser.tablaFijos.setFillsViewportHeight(true);
            frmPagerUser.tablaFijos.setRowSorter(sorter);
            RenderizaUser renderizador = new RenderizaUser();
            frmPagerUser.tablaFijos.setDefaultRenderer(String.class, renderizador);
            frmPagerUser.txtCaja.setEditable(false);

            pagina.inicializa();
            pagina.initLinkBox();

            /**
             * Componentes ActionPerformed
             */
            this.frmPagerUser.jComboBox1.setActionCommand("_jComboBox1");
            this.frmPagerUser.jComboBox1.addActionListener(this);

            this.frmPagerUser.btnPrimero.setActionCommand("_btnPrimero");
            this.frmPagerUser.btnPrimero.addActionListener(this);

            this.frmPagerUser.btnUltimo.setActionCommand("_btnUltimo");
            this.frmPagerUser.btnUltimo.addActionListener(this);

            this.frmPagerUser.btnSiguiente.setActionCommand("_btnSiguiente");
            this.frmPagerUser.btnSiguiente.addActionListener(this);

            this.frmPagerUser.btnAnterior.setActionCommand("_btnAnterior");
            this.frmPagerUser.btnAnterior.addActionListener(this);

            /**
             * Complementos Mouse Event
             */
            this.frmPagerUser.labMaxMin.setName("_labMaxmin");
            this.frmPagerUser.labMaxMin.addMouseListener(this);

            this.frmPagerUser.labEdadMedia.setName("_labEdadMedia");
            this.frmPagerUser.labEdadMedia.addMouseListener(this);

            this.frmPagerUser.labConfig.setName("_labConfig");
            this.frmPagerUser.labConfig.addMouseListener(this);

            this.frmPagerUser.labJSON.setName("_labJSON");
            this.frmPagerUser.labJSON.addMouseListener(this);

            this.frmPagerUser.labXML.setName("_labXML");
            this.frmPagerUser.labXML.addMouseListener(this);

            this.frmPagerUser.labTXT.setName("_labTXT");
            this.frmPagerUser.labTXT.addMouseListener(this);

            this.frmPagerUser.labCreate.setName("_labCreate");
            this.frmPagerUser.labCreate.addMouseListener(this);

            this.frmPagerUser.labUpdate.setName("_labUpdate");
            this.frmPagerUser.labUpdate.addMouseListener(this);

            this.frmPagerUser.labDelete.setName("_labDelete");
            this.frmPagerUser.labDelete.addMouseListener(this);

        }

    }

    public static void comboActionPerformed(java.awt.event.ActionEvent evt) {
        System.out.println("word selected: " + ((JComboBox) combo).getSelectedItem());
        pagina.currentPageIndex = 1;
        ((MiniSimpleTableModel_User) frmPagerUser.tablaFijos.getModel()).filtrar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (Action.valueOf(e.getActionCommand())) {
            case _jComboBox1:
                pagina.itemsPerPage = Integer.parseInt(frmPagerUser.jComboBox1.getSelectedItem().toString());
                pagina.currentPageIndex = 1;
                pagina.initLinkBox();
                break;

            case _btnPrimero:
                pagina.currentPageIndex = 1;
                pagina.initLinkBox();
                break;
            case _btnUltimo:
                pagina.currentPageIndex = pagina.maxPageIndex;
                pagina.initLinkBox();
                break;
            case _btnSiguiente:
                pagina.currentPageIndex += 1;
                pagina.initLinkBox();
                break;
            case _btnAnterior:
                pagina.currentPageIndex -= 1;
                pagina.initLinkBox();
                break;
            case _txtNombre:
                BLL_User.pideNombre();
                break;
            case _txtApellidos:
                BLL_User.pideApellidos();
                break;
            case _txtDireccion:
                BLL_User.pideDirec();
                break;
            case _txtDNI:
                BLL_User.pideDNI();
                break;
            case _txtTlfno:
                BLL_User.pideTlfno();
                break;
            case _txtCorreo:
                BLL_User.pideCorreo();
                break;
            case _txtPass:
                BLL_User.pidePass();
                break;
            case _txtConfirmPass:
                BLL_User.pideConfirmPass();
                break;
            case _txtPass1:
                BLL_User.cambiaPass();
                break;
            case _txtConfirmPass1:
                BLL_User.cambiaConfirmPass();
                break;
            case _txtUser:
                BLL_User.pideUserName();
                break;
            case _txtUser1:
                BLL_User.cambiaUserName();
                break;
            case _txtNombre1:
                BLL_User.cambiaNombre();
                break;
            case _txtApellidos1:
                BLL_User.cambiaApellidos();
                break;
            case _txtDireccion1:
                BLL_User.cambiaDirec();
                break;
            case _txtTlfno1:
                BLL_User.cambiaTlfno();
                break;
            case _txtCorreo1:
                BLL_User.cambiaCorreo();
                break;

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (Action.valueOf(e.getComponent().getName())) {

            case _txtNombre:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearUser.txtApellidos.setText("");
                    frmCrearUser.txtApellidos.requestFocus();
                    cont[0] = 1;
                    frmCrearUser.labCorrecto.setVisible(false);
                }
                break;
            case _txtApellidos:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearUser.txtDireccion.setText("");
                    frmCrearUser.txtDireccion.requestFocus();
                    cont[1] = 1;
                    frmCrearUser.labCorrecto.setVisible(false);
                }
                break;
            case _txtDireccion:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearUser.txtDNI.setText("");
                    frmCrearUser.txtDNI.requestFocus();
                    cont[2] = 1;
                    frmCrearUser.labCorrecto.setVisible(false);
                }
                break;
            case _txtDNI:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearUser.txtTlfno.setText("");
                    frmCrearUser.txtTlfno.requestFocus();
                    cont[3] = 1;
                    frmCrearUser.labCorrecto.setVisible(false);
                }
                break;
            case _txtTlfno:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearUser.txtCorreo.setText("");
                    frmCrearUser.txtCorreo.requestFocus();
                    cont[4] = 1;
                    frmCrearUser.labCorrecto.setVisible(false);
                }
                break;
            case _txtCorreo:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearUser.txtUser.setText("");
                    frmCrearUser.txtUser.requestFocus();
                    cont[5] = 1;
                    frmCrearUser.labCorrecto.setVisible(false);
                }
                break;
            case _txtUser:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearUser.txtPass.setText("");
                    frmCrearUser.txtPass.requestFocus();
                    cont[8] = 1;
                    frmCrearUser.labCorrecto.setVisible(false);
                }
                break;
            case _txtPass:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    frmCrearUser.txtConfirmPass.setText("");
                    frmCrearUser.txtConfirmPass.requestFocus();
                    cont[6] = 1;
                    cont[7] = 1;
                    frmCrearUser.labCorrecto.setVisible(false);
                }
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (Action.valueOf(e.getComponent().getName())) {

            case _txtNombre:
                BLL_User.pideNombre();
                break;
            case _txtApellidos:
                BLL_User.pideApellidos();
                break;
            case _txtDireccion:
                BLL_User.pideDirec();
                break;
            case _txtDNI:
                BLL_User.pideDNI();
                break;
            case _txtTlfno:
                BLL_User.pideTlfno();
                break;
            case _txtCorreo:
                BLL_User.pideCorreo();
                break;
            case _txtUser:
                BLL_User.pideUserName();
                break;
            case _txtPass:
                BLL_User.pidePass();
                break;
            case _txtConfirmPass:
                BLL_User.pideConfirmPass();
                break;
            case _txtUser1:
                BLL_User.cambiaUserName();
                break;
            case _txtPass1:
                BLL_User.cambiaPass();
                break;
            case _txtConfirmPass1:
                BLL_User.cambiaConfirmPass();
                break;
            case _txtNombre1:
                BLL_User.cambiaNombre();
                break;
            case _txtApellidos1:
                BLL_User.cambiaApellidos();
                break;
            case _txtDireccion1:
                BLL_User.cambiaDirec();
                break;
            case _txtTlfno1:
                BLL_User.cambiaTlfno();
                break;
            case _txtCorreo1:
                BLL_User.cambiaCorreo();
                break;

        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        switch (Action.valueOf(e.getComponent().getName())) {
            case _labChangeAvatar:
                FileUpload.pintar_guardar_imag(this.frmUpdateUser.labPrintAvatar, 90, 70);
                break;
            case _labConfig:
                /**
                 * Activar config
                 */
                this.frmPagerUser.dispose();
                new EmpF_Controlador(new Configuration(), 3).iniciar(3);
                JPanel p3 = new JPanel();
                p3.add(EmpF_Controlador.frmConfig.pnlConfig);
                Controlador_Login.frmInicio.internalFrame.setContentPane(p3);
                break;
            case _labMaxmin:
                BLLuser_DB usr = new BLLuser_DB();
                usr.userMenorMayorBLL();
                break;
            case _labEdadMedia:
                BLLuser_DB us3r = new BLLuser_DB();
                us3r.EdadMediaBLL();
                break;
            case _labJSON:
                BLL_User_Fitxers.GuardarJSON();
                break;
            case _labXML:
                BLL_User_Fitxers.GuardarXML();
                break;
            case _labTXT:
                BLL_User_Fitxers.GuardarTXT();
                break;
            case _labCreate:
                /**
                 * Activar crear
                 */
                this.frmPagerUser.dispose();
                new Controlador_User(new CrearUser(), 0).iniciar(0);
                JPanel p1 = new JPanel();
                p1.add(this.frmCrearUser.pnlEmpfCrear);
                Controlador_Login.frmInicio.internalFrame.setContentPane(p1);
                break;
            case _labUpdate:
                int correcto = BLL_User.updateUsuario();
                if (correcto != 0) {
                    String emailOld = SingletonUser.usr.getCorreo();

                    /**
                     * Activar config
                     */
                    this.frmPagerUser.dispose();
                    new Controlador_User(new UpdateUser(), 1).iniciar(1);
                    JPanel p2 = new JPanel();
                    p2.add(this.frmUpdateUser.pnlEmpfMod);
                    Controlador_Login.frmInicio.internalFrame.setContentPane(p2);
                }
                break;
            case _labDelete:
                BLL_User.deleteUser();
                break;
            case _txtNombre:
                if (cont[0] == 0) {
                    frmCrearUser.txtNombre.setText("");
                    cont[0]++;
                }
                if (frmCrearUser.labErrorNom.isVisible() == true) {
                    frmCrearUser.txtNombre.setText("");
                }

                frmCrearUser.labCorrecto.setVisible(false);
                break;
            case _txtApellidos:
                if (cont[1] == 0) {
                    frmCrearUser.txtApellidos.setText("");
                    cont[1]++;
                }
                if (frmCrearUser.labErrorApellido.isVisible() == true) {
                    frmCrearUser.txtApellidos.setText("");
                }

                frmCrearUser.labCorrecto.setVisible(false);
                break;
            case _txtDireccion:
                if (cont[2] == 0) {
                    frmCrearUser.txtDireccion.setText("");
                    cont[2]++;
                }
                if (frmCrearUser.labErrorDirec.isVisible() == true) {
                    frmCrearUser.txtDireccion.setText("");
                }

                frmCrearUser.labCorrecto.setVisible(false);
                break;
            case _txtDNI:
                if (cont[3] == 0) {
                    frmCrearUser.txtDNI.setText("");
                    cont[3]++;
                }
                if (frmCrearUser.labErrorDNI.isVisible() == true) {
                    frmCrearUser.txtDNI.setText("");
                }

                frmCrearUser.labCorrecto.setVisible(false);
                break;
            case _txtTlfno:
                if (cont[4] == 0) {
                    frmCrearUser.txtTlfno.setText("");
                    cont[4]++;
                }
                if (frmCrearUser.labErrorTelefono.isVisible() == true) {
                    frmCrearUser.txtTlfno.setText("");
                }

                frmCrearUser.labCorrecto.setVisible(false);
                break;
            case _txtCorreo:
                if (cont[5] == 0) {
                    frmCrearUser.txtCorreo.setText("");
                    cont[5]++;
                }
                if (frmCrearUser.labErrorCorreo.isVisible() == true) {
                    frmCrearUser.txtCorreo.setText("");
                }

                frmCrearUser.labCorrecto.setVisible(false);
                break;

            case _txtPass:
                if (cont[6] == 0) {
                    frmCrearUser.txtPass.setText("");
                    cont[6]++;
                }
                if (frmCrearUser.labErrorPass.isVisible() == true) {
                    frmCrearUser.txtPass.setText("");
                }

                frmCrearUser.labCorrecto.setVisible(false);
                break;
            case _txtConfirmPass:
                if (cont[7] == 0) {
                    frmCrearUser.txtConfirmPass.setText("");
                    cont[7]++;
                }
                if (frmCrearUser.labErrorConfirmPass.isVisible() == true) {
                    frmCrearUser.txtConfirmPass.setText("");
                }

                frmCrearUser.labCorrecto.setVisible(false);
                break;
            case _txtPass1:
                if (cont[6] == 0) {
                    frmUpdateUser.txtPass.setText("");
                    cont[6]++;
                }
                if (frmUpdateUser.labErrorPass.isVisible() == true) {
                    frmUpdateUser.txtPass.setText("");
                }

                frmUpdateUser.labUpdate.setVisible(false);
                break;
            case _txtConfirmPass1:
                if (cont[7] == 0) {
                    frmUpdateUser.txtConfirmPass.setText("");
                    cont[7]++;
                }
                if (frmUpdateUser.labErrorConfirmPass.isVisible() == true) {
                    frmUpdateUser.txtConfirmPass.setText("");
                }

                frmUpdateUser.labUpdate.setVisible(false);
                break;

            case _txtUser:
                if (cont[8] == 0) {
                    frmUpdateUser.txtUser.setText("");
                    cont[8]++;
                }
                if (frmUpdateUser.labErrorUsuario.isVisible() == true) {
                    frmUpdateUser.txtUser.setText("");
                }

                frmUpdateUser.labUpdate.setVisible(false);
                break;
            case _txtUser1:
                if (cont[8] == 0) {
                    frmUpdateUser.txtUser.setText("");
                    cont[8]++;
                }
                if (frmUpdateUser.labErrorUsuario.isVisible() == true) {
                    frmUpdateUser.txtUser.setText("");
                }

                frmUpdateUser.labUpdate.setVisible(false);
                break;

            case _txtNombre1:
                if (cont[0] == 0) {
                    frmUpdateUser.txtNombre.setText("");
                    cont[0]++;
                }
                if (frmUpdateUser.labErrorNom.isVisible() == true) {
                    frmUpdateUser.txtNombre.setText("");
                }

                frmUpdateUser.labUpdate.setVisible(false);
                break;
            case _txtApellidos1:
                if (cont[1] == 0) {
                    frmUpdateUser.txtApellidos.setText("");
                    cont[1]++;
                }
                if (frmUpdateUser.labErrorApellido.isVisible() == true) {
                    frmUpdateUser.txtApellidos.setText("");
                }

                frmUpdateUser.labUpdate.setVisible(false);
                break;
            case _txtDireccion1:
                if (cont[2] == 0) {
                    frmUpdateUser.txtDireccion.setText("");
                    cont[2]++;
                }
                if (frmUpdateUser.labErrorDirec.isVisible() == true) {
                    frmUpdateUser.txtDireccion.setText("");
                }

                frmUpdateUser.labUpdate.setVisible(false);
                break;
            case _txtTlfno1:
                if (cont[4] == 0) {
                    frmUpdateUser.txtTlfno.setText("");
                    cont[4]++;
                }
                if (frmUpdateUser.labErrorTelefono.isVisible() == true) {
                    frmUpdateUser.txtTlfno.setText("");
                }

                frmUpdateUser.labUpdate.setVisible(false);
                break;
            case _txtCorreo1:
                if (cont[5] == 0) {
                    frmUpdateUser.txtCorreo.setText("");
                    cont[5]++;
                }
                if (frmUpdateUser.labErrorCorreo.isVisible() == true) {
                    frmUpdateUser.txtCorreo.setText("");
                }

                frmUpdateUser.labUpdate.setVisible(false);
                break;

            case _labActualizar:
                BLL_User.cambiaUser();
                break;
            case _labAnadir:
                if (Controlador_Login.conectado == false) {
                    if (BLL_User.validateUser() == true) {
                        System.out.print(Singleton.confirmToken);
                        /**
                         * Activar ventanaConfirm
                         */
                        //JOptionPane.showMessageDialog(null,ArraylistEF.ef.toString());
                        Controlador_Login.frmSignIn.dispose();
                        new Controlador_Login(new VentanaConfirm(), 2).Iniciar(2);
                        JPanel p2 = new JPanel();
                        p2.add(Controlador_Login.frmConfirm.pnlConfirmUser);
                        Controlador_Login.frmInicio.internalFrame.setContentPane(p2);

                    }
                } else {
                    BLL_User.pideUser();
                }
                break;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        switch (Action.valueOf(e.getComponent().getName())) {
            case _labCreate:
                ImageIcon icon = new ImageIcon("src/TPV/imag/anadir.png");
                Image img = icon.getImage();
                ImageIcon newIcon = new ImageIcon(img);
                frmPagerUser.labCreate.setIcon(newIcon);
                break;
            case _labUpdate:
                ImageIcon icon1 = new ImageIcon("src/TPV/imag/actualizar.png");
                Image img1 = icon1.getImage();
                ImageIcon newIcon1 = new ImageIcon(img1);
                frmPagerUser.labUpdate.setIcon(newIcon1);
                break;
            case _labDelete:
                ImageIcon icon2 = new ImageIcon("src/TPV/imag/borrar.png");
                Image img2 = icon2.getImage();
                ImageIcon newIcon2 = new ImageIcon(img2);
                frmPagerUser.labDelete.setIcon(newIcon2);
                break;
            case _labConfig:
                ImageIcon icon3 = new ImageIcon("src/TPV/imag/config.png");
                Image img3 = icon3.getImage();
                ImageIcon newIcon3 = new ImageIcon(img3);
                frmPagerUser.labConfig.setIcon(newIcon3);
                break;
            case _labJSON:
                ImageIcon icon4 = new ImageIcon("src/TPV/imag/json.png");
                Image img4 = icon4.getImage();
                ImageIcon newIcon4 = new ImageIcon(img4);
                frmPagerUser.labJSON.setIcon(newIcon4);
                break;
            case _labXML:
                ImageIcon icon5 = new ImageIcon("src/TPV/imag/xml.png");
                Image img5 = icon5.getImage();
                ImageIcon newIcon5 = new ImageIcon(img5);
                frmPagerUser.labXML.setIcon(newIcon5);
                break;
            case _labTXT:
                ImageIcon icon6 = new ImageIcon("src/TPV/imag/txt.png");
                Image img6 = icon6.getImage();
                ImageIcon newIcon6 = new ImageIcon(img6);
                frmPagerUser.labTXT.setIcon(newIcon6);
                break;

            case _labActualizar:
                frmUpdateUser.labActualizar.setText("<html><font color=green>Actualizar</font></html>");
                break;
            case _labAnadir:
                frmCrearUser.labAnadir.setText("<html><font color=green>Añadir</font></html>");
                break;

            case _labEdadMedia:
                frmPagerUser.labEdadMedia.setText("<html><font color=green>Edad Media</font></html>");
                break;
            case _labMaxmin:
                frmPagerUser.labMaxMin.setText("<html><font color=green>Max/Min</font></html>");
                break;
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        switch (Action.valueOf(e.getComponent().getName())) {
            case _labCreate:
                ImageIcon icon = new ImageIcon("src/TPV/imag/anadirB.png");
                Image img = icon.getImage();
                ImageIcon newIcon = new ImageIcon(img);
                frmPagerUser.labCreate.setIcon(newIcon);
                break;
            case _labUpdate:
                ImageIcon icon1 = new ImageIcon("src/TPV/imag/actualizarB.png");
                Image img1 = icon1.getImage();
                ImageIcon newIcon1 = new ImageIcon(img1);
                frmPagerUser.labUpdate.setIcon(newIcon1);
                break;
            case _labDelete:
                ImageIcon icon2 = new ImageIcon("src/TPV/imag/borrarB.png");
                Image img2 = icon2.getImage();
                ImageIcon newIcon2 = new ImageIcon(img2);
                frmPagerUser.labDelete.setIcon(newIcon2);
                break;
            case _labConfig:
                ImageIcon icon3 = new ImageIcon("src/TPV/imag/configB.png");
                Image img3 = icon3.getImage();
                ImageIcon newIcon3 = new ImageIcon(img3);
                frmPagerUser.labConfig.setIcon(newIcon3);
                break;
            case _labJSON:
                ImageIcon icon4 = new ImageIcon("src/TPV/imag/JSONB.png");
                Image img4 = icon4.getImage();
                ImageIcon newIcon4 = new ImageIcon(img4);
                frmPagerUser.labJSON.setIcon(newIcon4);
                break;
            case _labXML:
                ImageIcon icon5 = new ImageIcon("src/TPV/imag/XMLB.png");
                Image img5 = icon5.getImage();
                ImageIcon newIcon5 = new ImageIcon(img5);
                frmPagerUser.labXML.setIcon(newIcon5);
                break;
            case _labTXT:
                ImageIcon icon6 = new ImageIcon("src/TPV/imag/TXTB.png");
                Image img6 = icon6.getImage();
                ImageIcon newIcon6 = new ImageIcon(img6);
                frmPagerUser.labTXT.setIcon(newIcon6);
                break;

            case _labActualizar:
                frmUpdateUser.labActualizar.setText("<html><font color=darkGray>Actualizar</font></html>");
                break;
            case _labAnadir:
                frmCrearUser.labAnadir.setText("<html><font color=darkGray>Añadir</font></html>");
                break;
            case _labEdadMedia:
                frmPagerUser.labEdadMedia.setText("<html><font color=darkGray>Edad Media</font></html>");
                break;
            case _labMaxmin:
                frmPagerUser.labMaxMin.setText("<html><font color=darkGray>Max/Min</font></html>");
                break;
        }
    }

}
