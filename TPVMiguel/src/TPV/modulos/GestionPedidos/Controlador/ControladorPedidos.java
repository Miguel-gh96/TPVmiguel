/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.GestionPedidos.Controlador;

import TPV.modulos.GestionPedidos.Modelo.BLL.BLL_Ped_Fitxers;
import TPV.modulos.GestionPedidos.Modelo.BLL.PedidosBLL;
import TPV.modulos.GestionPedidos.Modelo.Clases.MiniSimpleTableModel_Ped;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import TPV.modulos.GestionPedidos.Modelo.Clases.Pedidos;
import TPV.modulos.GestionPedidos.Modelo.Clases.RenderizaPed;
import TPV.modulos.GestionPedidos.Modelo.Vista.PagerPed;
import TPV.modulos.GestionPedidos.Modelo.Vista.VentanaPed;
import TPV.modulos.GestionPedidos.autocomplete.AutocompleteJComboBox;
import TPV.modulos.GestionPedidos.autocomplete.StringSearchable;
import TPV.modulos.GestionPedidos.pager.pagina;
import TPV.modulos.moduleProducts.Modelo.Clases.SingletonProd;
import java.awt.Image;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import TPV.modulos.GestionPedidos.Modelo.Clases.SingletonPed;
import TPV.modulos.moduleLogin.controlador.Controlador_Login;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.plaf.basic.BasicInternalFrameUI;

/**
 *
 * @author Jandro
 */
public class ControladorPedidos implements ActionListener, KeyListener, MouseListener{
    public static PagerPed Pedidos = new PagerPed();
    public static VentanaPed frmVentPed = new VentanaPed();
    public static TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(new MiniSimpleTableModel_Ped());
    public static AutocompleteJComboBox combo = null;
    
    public ControladorPedidos(JFrame Pedidos, int i){
        if(i==0){
            this.Pedidos=(PagerPed)Pedidos;
        }
        
        if (i == 1){
            this.frmVentPed = (VentanaPed)Pedidos;
        }
        
    }
    
    public enum Accion{
        //Acciones del PagerPed
        _jComboBox1,
        _labJSON,
        _labXML,
        _labTXT,
        _btnPrimero,
        _btnSiguiente,
        _btnAnterior,
        _btnUltimo,
        
        //Acciones de VentanaPed
        _labAceptar
        
    }
    
    public void Iniciar(int i){
        if (i==0){
            
            /**
             * Filtro para buscar empleados por nombre + automcompletar
             */
            List<String> myWords = new ArrayList<String>();
            for (int j = 0; j <= SingletonPed.ped.size() - 1; j++) {
                myWords.add(SingletonPed.ped.get(j).getID());
            }

            StringSearchable searchable = new StringSearchable(myWords);
            combo = new AutocompleteJComboBox(searchable);
            Pedidos.panelFiltro.setLayout(new java.awt.BorderLayout());
            Pedidos.panelFiltro.add(combo);

            combo.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboActionPerformed(evt);
                }
            });

            
            

            /**
             * Cargar tabla de empleados fijos
             */
            Pedidos.tablaFijos.setModel(new MiniSimpleTableModel_Ped());
            ((MiniSimpleTableModel_Ped) Pedidos.tablaFijos.getModel()).cargar();
            Pedidos.tablaFijos.setFillsViewportHeight(true);
            Pedidos.tablaFijos.setRowSorter(sorter);
            RenderizaPed renderizador = new RenderizaPed();
            Pedidos.tablaFijos.setDefaultRenderer(String.class, renderizador);
            Pedidos.txtCaja.setEditable(false);

            pagina.inicializa();
            pagina.initLinkBox();

            /**
             * Componentes ActionPerformed
             */
            this.Pedidos.jComboBox1.setActionCommand("_jComboBox1");
            this.Pedidos.jComboBox1.addActionListener(this);

            this.Pedidos.btnPrimero.setActionCommand("_btnPrimero");
            this.Pedidos.btnPrimero.addActionListener(this);

            this.Pedidos.btnUltimo.setActionCommand("_btnUltimo");
            this.Pedidos.btnUltimo.addActionListener(this);

            this.Pedidos.btnSiguiente.setActionCommand("_btnSiguiente");
            this.Pedidos.btnSiguiente.addActionListener(this);

            this.Pedidos.btnAnterior.setActionCommand("_btnAnterior");
            this.Pedidos.btnAnterior.addActionListener(this);

            /**
             * Complementos Mouse Event
             */

            this.Pedidos.labJSON.setName("_labJSON");
            this.Pedidos.labJSON.addMouseListener(this);

            this.Pedidos.labXML.setName("_labXML");
            this.Pedidos.labXML.addMouseListener(this);

            this.Pedidos.labTXT.setName("_labTXT");
            this.Pedidos.labTXT.addMouseListener(this);

            
        }
        
        if (i == 1){
            
            /**
             * Estructura del frame
             */
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception e) {
            }

            this.frmVentPed.setLocationRelativeTo(null);
            this.frmVentPed.setVisible(true);
            this.frmVentPed.setTitle("Bakery Miguel");
            this.frmVentPed.setResizable(false);
            Image icono = Toolkit.getDefaultToolkit().getImage("src/TPV/imag/logo.png");
            this.frmVentPed.setIconImage(icono);
            
            
            /**
             * Para salir de la aplicación
             */
            this.frmVentPed.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            this.frmVentPed.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    frmVentPed.dispose();
                }
            });
            
            /**
             * Rellenar el jcombobox con el tamaño del stock
             */
            
            String [] cont  = new String[SingletonProd.producto.getStock()];
            for (int k = 0; k < SingletonProd.producto.getStock();k++){
                cont[k] = k+1+"";

            }
            this.frmVentPed.jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(cont));
                   
            //componentes
            this.frmVentPed.labAceptar.setName("_labAceptar");
            this.frmVentPed.labAceptar.addMouseListener(this);
            
        }
        
    }
    
    public static void comboActionPerformed(java.awt.event.ActionEvent evt) {
        System.out.println("word selected: " + ((JComboBox) combo).getSelectedItem());
        pagina.currentPageIndex = 1;
        ((MiniSimpleTableModel_Ped) Pedidos.tablaFijos.getModel()).filtrar();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        switch (Accion.valueOf(ae.getActionCommand())) {
            
            case _jComboBox1:
                pagina.itemsPerPage = Integer.parseInt(Pedidos.jComboBox1.getSelectedItem().toString());
                pagina.currentPageIndex = 1;
                pagina.initLinkBox();
                break;

            case _btnPrimero:
                pagina.currentPageIndex = 1;
                pagina.initLinkBox();
                break;
            case _btnUltimo:
                pagina.currentPageIndex = pagina.maxPageIndex;
                pagina.initLinkBox();
                break;
            case _btnSiguiente:
                pagina.currentPageIndex += 1;
                pagina.initLinkBox();
                break;
            case _btnAnterior:
                pagina.currentPageIndex -= 1;
                pagina.initLinkBox();
                break;
            
        }
        
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        
        switch (Accion.valueOf(ke.getComponent().getName())) {

            
        }
        
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        
        switch (Accion.valueOf(me.getComponent().getName())) {
            
            case _labJSON:
                BLL_Ped_Fitxers.GuardarJSON();
                break;
            case _labXML:
                BLL_Ped_Fitxers.GuardarXML();
                break;
            case _labTXT:
                BLL_Ped_Fitxers.GuardarTXT();
                break;
            case _labAceptar:
                
                if(PedidosBLL.NuevoPedidos() != -1){
                    this.frmVentPed.dispose();
                    
                    new ControladorPedidos(new PagerPed(), 0).Iniciar(0);
                    JPanel p3 = new JPanel();
                    p3.add(ControladorPedidos.Pedidos.jPanel);
                    Controlador_Login.frmInicio.internalFrame.setContentPane(p3);
                    
                }
                break;
            
        }
        
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        
        switch (Accion.valueOf(me.getComponent().getName())){
            
            
            case _labJSON:
                ImageIcon icon4 = new ImageIcon("src/TPV/imag/json.png");
                Image img4 = icon4.getImage();
                ImageIcon newIcon4 = new ImageIcon(img4);
                Pedidos.labJSON.setIcon(newIcon4);
                break;
            case _labXML:
                ImageIcon icon5 = new ImageIcon("src/TPV/imag/xml.png");
                Image img5 = icon5.getImage();
                ImageIcon newIcon5 = new ImageIcon(img5);
                Pedidos.labXML.setIcon(newIcon5);
                break;
            case _labTXT:
                ImageIcon icon6 = new ImageIcon("src/TPV/imag/txt.png");
                Image img6 = icon6.getImage();
                ImageIcon newIcon6 = new ImageIcon(img6);
                Pedidos.labTXT.setIcon(newIcon6);
                break;
            case _labAceptar:
                this.frmVentPed.labAceptar.setText("<html><font color=green>Aceptar</font></html>");
                break;
            
        }
    }

    @Override
    public void mouseExited(MouseEvent me) {
        
        switch (Accion.valueOf(me.getComponent().getName())){
            
            case _labJSON:
                ImageIcon icon4 = new ImageIcon("src/TPV/imag/JSONB.png");
                Image img4 = icon4.getImage();
                ImageIcon newIcon4 = new ImageIcon(img4);
                Pedidos.labJSON.setIcon(newIcon4);
                break;
            case _labXML:
                ImageIcon icon5 = new ImageIcon("src/TPV/imag/XMLB.png");
                Image img5 = icon5.getImage();
                ImageIcon newIcon5 = new ImageIcon(img5);
                Pedidos.labXML.setIcon(newIcon5);
                break;
            case _labTXT:
                ImageIcon icon6 = new ImageIcon("src/TPV/imag/TXTB.png");
                Image img6 = icon6.getImage();
                ImageIcon newIcon6 = new ImageIcon(img6);
                Pedidos.labTXT.setIcon(newIcon6);
                break;
            case _labAceptar:
                this.frmVentPed.labAceptar.setText("<html><font color=black>Aceptar</font></html>");
                break;
        }
    }
    
}
