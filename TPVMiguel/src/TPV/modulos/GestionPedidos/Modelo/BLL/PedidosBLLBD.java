/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.GestionPedidos.Modelo.BLL;

import TPV.clases.ConexionBD;
import java.sql.Connection;
import javax.swing.JOptionPane;
import TPV.modulos.GestionPedidos.Modelo.Clases.Pedidos;
import TPV.modulos.GestionPedidos.Modelo.DAO.PedidosDAOBD;


/**
 *
 * @author Adry
 */
public class PedidosBLLBD {
    
    
     public int nuevoPedidoBLL(Pedidos _pedido) {
        int resultado=0;
        Connection _con = null;
        Pedidos _nuevoPedidos = null;
        ConexionBD _conexion_DB = new ConexionBD();
		
        _con = _conexion_DB.AbrirConexion();
        PedidosDAOBD _pedidosDAO = new PedidosDAOBD();
        
        resultado = _pedidosDAO.nuevoPedidoDAO(_con, _pedido);
        
        _conexion_DB.CerrarConexion(_con);
        return resultado;
    }
    //obtener un arraylist con todos los pedidos disponibles

    public void listAllPedidosBLL() {
        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
		
        _con = _conexion_DB.AbrirConexion();
        PedidosDAOBD _pedidosDAO = new PedidosDAOBD();
        try {
            _pedidosDAO.listAllPedidosDAO(_con);//Recuperamos los usuarios       
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
        }
        _conexion_DB.CerrarConexion(_con);
    }
    
    
    
    
}
