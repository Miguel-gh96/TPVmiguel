/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.GestionPedidos.Modelo.BLL;


import TPV.modulos.GestionPedidos.Modelo.DAO.DAO_Ped_Fitxers;
import TPV.modulos.gestionPers.gestionEF.modelo.DAO.EfDAOfitxers;

/**
 *
 * @author miguel
 */
public class BLL_Ped_Fitxers {
    
    
    public static void GuardarJSON() {
        PedidosBLLBD _ped = new PedidosBLLBD();
        _ped.listAllPedidosBLL();
        DAO_Ped_Fitxers.GuardarJSON();
    }
    
    public static void GuardarXML() {
        PedidosBLLBD _ped = new PedidosBLLBD();
        _ped.listAllPedidosBLL();
        DAO_Ped_Fitxers.GuardarXML();
    }
    
    public static void GuardarTXT() {
        PedidosBLLBD _ped = new PedidosBLLBD();
        _ped.listAllPedidosBLL();
        DAO_Ped_Fitxers.GuardarTXT();
    }
    
	
    
    
}
