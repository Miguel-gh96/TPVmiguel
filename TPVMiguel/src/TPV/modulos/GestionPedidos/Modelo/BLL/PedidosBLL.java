/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.GestionPedidos.Modelo.BLL;

import javax.swing.JOptionPane;
import TPV.modulos.GestionPedidos.Modelo.Clases.Pedidos;
import TPV.modulos.GestionPedidos.Modelo.Clases.SingletonPed;
import TPV.modulos.GestionPedidos.Modelo.Vista.PagerPed;
import TPV.modulos.GestionPedidos.Modelo.Vista.VentanaPed;
import TPV.modulos.gestionPers.gestionUser.modelo.BLL.BLLuser_DB;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.Usuario;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import TPV.modulos.moduleProducts.Controlador.Controlador_Prod;
import TPV.modulos.moduleProducts.Modelo.BLL.BLL_Prod;
import TPV.modulos.moduleProducts.Modelo.Clases.Productos;
import TPV.modulos.moduleProducts.Modelo.Clases.SingletonProd;
import java.util.Random;

/**
 *
 * @author pollotorrao
 */
public class PedidosBLL {
    
    public static int NuevoPedidos(){
        
        
        
         PedidosBLLBD ped1=new PedidosBLLBD();
                    ped1.listAllPedidosBLL();
        int cant = Integer.parseInt(VentanaPed.jComboBox1.getSelectedItem().toString());
        Usuario cli=Singleton.efLog;
        Productos prod= SingletonProd.producto;
        
        
        Pedidos _ped = new Pedidos();
          
        _ped.setID_pedido(SingletonPed.ped.size()+1);
        _ped.setID(prod.getID_Producto());
        _ped.setDNI(cli.getNif());
        _ped.setCantidad(cant);
        _ped.setImporte(cant*prod.getPrecio());      
        
        PedidosBLLBD ped = new PedidosBLLBD();
        
        int resultado=ped.nuevoPedidoBLL(_ped);
        
        if(resultado==-1){
            JOptionPane.showMessageDialog(null, "No se ha podido realizar");
                }else{
                    JOptionPane.showMessageDialog(null, "Pedido realizado");
                    ped.listAllPedidosBLL(); 
                    BLLuser_DB cli2=new BLLuser_DB();
                    cli2.listAllUserBLL();
                    return 1;
                }
        
                
           return -1; 
    }
    
    public static String getCadenaAleatoria1 (int longitud){
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while ( i < longitud){
			char c = (char)r.nextInt(255);
			if ( (c >= 'A' && c <='Z') || (c >='a' && c <='z') ){
				cadenaAleatoria += c;
				i ++;
			}
		}
		return cadenaAleatoria;
    }
    
}
