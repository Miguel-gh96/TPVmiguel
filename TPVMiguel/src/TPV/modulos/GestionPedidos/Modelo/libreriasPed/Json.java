package TPV.modulos.GestionPedidos.Modelo.libreriasPed;



import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import TPV.librerias.Funciones;
import TPV.modulos.GestionPedidos.Modelo.Clases.Pedidos;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import TPV.modulos.GestionPedidos.Modelo.Clases.SingletonPed;

public class Json {

	public static void generajsonPed() {// Guarda Json de pedidos
		String PATH = null;

		try {
			XStream xstreamjson = new XStream(new JettisonMappedXmlDriver());
			xstreamjson.setMode(XStream.NO_REFERENCES);
			xstreamjson.alias("Pedidos", Pedidos.class);

			JFileChooser fileChooser = new JFileChooser();
			
			 fileChooser.setAcceptAllFileFilterUsed(false);
			 fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("JSON (*.json)", "json"));
			
			int seleccion = fileChooser.showSaveDialog(null);
			if (seleccion == JFileChooser.APPROVE_OPTION) {
				File JFC = fileChooser.getSelectedFile();
				PATH = JFC.getAbsolutePath();
				PATH = PATH + ".json";

				Gson gson1 = new Gson();
				String json = gson1.toJson(SingletonPed.ped);
				FileWriter fileXml = new FileWriter(PATH);
				fileXml.write(json.toString());
				fileXml.close();

				Funciones.resultado("Archivo JSON guardado con exito", "Archivo JSON");
			}

		} catch (Exception e) {
			Funciones.resultado("Archivo JSON guardado con exito", "Archivo JSON");
		}

	}

	
	 
	 
	 
}
