/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.GestionPedidos.Modelo.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static javax.swing.DropMode.INSERT;
import javax.swing.JOptionPane;
import TPV.modulos.GestionPedidos.Modelo.Clases.Pedidos;
import TPV.modulos.GestionPedidos.Modelo.Clases.SingletonPed;

/**
 *
 * @author Adry
 */
public class PedidosDAOBD {
    

    
    public int nuevoPedidoDAO(Connection con, Pedidos ped) {
        PreparedStatement stmt = null;
        int resultado=0;
        try {
            stmt = con.prepareStatement("INSERT INTO TPV.Pedidos"
                    + "(ID_Pedidos,ID_Producto, DNI, Cantidad, Importe) "
                    + "VALUES(?,?,?,?,?)");
            stmt.setInt(1,ped.getID_pedido());
            stmt.setString(2, ped.getID());
            stmt.setString(3, ped.getDNI());
            stmt.setInt(4, ped.getCantidad());
            stmt.setFloat(5, ped.getImporte());
            //JOptionPane.showMessageDialog(null, ped);
            resultado=stmt.executeUpdate();
           
            
           
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un problema al insertar un nuevo pedido!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }
        return resultado;
    }

   public void listAllPedidosDAO(Connection con) {
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        SingletonPed.ped.clear();
        try {
            stmt = con.prepareStatement("SELECT * FROM TPV.Pedidos");
            rs = stmt.executeQuery();
            Pedidos _pedido = null;
            while (rs.next()) {
                
                _pedido = new Pedidos();
                _pedido.setID_pedido(rs.getInt("ID_Pedidos"));
                _pedido.setID(rs.getString("ID_Producto"));
                _pedido.setDNI(rs.getString("DNI"));
                _pedido.setCantidad(rs.getInt("Cantidad"));
                _pedido.setImporte(rs.getFloat("Importe"));
                SingletonPed.ped.add(_pedido);  
            }
            

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un problema al obtener los pedidos!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }

    }
    
    
    
}
