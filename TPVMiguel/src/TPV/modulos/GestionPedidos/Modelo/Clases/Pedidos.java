/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.GestionPedidos.Modelo.Clases;

import java.io.Serializable;


/**
 *
 * @author pollotorrao
 */
public class Pedidos {
    
    private int ID_pedido;
    private String DNI;
    private String ID;
    private int Cantidad;
    private float Importe;
    
    public Pedidos(){}
    
    public Pedidos(int id_pedido, String dni, String id, int cantidad){
        this.ID_pedido=id_pedido;
        this.DNI=dni;
        this.ID=id;
        this.Cantidad=cantidad;
    }
    
    public Pedidos(String dni, String id, int cantidad, float importe){
        this.DNI=dni;
        this.ID=id;
        this.Cantidad=cantidad;
        this.Importe=importe;
    }

    public float getImporte() {
        return Importe;
    }

    public void setImporte(float Importe) {
        this.Importe = Importe;
    }

    
    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public int getID_pedido() {
        return ID_pedido;
    }

    public void setID_pedido(int ID_pedido) {
        this.ID_pedido = ID_pedido;
    }
    
    
    
    /*public int compareTo(Pedidos p) {// Ordena los pedidos por ID
        if (this.getID_pedido().compareTo(p.getID_pedido()) > 0) {
            return 1;
        }
        if (this.getID_pedido().compareTo(p.getID_pedido()) < 0) {
            return -1;
        }
        return 0;
    }*/

   /* public boolean equals(Object p) {
        return getID_pedido().equals(((Pedidos) p).getID_pedido());
    }*/

    public String toString() {

        StringBuffer s = new StringBuffer();

        s.append("ID pedido: " + this.getID_pedido()+ "\n");
        s.append("DNI cliente: " + this.getDNI()+ "\n");
        s.append("ID pizza: " + this.getID()+ "\n");
        s.append("Cantidad: " + this.getCantidad() + "\n");
        s.append("Importe: "+this.getImporte()+"\n");
        s.append("----------------------------------------" + "\n");
        return s.toString();
    }

    
    
}
