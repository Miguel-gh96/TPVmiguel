/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.GestionPedidos.Modelo.Clases;

import static TPV.modulos.GestionPedidos.Controlador.ControladorPedidos.combo;
import TPV.modulos.GestionPedidos.Modelo.BLL.PedidosBLLBD;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.table.AbstractTableModel;
import TPV.modulos.GestionPedidos.Modelo.Clases.Pedidos;
import TPV.modulos.GestionPedidos.Modelo.Vista.PagerPed;
import TPV.modulos.GestionPedidos.pager.pagina;
import TPV.modulos.moduleProducts.Modelo.Clases.Productos;
import java.awt.Color;
import javax.swing.JComboBox;

/**
 *
 * @author pollotorrao
 */
public class MiniSimpleTableModel_Ped extends AbstractTableModel {
    public static ArrayList<Pedidos> datos = new ArrayList<Pedidos>();
    public static ArrayList<Pedidos> datosaux = new ArrayList<Pedidos>();
    
    String[] columnas = {"ID_Pedido", "ID_Producto", "Cantidad", "Importe" };

    ////////////////////estos métodos son necesarios para que jtable funcione/////////////////////
    @Override
    public String getColumnName(int col) {
        return columnas[col].toString();
    }

    //Devuelve el numero de filas
    @Override
    public int getRowCount() {
        return datos.size();
    }

    //Devuelve el numero de columnas
    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    //Devuelve el valor del objeto en la fila y columna
    @Override
    public Object getValueAt(int row, int col) {

        Object dev = null;
        Pedidos fila = (Pedidos) datos.get(row);

        switch (col) {
            case 0:
                dev = fila.getID_pedido();
                break;

            case 1:
                dev = fila.getID();
                break;

            case 2:
                dev = fila.getCantidad();
                break;
                
            case 3:
                dev = fila.getImporte();
                break;

        }
        return dev;
    }

    //Determina si una fila y columna ha de ser editable
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    //Actualiza un objeto de una fila y columna
    @Override
    public void setValueAt(Object value, int row, int col) {
        Pedidos fila = (Pedidos) datos.get(row);

        switch (col) {
            case 0:
                fila.setID_pedido((int) value);
                break;

            case 1:
                fila.setID(value.toString());
                break;
            
            case 2:
                fila.setCantidad((int) value);
                break;
                
            case 3:
                fila.setImporte((float) value);
                break;

        }
        fireTableCellUpdated(row, col);
    }

    public void addRow(Pedidos ped) {
        datos.add(ped);
        fireTableDataChanged();
    }

    public void cargar() {
        datos.clear();
        datosaux.clear();
        
        PedidosBLLBD _pedidos = new PedidosBLLBD();
        _pedidos.listAllPedidosBLL();
       
        for (int i = 0; i < SingletonPed.ped.size(); i++) {
            datosaux.add(SingletonPed.ped.get(i));
            addRow(SingletonPed.ped.get(i));
        }
    }
    
    public void filtrar() {
        datos.clear();
        int cont = 0;

        String nom = (String) ((JComboBox) combo).getSelectedItem();

        if (nom != null) {
            for (int i = 0; i < datosaux.size(); i++) {
                if (datosaux.get(i).getID().toLowerCase().startsWith(nom.toLowerCase())) {
                    addRow(datosaux.get(i));
                    cont++;
                }
            }

            if (cont > 0) {
                PagerPed.labResultado.setText(" " + cont);
                PagerPed.labResultado.setForeground(Color.green);
            } else {
                PagerPed.labResultado.setText(" " + cont);
                PagerPed.labResultado.setForeground(Color.red);
            }

            System.out.println("word selected: " + nom);
            pagina.initLinkBox();
        }
    }


    public Pedidos buscar(String u) {
        datos.clear();
        cargar();

        String res;
        for (int i = 0; i < datos.size(); i++) {
            res = datos.get(i).toString();
            if (res.contains(u)) {
                return datos.get(i);
            }
        }
        return null;
    }

    public int buscaProductos(Productos u) {
        datos.clear();
        cargar();

        for (int i = 0; i < datos.size(); i++) {
            if (datos.get(i).equals(u)) {
                return i;
            }
        }
        return -1;
    }

    public void removeRow(int fila) {
        datos.remove(fila);
        fireTableDataChanged();
    }

    
    
}
