/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleLogin.controlador;

import TPV.librerias.FileUpload;
import TPV.modulos.GestionPedidos.Controlador.ControladorPedidos;
import TPV.modulos.GestionPedidos.Modelo.Vista.PagerPed;
import TPV.modulos.gestionPers.gestionEF.controlador.EmpF_Controlador;
import TPV.modulos.gestionPers.gestionEF.vista.PagerEF;
import TPV.modulos.gestionPers.gestionUser.controlador.Controlador_User;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.SingletonUser;
import TPV.modulos.gestionPers.gestionUser.vista.CrearUser;
import TPV.modulos.gestionPers.gestionUser.vista.PagerUser;
import TPV.modulos.gestionPers.gestionUser.vista.UpdateUser;
import TPV.modulos.moduleLogin.modelo.BLL.BLL_Login;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import TPV.modulos.moduleLogin.vista.SignIn;
import TPV.modulos.moduleLogin.vista.VentanaConfirm;
import TPV.modulos.moduleLogin.vista.VentanaInicio;
import TPV.modulos.moduleLogin.vista.VentanaOlvPass;
import TPV.modulos.moduleLogin.vista.VentanaWelcome;
import TPV.modulos.moduleProducts.Controlador.Controlador_Prod;
import TPV.modulos.moduleProducts.Vista.PagerProducts;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.plaf.basic.BasicInternalFrameUI;

/**
 *
 * @author miguel
 */
public class Controlador_Login implements ActionListener, KeyListener, MouseListener {

    public static VentanaInicio frmInicio = new VentanaInicio();
    public static SignIn frmSignIn = new SignIn();
    public static VentanaConfirm frmConfirm = new VentanaConfirm();
    public static VentanaOlvPass frmOlvPass = new VentanaOlvPass();
    public static VentanaWelcome frmWelcome = new VentanaWelcome();
    public static boolean conectado = false;

    public enum Accion {

        //Acciones ventana Inicio
        _labEntrar,
        _labLogOut,
        _labEmpeados,
        _labRegistro,
        _labPrintUser,
        _labInicio,
        _labUsuarios,
        _labProductos,
        _labPedidos,
        //Acciones ventana SignIn

        _txtUser,
        _txtPass,
        _labAceptar,
        _labVolver,
        _labOlvidarCont,
        //Acciones ventana Confirm
        _labAceptarToken,
        //Acciones ventana Olvidar pass
        _labEnviar,
        _newPass,
        _txtEmail

    }

    public Controlador_Login(JFrame logeo, int i) {

        if (i == 0) {
            this.frmInicio = (VentanaInicio) logeo;
        }
        if (i == 1) {
            this.frmSignIn = (SignIn) logeo;
        }

        if (i == 2) {
            this.frmConfirm = (VentanaConfirm) logeo;
        }

        if (i == 3) {
            this.frmOlvPass = (VentanaOlvPass) logeo;
        }

        if (i == 4) {
            this.frmWelcome = (VentanaWelcome) logeo;

        }

    }

    public void Iniciar(int i) {

        if (i == 0) {

            /**
             * Estructura del frame
             */
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception e) {
            }

            ((BasicInternalFrameUI) this.frmInicio.internalFrame.getUI()).setNorthPane(null);

            this.frmInicio.setLocationRelativeTo(null);
            this.frmInicio.setVisible(true);
            this.frmInicio.setTitle("Bakery Miguel");
            this.frmInicio.setResizable(false);
            Image icono = Toolkit.getDefaultToolkit().getImage("src/TPV/imag/logo.png");
            this.frmInicio.setIconImage(icono);

            /**
             * Ventana Inicio
             */
            JPanel p3 = new JPanel();
            p3.add(Controlador_Login.frmWelcome.jPanel1);
            this.frmInicio.internalFrame.setContentPane(p3);

            /**
             * Para salir de la aplicación
             */
            this.frmInicio.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            this.frmInicio.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    if (conectado == true) {
                        new Controlador_Login(new SignIn(), 1).Iniciar(1);
                        JPanel p2 = new JPanel();
                        p2.add(Controlador_Login.frmSignIn.jPanel1);
                        Controlador_Login.frmInicio.internalFrame.setContentPane(p2);

                        /**
                         * activar y desactivar labels
                         */
                        Controlador_Login.frmInicio.labEmpleados.setVisible(false);
                        Controlador_Login.frmInicio.labUsuarios.setVisible(false);
                        Controlador_Login.frmInicio.labEntrar.setVisible(true);
                        Controlador_Login.frmInicio.labRegistro.setVisible(true);
                        Controlador_Login.frmInicio.pnlUser.setVisible(false);

                        /**
                         * Boolean para saber si el usuario esta conectado o no,
                         * se utiliza para el btn anadir del crearEF
                         */
                        conectado = false;
                    } else {
                        JOptionPane.showMessageDialog(null, "Saliendo de la aplicación");
                        frmInicio.dispose();
                        System.exit(0);
                    }
                }
            });

            /**
             * Ocultar labels
             */
            this.frmInicio.labEmpleados.setVisible(false);
            this.frmInicio.labUsuarios.setVisible(false);
            this.frmInicio.pnlUser.setVisible(false);
            this.frmInicio.labPedidos.setVisible(false);

            //Componentes de SignIn
            this.frmInicio.labEntrar.setName("_labEntrar");
            this.frmInicio.labEntrar.addMouseListener(this);

            this.frmInicio.labRegistro.setName("_labRegistro");
            this.frmInicio.labRegistro.addMouseListener(this);

            this.frmInicio.labLogOut.setName("_labLogOut");
            this.frmInicio.labLogOut.addMouseListener(this);

            this.frmInicio.labEmpleados.setName("_labEmpeados");
            this.frmInicio.labEmpleados.addMouseListener(this);

            this.frmInicio.labPrintUser.setName("_labPrintUser");
            this.frmInicio.labPrintUser.addMouseListener(this);

            this.frmInicio.labInicio.setName("_labInicio");
            this.frmInicio.labInicio.addMouseListener(this);

            this.frmInicio.labUsuarios.setName("_labUsuarios");
            this.frmInicio.labUsuarios.addMouseListener(this);
            
            this.frmInicio.labProductos.setName("_labProductos");
            this.frmInicio.labProductos.addMouseListener(this); 
            
            this.frmInicio.labPedidos.setName("_labPedidos");
            this.frmInicio.labPedidos.addMouseListener(this);        

        }

        if (i == 1) {//SignIn

            /**
             * Ocultar labels de errores
             */
            this.frmSignIn.labErrorUser.setVisible(false);
            this.frmSignIn.labErrorPass.setVisible(false);
            this.frmInicio.labEmpleados.setVisible(false);

            //Componentes de SignIn
            this.frmSignIn.txtUser.setActionCommand("_txtUser");
            this.frmSignIn.txtUser.setName("_txtUser");
            this.frmSignIn.txtUser.addActionListener(this);
            this.frmSignIn.txtUser.addKeyListener(this);
            this.frmSignIn.txtUser.addMouseListener(this);

            this.frmSignIn.txtPass.setActionCommand("_txtPass");
            this.frmSignIn.txtPass.setName("_txtPass");
            this.frmSignIn.txtPass.addActionListener(this);
            this.frmSignIn.txtPass.addKeyListener(this);
            this.frmSignIn.txtPass.addMouseListener(this);

            this.frmSignIn.labAceptar.setName("_labAceptar");
            this.frmSignIn.labAceptar.addMouseListener(this);

            this.frmSignIn.LabOlvidarCont.setName("_labOlvidarCont");
            this.frmSignIn.LabOlvidarCont.addMouseListener(this);

        }

        if (i == 2) {//ventana confirmar
            //Ocultar label error
            this.frmConfirm.labErrorToken.setVisible(false);

            //Componentes de Comfirmar
            this.frmConfirm.labAceptarToken.setName("_labAceptarToken");
            this.frmConfirm.labAceptarToken.addMouseListener(this);

        }

        if (i == 3) {//ventana olvidar password

            //Ocultar label error
            this.frmOlvPass.labErrorEmail.setVisible(false);
            this.frmOlvPass.labErrorNewPass.setVisible(false);

            //Componentes de olvidar pass
            this.frmOlvPass.labEnviar.setName("_labEnviar");
            this.frmOlvPass.labEnviar.addMouseListener(this);

            this.frmOlvPass.passNew.setActionCommand("_newPass");
            this.frmOlvPass.passNew.setName("_newPass");
            this.frmOlvPass.passNew.addActionListener(this);
            this.frmOlvPass.passNew.addKeyListener(this);
            this.frmOlvPass.passNew.addMouseListener(this);

            this.frmOlvPass.txtEmail.setActionCommand("_txtEmail");
            this.frmOlvPass.txtEmail.setName("_txtEmail");
            this.frmOlvPass.txtEmail.addActionListener(this);
            this.frmOlvPass.txtEmail.addKeyListener(this);
            this.frmOlvPass.txtEmail.addMouseListener(this);

        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        switch (Accion.valueOf(e.getComponent().getName())) {

            case _txtUser:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    this.frmSignIn.txtPass.requestFocus();
                    this.frmSignIn.txtPass.setText("");
                }
                break;
            case _txtEmail:
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    this.frmOlvPass.passNew.requestFocus();
                    this.frmOlvPass.passNew.setText("");
                }
                break;
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (Accion.valueOf(e.getComponent().getName())) {

            case _newPass:
                BLL_Login.pideNewPass();
                break;
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        switch (Accion.valueOf(e.getComponent().getName())) {
            
            case _labProductos:
                new Controlador_Prod(new PagerProducts(),0).Iniciar(0);
                JPanel p0 = new JPanel();
                p0.add(Controlador_Prod.frmProductos.jPanel);
                this.frmInicio.internalFrame.setContentPane(p0);
                break;
            case _labPrintUser:
                SingletonUser.usr = Singleton.efLog;
                /**
                 * Activar updateef
                 */

                new Controlador_User(new UpdateUser(), 1).iniciar(1);
                JPanel p1 = new JPanel();
                p1.add(Controlador_User.frmUpdateUser.pnlEmpfMod);
                this.frmInicio.internalFrame.setContentPane(p1);

                //desactivar combobox admin/user
                Controlador_User.frmUpdateUser.ComboxTipo.setVisible(false);

                break;
            case _labAceptarToken:
                BLL_Login.validateToken();
                break;
            case _labEmpeados:
                /**
                 * Activar pager
                 */
                this.frmSignIn.dispose();
                new EmpF_Controlador(new PagerEF(), 0).iniciar(0);
                JPanel p3 = new JPanel();
                p3.add(EmpF_Controlador.frmPageref.jPanel);
                this.frmInicio.internalFrame.setContentPane(p3);
                break;
            case _labEntrar:
                new Controlador_Login(new SignIn(), 1).Iniciar(1);

                JPanel p = new JPanel();
                p.add(this.frmSignIn.jPanel1);
                this.frmInicio.internalFrame.setContentPane(p);
                break;
            case _labRegistro:
                new Controlador_User(new CrearUser(), 0).iniciar(0);

                JPanel p4 = new JPanel();
                p4.add(Controlador_User.frmCrearUser.pnlEmpfCrear);
                this.frmInicio.internalFrame.setContentPane(p4);

                //desactivar combobox admin/user
                Controlador_User.frmCrearUser.ComboxTipo.setVisible(false);

                break;
            case _labAceptar:
                BLL_Login _log = new BLL_Login();
                if (_log.login() == true) {

                    /**
                     * Rellenar datos logueo
                     */
                    this.frmInicio.labPrintUser.setText(Singleton.efLog.getLogin());
                    FileUpload.pintar(this.frmInicio.labAvatar, 60, 40, 1, Singleton.efLog.getAvatar());

                    /**
                     * Control de usurios usr o admin
                     */
                    if (Singleton.efLog.getTipo().equals("admin")) {
                        this.frmInicio.labEmpleados.setVisible(true);
                        this.frmInicio.labUsuarios.setVisible(true);
                        this.frmInicio.labPedidos.setVisible(true);

                        /**
                         * Activar pager
                         */
                        this.frmSignIn.dispose();
                        new Controlador_User(new PagerUser(), 2).iniciar(2);
                        JPanel p2 = new JPanel();
                        p2.add(Controlador_User.frmPagerUser.jPanel);
                        this.frmInicio.internalFrame.setContentPane(p2);

                    } else {
                        /**
                         * Activar Welcome
                         */
                        this.frmSignIn.dispose();
                        JPanel p11 = new JPanel();
                        p11.add(Controlador_Login.frmWelcome.jPanel1);
                        this.frmInicio.internalFrame.setContentPane(p11);
                        this.frmInicio.labPedidos.setVisible(true);
                    }

                    this.frmInicio.labEntrar.setVisible(false);
                    this.frmInicio.labRegistro.setVisible(false);
                    this.frmInicio.pnlUser.setVisible(true);

                    /**
                     * Boolean para saber si el usuario esta conectado o no, se
                     * utiliza para el btn anadir del crearEF
                     */
                    conectado = true;

                }
                break;
            case _txtPass:
                int count = 0;
                if (this.frmSignIn.labErrorPass.isVisible()) {
                    count = 0;
                }
                if (count == 0) {
                    this.frmSignIn.txtPass.setText("");
                    count++;
                }
                break;
            case _labLogOut:
                EmpF_Controlador.frmPageref.dispose();
                EmpF_Controlador.frmCrearef.dispose();
                EmpF_Controlador.frmUpdateef.dispose();
                EmpF_Controlador.frmConfig.dispose();
                Controlador_User.frmPagerUser.dispose();
                Controlador_User.frmCrearUser.dispose();
                Controlador_User.frmUpdateUser.dispose();

                new Controlador_Login(new SignIn(), 1).Iniciar(1);
                JPanel p2 = new JPanel();
                p2.add(this.frmSignIn.jPanel1);
                this.frmInicio.internalFrame.setContentPane(p2);

                /**
                 * activar y desactivar labels
                 */
                this.frmInicio.labEmpleados.setVisible(false);
                this.frmInicio.labUsuarios.setVisible(false);
                this.frmInicio.labEntrar.setVisible(true);
                this.frmInicio.labRegistro.setVisible(true);
                this.frmInicio.pnlUser.setVisible(false);
                this.frmInicio.labPedidos.setVisible(false);

                /**
                 * Boolean para saber si el usuario esta conectado o no, se
                 * utiliza para el btn anadir del crearEF
                 */
                conectado = false;

                break;
            case _labOlvidarCont:
                new Controlador_Login(new VentanaOlvPass(), 3).Iniciar(3);
                JPanel p5 = new JPanel();
                p5.add(this.frmOlvPass.pnlOlvPass);
                this.frmInicio.internalFrame.setContentPane(p5);
                break;
            case _labEnviar:
                BLL_Login.validateOlvPass();
                System.out.print(Singleton.confirmToken);
                break;
            case _labInicio:
                JPanel p9 = new JPanel();
                p9.add(Controlador_Login.frmWelcome.jPanel1);
                this.frmInicio.internalFrame.setContentPane(p9);
                break;
            case _labUsuarios:
                new Controlador_User(new PagerUser(), 2).iniciar(2);
                JPanel p10 = new JPanel();
                p10.add(Controlador_User.frmPagerUser.jPanel);
                this.frmInicio.internalFrame.setContentPane(p10);
                break;
            case _labPedidos:
                new ControladorPedidos (new PagerPed(), 0).Iniciar(0);
                JPanel p11 = new JPanel();
                p11.add(ControladorPedidos.Pedidos.jPanel);
                this.frmInicio.internalFrame.setContentPane(p11);
                break;

        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

        switch (Controlador_Login.Accion.valueOf(e.getComponent().getName())) {
            case _labEntrar:
                frmInicio.labEntrar.setText("<html><u>Entrar</u></html>");
                break;
            case _labRegistro:
                frmInicio.labRegistro.setText("<html><u>Registro</u></html>");
                break;
            case _labAceptar:
                frmSignIn.labAceptar.setText("<html><font color=green>Aceptar</font></html>");
                break;
            case _labProductos:
                frmInicio.labProductos.setForeground(Color.darkGray);
                break;
            case _labLogOut:
                frmInicio.labLogOut.setText("<html><u>Cerrar Sesión</u></html>");
                break;
            case _labAceptarToken:
                frmConfirm.labAceptarToken.setText("<html><font color=green>Aceptar</font></html>");
                break;
            case _labOlvidarCont:
                frmSignIn.LabOlvidarCont.setText("<html><font color=green>He olvidado la contraseña</font></html>");
                break;
            case _labEnviar:
                frmOlvPass.labEnviar.setText("<html><font color=green>Enviar</font></html>");
                break;
            case _labPrintUser:
                frmInicio.labPrintUser.setText("<html><u>" + Singleton.efLog.getLogin() + "</u></html>");
                break;
            case _labEmpeados:
                frmInicio.labEmpleados.setForeground(Color.darkGray);
                break;
            case _labInicio:
                frmInicio.labInicio.setForeground(Color.darkGray);
                break;
            case _labUsuarios:
                frmInicio.labUsuarios.setForeground(Color.darkGray);
                break;
            case _labPedidos:
                frmInicio.labPedidos.setForeground(Color.darkGray);
                break;

        }

    }

    @Override
    public void mouseExited(MouseEvent e) {

        switch (Controlador_Login.Accion.valueOf(e.getComponent().getName())) {
            case _labEntrar:
                frmInicio.labEntrar.setText("<html>Entrar</html>");
                break;
            case _labAceptar:
                frmSignIn.labAceptar.setText("<html><font color=black>Aceptar</font></html>");
                break;
            case _labLogOut:
                frmInicio.labLogOut.setText("<html>Cerrar Sesión</html>");
                break;
            case _labRegistro:
                frmInicio.labRegistro.setText("<html>Registro</html>");
                break;
            case _labAceptarToken:
                frmConfirm.labAceptarToken.setText("<html><font color=black>Aceptar</font></html>");
                break;
            case _labOlvidarCont:
                frmSignIn.LabOlvidarCont.setText("<html><font color=black>He olvidado la contraseña</font></html>");
                break;
            case _labEnviar:
                frmOlvPass.labEnviar.setText("<html><font color=black>Enviar</font></html>");
                break;
            case _labPrintUser:
                frmInicio.labPrintUser.setText("<html>" + Singleton.efLog.getLogin() + "</html>");
                break;
            case _labEmpeados:
                frmInicio.labEmpleados.setForeground(Color.white);
                break;
            case _labProductos:
                frmInicio.labProductos.setForeground(Color.white);
                break;
            case _labInicio:
                frmInicio.labInicio.setForeground(Color.white);
                break;
            case _labUsuarios:
                frmInicio.labUsuarios.setForeground(Color.white);
                break;
            case _labPedidos:
                frmInicio.labPedidos.setForeground(Color.white);
                break;
        }

    }

}
