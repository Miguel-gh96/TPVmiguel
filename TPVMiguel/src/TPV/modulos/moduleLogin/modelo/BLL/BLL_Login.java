/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleLogin.modelo.BLL;

import TPV.clases.JavaMail;
import TPV.librerias.Encriptar;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EfGraficoBLL;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EmpfBLL_DB;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.gestionPers.gestionUser.modelo.BLL.BLL_User;
import TPV.modulos.gestionPers.gestionUser.modelo.BLL.BLLuser_DB;
import TPV.modulos.gestionPers.gestionUser.modelo.clases.SingletonUser;
import TPV.modulos.moduleLogin.controlador.Controlador_Login;
import TPV.modulos.moduleLogin.modelo.DAO.DAO_Login;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import TPV.modulos.moduleLogin.vista.SignIn;
import TPV.modulos.moduleLogin.vista.VentanaConfirm;
import TPV.modulos.moduleLogin.vista.VentanaOlvPass;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author miguel
 */
public class BLL_Login {

    public static void pideUser() {
        DAO_Login.pideUser();

        if (SignIn.labErrorUser.isVisible()) {
            SignIn.txtUser.requestFocus();
        } else {
            
            if (BLL_User.buscarUser(SignIn.txtUser.getText()) != -1) {               
                SignIn.labErrorUser.setVisible(false);               
                return;
            } else {
                SignIn.labErrorUser.setText("No existe el usuario");
                SignIn.labErrorUser.setVisible(true);
                SignIn.txtUser.requestFocus();
            }

        }

    }

    public static void pidePass() {
        DAO_Login.pidePass();
        if (SignIn.labErrorPass.isVisible()) {
            SignIn.txtPass.requestFocus();
        }
    }

    public static void pideNewPass() {
        DAO_Login.pideNewPass();
        if (VentanaOlvPass.labErrorNewPass.isShowing() == false) {
            VentanaOlvPass.passNew.requestFocus();
        }
    }

    public static Boolean login() {
        boolean resultado = false;
        pideUser();
        pidePass();

        if (SignIn.labErrorPass.isVisible() == false && SignIn.labErrorUser.isVisible() == false) {
            BLL_DB_Login _log = new BLL_DB_Login();
            resultado = _log.loginUsuarioBLL(SignIn.txtUser.getText(), SignIn.txtPass.getText());
            
        }

        return resultado;

    }

    public static void validateToken() {
        String token = VentanaConfirm.txtConfirmToken.getText();

        if (token.equals(Singleton.confirmToken)) {

            if (Singleton.pass == null) {
                SingletonUser.user.add(SingletonUser.usr);
                BLLuser_DB _usr = new BLLuser_DB();
                _usr.nuevoUserBLL();
                

                /**
                 * Activar ventanaConfirm
                 */
                Controlador_Login.frmConfirm.dispose();
                new Controlador_Login(new SignIn(), 1).Iniciar(1);
                JPanel p6 = new JPanel();
                p6.add(Controlador_Login.frmSignIn.jPanel1);
                Controlador_Login.frmInicio.internalFrame.setContentPane(p6);

            } else {
                SingletonUser.usr.setPassword(Singleton.pass);
                BLLuser_DB _us = new BLLuser_DB();
                _us.modificarUserBLL();
                if (_us.modificarUserBLL()!= 0) {
                    Singleton.pass = null;
                    /**
                     * Activar ventanaConfirm
                     */
                    Controlador_Login.frmConfirm.dispose();
                    new Controlador_Login(new SignIn(), 1).Iniciar(1);
                    JPanel p2 = new JPanel();
                    p2.add(Controlador_Login.frmSignIn.jPanel1);
                    Controlador_Login.frmInicio.internalFrame.setContentPane(p2);

                }

            }

        } else {
            VentanaConfirm.labErrorToken.setVisible(true);
            VentanaConfirm.labErrorToken.setText("El código de verificación introducido no coincide");
        }

    }

    public static void validateOlvPass() {
        String token, EncrypToken;

        pideNewPass();
        if (VentanaOlvPass.labErrorNewPass.isVisible() == false) {

            //buscamos el email introducido en la BD si hay coincidencia entra y además se rellena el SingletonUser con los datos del uuser con coincidencia
            if (BLL_User.buscaremailUser(Controlador_Login.frmOlvPass.txtEmail.getText()) != -1) {
                //Guardamos la pass nueva en un singleton
                Singleton.pass = Controlador_Login.frmOlvPass.passNew.getText();
                //Creamos un token
                token = Encriptar.getCadenaAleatoria(25);
                //encriptamos el token
                EncrypToken = Encriptar.encriptarTokenMD5(token);
                //Guardamos el token en un singleton
                Singleton.confirmToken = EncrypToken;

                JavaMail mail = new JavaMail(Controlador_Login.frmOlvPass.txtEmail.getText(), "E-mail de confirmación", "Código de activación: \n" + EncrypToken + "\n");
                //enviamos el mensaje
                String error = mail.send();
                if (error.equals("")) {
                    JOptionPane.showMessageDialog(null, "Envio Correcto", "Correcto", JOptionPane.INFORMATION_MESSAGE);
                    /**
                     * Activar ventanaConfirm
                     */
                    //JOptionPane.showMessageDialog(null, ArraylistEF.ef.toString());
                    Controlador_Login.frmSignIn.dispose();
                    new Controlador_Login(new VentanaConfirm(), 2).Iniciar(2);
                    JPanel p5 = new JPanel();
                    p5.add(Controlador_Login.frmConfirm.pnlConfirmUser);
                    Controlador_Login.frmInicio.internalFrame.setContentPane(p5);
                } else {
                    JOptionPane.showMessageDialog(null, "Error de envio:\n" + error, "Error", JOptionPane.ERROR_MESSAGE);
                }

            } else {
                Controlador_Login.frmOlvPass.labErrorEmail.setText("No existe el email introducido");
                Controlador_Login.frmOlvPass.labErrorEmail.setVisible(true);
            }

        }

    }

}
