/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleLogin.modelo.BLL;

import TPV.clases.ConexionBD;
import TPV.modulos.moduleLogin.modelo.DAO.DAO_DB_Login;
import java.sql.Connection;

/**
 *
 * @author miguel
 */
public class BLL_DB_Login {
    
    public boolean loginUsuarioBLL(String login,String pass) {

        Connection _con;
        boolean _resul;
        ConexionBD _conexion_DB = new ConexionBD();

        _con = _conexion_DB.AbrirConexion();

        DAO_DB_Login _loginDAO = new DAO_DB_Login();

        _resul = _loginDAO.loginUserDAO(_con,login ,pass);

        _conexion_DB.CerrarConexion(_con);

        return _resul;

    }
    
}
