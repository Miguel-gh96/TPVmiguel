/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleLogin.modelo.DAO;

import TPV.librerias.Validate;
import TPV.modulos.moduleLogin.vista.SignIn;
import TPV.modulos.moduleLogin.vista.VentanaOlvPass;


/**
 *
 * @author miguel
 */
public class DAO_Login {
    
    public static void pideUser() {

        if (SignIn.txtUser.getText().isEmpty()) {
            SignIn.labErrorUser.setText("<html>El campo de usuario no puede estar vacio</html>");
            SignIn.labErrorUser.setVisible(true);
            return;

        } else {
            SignIn.labErrorUser.setVisible(false);
            
            if(Validate.usuario(SignIn.txtUser.getText()) == false){
                SignIn.labErrorUser.setText("<html>El campo debe  <br/> contener letras y números</html>");
                SignIn.labErrorUser.setVisible(true);
                return;
            }else
                SignIn.labErrorUser.setVisible(false);
            
        }

    }
    
    public static void pidePass() {

        if (SignIn.txtPass.getText().isEmpty()) {
            SignIn.labErrorPass.setText("<html>El campo de la contraseña no <br/> puede estar vacio</html>");
            SignIn.labErrorPass.setVisible(true);
            return;

        } else {
            SignIn.labErrorPass.setVisible(false);
            
            if(Validate.pass(SignIn.txtPass.getText()) == false){
                SignIn.labErrorPass.setText("<html>El campo de la contraseña solo puede<br/> contener letras y numeros tiene que<br/> tener entre 8 y 10 </html>");
                SignIn.labErrorPass.setVisible(true);
                return;            
            }else
                SignIn.labErrorPass.setVisible(false);
            
        }

    }
    
    
    public static void pideNewPass() {
        
        if (VentanaOlvPass.passNew.getText().isEmpty()) {
            VentanaOlvPass.labErrorNewPass.setText("<html>El campo de la contraseña no <br/> puede estar vacio</html>");
            VentanaOlvPass.labErrorNewPass.setVisible(true);
            return;

        } else {
            VentanaOlvPass.labErrorNewPass.setVisible(false);
            
            if(Validate.pass(VentanaOlvPass.passNew.getText()) == false){
                VentanaOlvPass.labErrorNewPass.setText("<html>El campo de la contraseña debe contener letras y numeros.<br/> Tiene que tener entre 8 y 10 carácteres </html>");
                VentanaOlvPass.labErrorNewPass.setVisible(true);
                return;        
            }else{
                VentanaOlvPass.labErrorNewPass.setVisible(false);
            }
            
        }
        
    }
    
    
    
    
    
}
