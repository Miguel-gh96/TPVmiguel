/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleLogin.modelo.DAO;

import TPV.clases.Fecha;
import TPV.librerias.Encriptar;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author miguel
 */
public class DAO_DB_Login {
    
    public boolean loginUserDAO(Connection con,String login,String pass) {


        boolean resultado = false;
        Fecha fechanac ,fechacont;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        String passPhrase   = "Xenobosfeelthesame";

        //Encriptar encriptador = new Encriptar(passPhrase);

        try {
            stmt = con.prepareStatement("SELECT * FROM TPV.User WHERE login=? AND password=?");

            stmt.setString(1,login);
            stmt.setString(2,Encriptar.encriptarTokenMD5(pass));
            rs = stmt.executeQuery();

            while (rs.next()) {
  
                Singleton.efLog.setNombre(rs.getString("nombre"));
                Singleton.efLog.setApellidos(rs.getString("apellidos"));
                Singleton.efLog.setDireccion(rs.getString("direccion"));
                Singleton.efLog.setNif(rs.getString("nif"));
                fechanac = new Fecha(rs.getString("fecha_nacimiento"));
                Singleton.efLog.setFechanac(fechanac);
                Singleton.efLog.cambiaredad(fechanac);
                Singleton.efLog.setTlfno(rs.getString("telefono"));
                Singleton.efLog.setCorreo(rs.getString("correo"));
                Singleton.efLog.setLogin(rs.getString("login"));
                Singleton.efLog.setPassword(rs.getString("password"));
                Singleton.efLog.setAvatar(rs.getString("avatar"));
                Singleton.efLog.setTipo(rs.getString("tipo"));
                Singleton.efLog.setEstado(rs.getInt("estado"));
                resultado = true;
                
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error Logger");
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error Logger");
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error Logger");
                }
            }
        }
        return resultado;
    }
    
}
