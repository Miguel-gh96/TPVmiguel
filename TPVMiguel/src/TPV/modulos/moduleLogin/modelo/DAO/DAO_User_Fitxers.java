/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleLogin.modelo.DAO;
import TPV.modulos.gestionPers.gestionEF.modelo.DAO.*;
import TPV.Ppal;
import TPV.librerias.Funciones;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EfBLLfitxers;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;

/**
 *
 * @author miguel
 */
public class DAO_User_Fitxers {
    
    
    
    public static void GuardarJSON(){
        TPV.modulos.gestionPers.gestionUser.modelo.libreriasUser.Json.generajsonUser();//Genera un archivo json con todos los datos del Arraylist de usuarios
       
    }
    
    public static void GuardarTXT() {
        TPV.modulos.gestionPers.gestionUser.modelo.libreriasUser.Txt.generatxtUser();//Lo mismo que antes pero con un txt
     
    }
    
    public static void GuardarXML() {
        TPV.modulos.gestionPers.gestionUser.modelo.libreriasUser.Xml.generaxmlUser();//Ídem pero para un xml
      
    }
    

	

    
}
