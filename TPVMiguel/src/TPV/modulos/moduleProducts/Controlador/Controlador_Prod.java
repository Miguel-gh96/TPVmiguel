/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleProducts.Controlador;

import TPV.librerias.FileUpload;
import TPV.modulos.GestionPedidos.Controlador.ControladorPedidos;
import TPV.modulos.GestionPedidos.Modelo.Vista.VentanaPed;
import TPV.modulos.gestionPers.gestionEF.controlador.EmpF_Controlador;
import TPV.modulos.gestionPers.gestionEF.vista.Configuration;
import TPV.modulos.moduleLogin.controlador.Controlador_Login;
import TPV.modulos.moduleLogin.modelo.clases.Singleton;
import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import TPV.modulos.moduleProducts.Modelo.Clases.Productos;
import TPV.modulos.moduleProducts.Modelo.BLL.BLL_Prod;
import TPV.modulos.moduleProducts.Modelo.BLL.BLL_Prod_Fitxers;
import TPV.modulos.moduleProducts.Modelo.BLL.BLLprod_DB;
import TPV.modulos.moduleProducts.Modelo.Clases.SingletonProd;
import TPV.modulos.moduleProducts.Modelo.Clases.MiniSimpleTableModel_Prod;
import TPV.modulos.moduleProducts.Modelo.Clases.RenderizaProd;
import TPV.modulos.moduleProducts.Modelo.DAO.DAO_Prod;
import TPV.modulos.moduleProducts.Vista.CreateProd;
import TPV.modulos.moduleProducts.Vista.PagerProducts;
import TPV.modulos.moduleProducts.Vista.UpdateProd;
import TPV.modulos.moduleProducts.autocomplete.AutocompleteJComboBox;
import TPV.modulos.moduleProducts.autocomplete.StringSearchable;
import TPV.modulos.moduleProducts.pager.pagina;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 *
 * @author pollotorrao
 */
public class Controlador_Prod implements ActionListener, KeyListener, MouseListener {

    public static PagerProducts frmProductos = new PagerProducts();
    public static TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(new MiniSimpleTableModel_Prod());
    public static AutocompleteJComboBox combo = null;
    private int count = 0;

    public Controlador_Prod(JFrame Productos, int i) {
        if (i == 0) {
            this.frmProductos = (PagerProducts) Productos;
        }
    }

    public enum Action {

        //Acciones del PagerProd
        _labCreate,
        _labUpdate,
        _labDelete,
        _jComboBox1,
        _labJSON,
        _labXML,
        _labTXT,
        _labConfig,
        _btnPrimero,
        _btnSiguiente,
        _btnAnterior,
        _btnUltimo,
        _labMaxmin,
        _labEdadMedia,
        _txtNombre,
        _txtID,
        _txtPrecio,
        _txtStock,
        _txtDesc,
        _tablaFijos,
        _labChangeAvatar,
        _labRealPed

    }

    public void Iniciar(int i) {
        if (i == 0) {//Pager Productos

            /**
             * Filtro para buscar empleados por nombre + automcompletar
             */
            List<String> myWords = new ArrayList<String>();

            for (int j = 0; j <= SingletonProd.prod.size() - 1; j++) {
                myWords.add(SingletonProd.prod.get(j).getNombre());
            }

            StringSearchable searchable = new StringSearchable(myWords);
            combo = new AutocompleteJComboBox(searchable);
            frmProductos.panelFiltro.setLayout(new java.awt.BorderLayout());
            frmProductos.panelFiltro.add(combo);

            combo.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    comboActionPerformed(evt);
                }
            });

            //Ocultar etiquetas
            this.frmProductos.labErrorNom.setVisible(false);
            this.frmProductos.labErrorID.setVisible(false);
            this.frmProductos.labErrorDesc.setVisible(false);
            this.frmProductos.labErrorPrecio.setVisible(false);
            this.frmProductos.labErrorStock.setVisible(false);

            if (Controlador_Login.conectado == false) {
                this.frmProductos.labRealPed.setVisible(false);
            } else {
                this.frmProductos.labRealPed.setVisible(true);
            }

            /**
             * Deshabilitar botones para los usuarios no admin
             */
            if (Singleton.efLog.getTipo().equals("admin")) {
                frmProductos.labCreate.setVisible(true);
                frmProductos.labDelete.setVisible(true);
                frmProductos.labUpdate.setVisible(true);
                frmProductos.labConfig.setVisible(true);
                frmProductos.labChangeAvatar.setVisible(true);
                frmProductos.txtID.setEditable(true);
                frmProductos.txtNombre.setEditable(true);
                frmProductos.txtPrecio.setEditable(true);
                frmProductos.txtStock.setEditable(true);
                frmProductos.txtDesc.setEditable(true);

            } else {
                frmProductos.labCreate.setVisible(false);
                frmProductos.labDelete.setVisible(false);
                frmProductos.labUpdate.setVisible(false);
                frmProductos.labConfig.setVisible(false);
                frmProductos.labChangeAvatar.setVisible(false);
                frmProductos.txtID.setEditable(false);
                frmProductos.txtNombre.setEditable(false);
                frmProductos.txtPrecio.setEditable(false);
                frmProductos.txtStock.setEditable(false);
                frmProductos.txtDesc.setEditable(false);
            }

            /**
             * Cargar tabla de empleados fijos
             */
            frmProductos.tablaFijos.setModel(new MiniSimpleTableModel_Prod());
            ((MiniSimpleTableModel_Prod) frmProductos.tablaFijos.getModel()).cargar();
            frmProductos.tablaFijos.setFillsViewportHeight(true);
            frmProductos.tablaFijos.setRowSorter(sorter);
            RenderizaProd renderizador = new RenderizaProd();
            frmProductos.tablaFijos.setDefaultRenderer(String.class, renderizador);
            frmProductos.txtCaja.setEditable(false);

            pagina.inicializa();
            pagina.initLinkBox();

            /**
             * Componentes ActionPerformed
             */
            this.frmProductos.jComboBox1.setActionCommand("_jComboBox1");
            this.frmProductos.jComboBox1.addActionListener(this);

            this.frmProductos.btnPrimero.setActionCommand("_btnPrimero");
            this.frmProductos.btnPrimero.addActionListener(this);

            this.frmProductos.btnUltimo.setActionCommand("_btnUltimo");
            this.frmProductos.btnUltimo.addActionListener(this);

            this.frmProductos.btnSiguiente.setActionCommand("_btnSiguiente");
            this.frmProductos.btnSiguiente.addActionListener(this);

            this.frmProductos.btnAnterior.setActionCommand("_btnAnterior");
            this.frmProductos.btnAnterior.addActionListener(this);

            /**
             * Complementos Mouse Event
             */
            this.frmProductos.labConfig.setName("_labConfig");
            this.frmProductos.labConfig.addMouseListener(this);

            this.frmProductos.labJSON.setName("_labJSON");
            this.frmProductos.labJSON.addMouseListener(this);

            this.frmProductos.labXML.setName("_labXML");
            this.frmProductos.labXML.addMouseListener(this);

            this.frmProductos.labTXT.setName("_labTXT");
            this.frmProductos.labTXT.addMouseListener(this);

            this.frmProductos.labCreate.setName("_labCreate");
            this.frmProductos.labCreate.addMouseListener(this);

            this.frmProductos.labRealPed.setName("_labRealPed");
            this.frmProductos.labRealPed.addMouseListener(this);

            this.frmProductos.labUpdate.setName("_labUpdate");
            this.frmProductos.labUpdate.addMouseListener(this);

            this.frmProductos.labDelete.setName("_labDelete");
            this.frmProductos.labDelete.addMouseListener(this);

            this.frmProductos.labChangeAvatar.setName("_labChangeAvatar");
            this.frmProductos.labChangeAvatar.addMouseListener(this);

            this.frmProductos.tablaFijos.setName("_tablaFijos");
            this.frmProductos.tablaFijos.addMouseListener(this);

            this.frmProductos.txtNombre.setActionCommand("_txtNombre");
            this.frmProductos.txtNombre.setName("_txtNombre");
            this.frmProductos.txtNombre.addActionListener(this);
            this.frmProductos.txtNombre.addKeyListener(this);
            this.frmProductos.txtNombre.addMouseListener(this);

            this.frmProductos.txtID.setActionCommand("_txtID");
            this.frmProductos.txtID.setName("_txtID");
            this.frmProductos.txtID.addActionListener(this);
            this.frmProductos.txtID.addKeyListener(this);
            this.frmProductos.txtID.addMouseListener(this);

            this.frmProductos.txtDesc.setName("_txtDesc");
            this.frmProductos.txtDesc.addKeyListener(this);
            this.frmProductos.txtDesc.addMouseListener(this);

            this.frmProductos.txtPrecio.setActionCommand("_txtPrecio");
            this.frmProductos.txtPrecio.setName("_txtPrecio");
            this.frmProductos.txtPrecio.addActionListener(this);
            this.frmProductos.txtPrecio.addKeyListener(this);
            this.frmProductos.txtPrecio.addMouseListener(this);

            this.frmProductos.txtStock.setActionCommand("_txtStock");
            this.frmProductos.txtStock.setName("_txtStock");
            this.frmProductos.txtStock.addActionListener(this);
            this.frmProductos.txtStock.addKeyListener(this);
            this.frmProductos.txtStock.addMouseListener(this);

        }

    }

    public static void comboActionPerformed(java.awt.event.ActionEvent evt) {
        System.out.println("word selected: " + ((JComboBox) combo).getSelectedItem());
        pagina.currentPageIndex = 1;
        ((MiniSimpleTableModel_Prod) frmProductos.tablaFijos.getModel()).filtrar();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        switch (Action.valueOf(e.getActionCommand())) {

            case _txtNombre:
                BLL_Prod.pideNombre();
                break;
            case _txtID:
                BLL_Prod.pideID();
                break;
            case _txtPrecio:
                BLL_Prod.pidePrecio();
                break;
            case _txtStock:
                BLL_Prod.pideStock();
                break;
            case _jComboBox1:
                pagina.itemsPerPage = Integer.parseInt(frmProductos.jComboBox1.getSelectedItem().toString());
                pagina.currentPageIndex = 1;
                pagina.initLinkBox();
                break;

            case _btnPrimero:
                pagina.currentPageIndex = 1;
                pagina.initLinkBox();
                break;
            case _btnUltimo:
                pagina.currentPageIndex = pagina.maxPageIndex;
                pagina.initLinkBox();
                break;
            case _btnSiguiente:
                pagina.currentPageIndex += 1;
                pagina.initLinkBox();
                break;
            case _btnAnterior:
                pagina.currentPageIndex -= 1;
                pagina.initLinkBox();
                break;

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

        switch (Action.valueOf(e.getComponent().getName())) {

            case _txtNombre:
                BLL_Prod.pideNombre();
                break;
            case _txtID:
                BLL_Prod.pideID();
                break;
            case _txtPrecio:
                BLL_Prod.pidePrecio();
                break;
            case _txtStock:
                BLL_Prod.pideStock();
                break;
            case _txtDesc:
                BLL_Prod.pideDesc();
                break;

        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {

        switch (Action.valueOf(me.getComponent().getName())) {

            case _labConfig:
                /**
                 * Activar config
                 */
                new EmpF_Controlador(new Configuration(), 3).iniciar(3);
                JPanel p3 = new JPanel();
                p3.add(EmpF_Controlador.frmConfig.pnlConfig);
                Controlador_Login.frmInicio.internalFrame.setContentPane(p3);
                break;
            case _labJSON:
                BLL_Prod_Fitxers.GuardarJSON();
                break;
            case _labXML:
                BLL_Prod_Fitxers.GuardarXML();
                break;
            case _labTXT:
                BLL_Prod_Fitxers.GuardarTXT();
                break;
            case _labCreate:
                if (count == 0) {
                    //Clear
                    PagerProducts.txtNombre.setText("");
                    PagerProducts.txtID.setText("");
                    PagerProducts.txtDesc.setText("");
                    PagerProducts.txtID.setText("");
                    PagerProducts.txtPrecio.setText("");
                    PagerProducts.txtStock.setText("");
                    count++;

                    JOptionPane.showMessageDialog(null, "Introduzca los datos del producto.");

                } else {

                    BLL_Prod.pideProd();
                    count = 0;

                }
                break;
            case _labUpdate:
                int correcto = BLL_Prod.updateProd();
                if (correcto != 0) {
                    BLL_Prod.cambiaProd();
                }
                break;
            case _labDelete:
                BLL_Prod.deleteProd();
                break;
            case _txtNombre:
                BLL_Prod.pideNombre();
                break;
            case _txtID:
                BLL_Prod.pideID();
                break;
            case _txtPrecio:
                BLL_Prod.pidePrecio();
                break;
            case _txtStock:
                BLL_Prod.pideStock();
                break;
            case _txtDesc:
                BLL_Prod.pideDesc();
                break;
            case _tablaFijos:
                String ID;
                int n,
                 selection,
                 inicio,
                 selection1;
                n = ((MiniSimpleTableModel_Prod) PagerProducts.tablaFijos.getModel()).getRowCount();

                if (n != 0) {

                    inicio = (pagina.currentPageIndex - 1) * pagina.itemsPerPage;
                    selection = PagerProducts.tablaFijos.getSelectedRow();
                    selection1 = inicio + selection;

                    ID = (String) PagerProducts.tablaFijos.getModel().getValueAt(selection1, 0);
                    Productos prod = new Productos(ID);
                    SingletonProd.producto = SingletonProd.prod.get(BLL_Prod.buscaID(prod));
                    BLL_Prod.rellenarProd();
                }
                break;
            case _labChangeAvatar:
                FileUpload.pintar_guardar_imag(this.frmProductos.labPrintAvatar, 120, 90);
                break;
            case _labRealPed:
                if (SingletonProd.producto.getStock() != 0) {
                    if (BLL_Prod.buscarProd() != 0) {
                        new ControladorPedidos(new VentanaPed(), 1).Iniciar(1);
                    };
                } else {
                    JOptionPane.showMessageDialog(null, "No hay stock");
                }
                break;

        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {

        switch (Action.valueOf(me.getComponent().getName())) {

            case _labCreate:
                ImageIcon icon = new ImageIcon("src/TPV/imag/anadir.png");
                Image img = icon.getImage();
                ImageIcon newIcon = new ImageIcon(img);
                frmProductos.labCreate.setIcon(newIcon);
                break;
            case _labUpdate:
                ImageIcon icon1 = new ImageIcon("src/TPV/imag/actualizar.png");
                Image img1 = icon1.getImage();
                ImageIcon newIcon1 = new ImageIcon(img1);
                frmProductos.labUpdate.setIcon(newIcon1);
                break;
            case _labDelete:
                ImageIcon icon2 = new ImageIcon("src/TPV/imag/borrar.png");
                Image img2 = icon2.getImage();
                ImageIcon newIcon2 = new ImageIcon(img2);
                frmProductos.labDelete.setIcon(newIcon2);
                break;
            case _labConfig:
                ImageIcon icon3 = new ImageIcon("src/TPV/imag/config.png");
                Image img3 = icon3.getImage();
                ImageIcon newIcon3 = new ImageIcon(img3);
                frmProductos.labConfig.setIcon(newIcon3);
                break;
            case _labJSON:
                ImageIcon icon4 = new ImageIcon("src/TPV/imag/json.png");
                Image img4 = icon4.getImage();
                ImageIcon newIcon4 = new ImageIcon(img4);
                frmProductos.labJSON.setIcon(newIcon4);
                break;
            case _labXML:
                ImageIcon icon5 = new ImageIcon("src/TPV/imag/xml.png");
                Image img5 = icon5.getImage();
                ImageIcon newIcon5 = new ImageIcon(img5);
                frmProductos.labXML.setIcon(newIcon5);
                break;
            case _labTXT:
                ImageIcon icon6 = new ImageIcon("src/TPV/imag/txt.png");
                Image img6 = icon6.getImage();
                ImageIcon newIcon6 = new ImageIcon(img6);
                frmProductos.labTXT.setIcon(newIcon6);
                break;
            case _labRealPed:
                this.frmProductos.labRealPed.setText("<html><font color=green>Realizar Pedido</font></html>");
                break;

        }
    }

    @Override
    public void mouseExited(MouseEvent me) {

        switch (Action.valueOf(me.getComponent().getName())) {

            case _labCreate:
                ImageIcon icon = new ImageIcon("src/TPV/imag/anadirB.png");
                Image img = icon.getImage();
                ImageIcon newIcon = new ImageIcon(img);
                frmProductos.labCreate.setIcon(newIcon);
                break;
            case _labUpdate:
                ImageIcon icon1 = new ImageIcon("src/TPV/imag/actualizarB.png");
                Image img1 = icon1.getImage();
                ImageIcon newIcon1 = new ImageIcon(img1);
                frmProductos.labUpdate.setIcon(newIcon1);
                break;
            case _labDelete:
                ImageIcon icon2 = new ImageIcon("src/TPV/imag/borrarB.png");
                Image img2 = icon2.getImage();
                ImageIcon newIcon2 = new ImageIcon(img2);
                frmProductos.labDelete.setIcon(newIcon2);
                break;
            case _labConfig:
                ImageIcon icon3 = new ImageIcon("src/TPV/imag/configB.png");
                Image img3 = icon3.getImage();
                ImageIcon newIcon3 = new ImageIcon(img3);
                frmProductos.labConfig.setIcon(newIcon3);
                break;
            case _labJSON:
                ImageIcon icon4 = new ImageIcon("src/TPV/imag/JSONB.png");
                Image img4 = icon4.getImage();
                ImageIcon newIcon4 = new ImageIcon(img4);
                frmProductos.labJSON.setIcon(newIcon4);
                break;
            case _labXML:
                ImageIcon icon5 = new ImageIcon("src/TPV/imag/XMLB.png");
                Image img5 = icon5.getImage();
                ImageIcon newIcon5 = new ImageIcon(img5);
                frmProductos.labXML.setIcon(newIcon5);
                break;
            case _labTXT:
                ImageIcon icon6 = new ImageIcon("src/TPV/imag/TXTB.png");
                Image img6 = icon6.getImage();
                ImageIcon newIcon6 = new ImageIcon(img6);
                frmProductos.labTXT.setIcon(newIcon6);
                break;
            case _labRealPed:
                this.frmProductos.labRealPed.setText("<html><font color=black>Realizar Pedido</font></html>");
                break;
        }
    }

}
