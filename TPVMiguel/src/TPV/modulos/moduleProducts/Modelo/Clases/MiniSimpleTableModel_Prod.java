/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleProducts.Modelo.Clases;

import java.util.ArrayList;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import static TPV.modulos.moduleProducts.Controlador.Controlador_Prod.combo;
import TPV.modulos.moduleProducts.Modelo.BLL.BLLprod_DB;
import TPV.modulos.moduleProducts.pager.pagina;
import javax.swing.JComboBox;

/**
 *
 * @author pollotorrao
 */
public class MiniSimpleTableModel_Prod extends AbstractTableModel {

    public static ArrayList<Productos> datos = new ArrayList<Productos>();
    public static ArrayList<Productos> datosaux = new ArrayList<Productos>();

    String[] columnas = {"ID_Producto", "Nombre", "Precio"};

    ////////////////////estos métodos son necesarios para que jtable funcione/////////////////////
    @Override
    public String getColumnName(int col) {
        return columnas[col].toString();
    }

    //Devuelve el numero de filas
    @Override
    public int getRowCount() {
        return datos.size();
    }

    //Devuelve el numero de columnas
    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    //Devuelve el valor del objeto en la fila y columna
    @Override
    public Object getValueAt(int row, int col) {

        Object dev = null;
        Productos fila = (Productos) datos.get(row);

        switch (col) {
            case 0:
                dev = fila.getID_Producto();
                break;

            case 1:
                dev = fila.getNombre();
                break;

            case 2:
                dev = fila.getPrecio();
                break;

        }
        return dev;
    }

    //Determina si una fila y columna ha de ser editable
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    //Actualiza un objeto de una fila y columna
    @Override
    public void setValueAt(Object value, int row, int col) {
        Productos fila = (Productos) datos.get(row);

        switch (col) {
            case 0:
                fila.setID_Producto(value.toString());
                break;

            case 1:
                fila.setNombre(value.toString());
                break;

            case 2:
                fila.setPrecio((float) value);
                break;

        }
        fireTableCellUpdated(row, col);
    }

    public void addRow(Productos pro) {
        datos.add(pro);
        fireTableDataChanged();
    }

    public void cargar() {
        datos.clear();
        datosaux.clear();

        BLLprod_DB _productos = new BLLprod_DB();
        _productos.listAllProductosBLL();

        for (int i = 0; i < SingletonProd.prod.size(); i++) {
            datosaux.add(SingletonProd.prod.get(i));
            addRow(SingletonProd.prod.get(i));
        }
    }

    public void filtrar() {
        datos.clear();
        int cont = 0;
        String ID = (String) ((JComboBox) combo).getSelectedItem();

        
        if (ID != null) {
            for (int i = 0; i < datosaux.size(); i++) {
                
                if (datosaux.get(i).getNombre().toLowerCase().startsWith(ID.toLowerCase())) {
                    addRow(datosaux.get(i));
                    cont++;
                }
            }
            pagina.initLinkBox();
        }

    }

    public Productos buscar(String u) {
        datos.clear();
        cargar();

        String res;
        for (int i = 0; i < datos.size(); i++) {
            res = datos.get(i).toString();
            if (res.contains(u)) {
                return datos.get(i);
            }
        }
        return null;
    }

    public int buscaProductos(Productos u) {
        datos.clear();
        cargar();

        for (int i = 0; i < datos.size(); i++) {
            if (datos.get(i).equals(u)) {
                return i;
            }
        }
        return -1;
    }

    public void removeRow(int fila) {
        datos.remove(fila);
        fireTableDataChanged();
    }

    public static String getCadenaAleatoria1(int longitud) {
        String cadenaAleatoria = "";
        long milis = new java.util.GregorianCalendar().getTimeInMillis();
        Random r = new Random(milis);
        int i = 0;
        while (i < longitud) {
            char c = (char) r.nextInt(255);
            if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) {
                cadenaAleatoria += c;
                i++;
            }
        }
        return cadenaAleatoria;
    }

    public static String getCadenaAleatoria2(int longitud) {
        String cadenaAleatoria = "";
        long milis = new java.util.GregorianCalendar().getTimeInMillis();
        Random r = new Random(milis);
        int i = 0;
        while (i < longitud) {
            char c = (char) r.nextInt(255);
            if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z')) {
                cadenaAleatoria += c;
                i++;
            }
        }
        return cadenaAleatoria;
    }

}
