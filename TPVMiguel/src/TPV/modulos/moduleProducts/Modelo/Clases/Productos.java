/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.moduleProducts.Modelo.Clases;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

/**
 *
 * @author pollotorrao
 */
public class Productos implements Comparable<Productos>, Serializable{
    
    @XStreamAlias("ID_Producto")
    public String ID_Producto;
    @XStreamAlias("Nombre")
    public String Nombre;
    @XStreamAlias("Descripcion")
    public String Descripcion;
    @XStreamAlias("Precio")
    public float Precio;
    @XStreamAlias("Stock")
    public int Stock;
    
    public String Avatar;
    
    public Productos(){}
    
    public Productos(String ID_Producto){
         this.ID_Producto=ID_Producto;
     }
    
    /*public Productos(String ID_Producto, String Nombre, String Descripcion, float Precio, int Stock){
        this.ID_Producto=ID_Producto;
        this.Nombre=Nombre;
        this.Descripcion=Descripcion;
        this.Precio=Precio;
        this.Stock=Stock;
    }*/

    public Productos(String ID_Producto, String Nombre, String Descripcion, float Precio, int Stock, String avatar){
        this.ID_Producto=ID_Producto;
        this.Nombre=Nombre;
        this.Descripcion=Descripcion;
        this.Precio=Precio;
        this.Stock=Stock;
        this.Avatar=avatar;
    }
    
    public int getStock() {
        return Stock;
    }

    public void setStock(int Stock) {
        this.Stock = Stock;
    }

    public String getID_Producto() {
        return ID_Producto;
    }

    public void setID_Producto(String ID_Producto) {
        this.ID_Producto = ID_Producto;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public float getPrecio() {
        return Precio;
    }

    public void setPrecio(float Precio) {
        this.Precio = Precio;
    }

    public String getAvatar() {
        return Avatar;
    }

    public void setAvatar(String Avatar) {
        this.Avatar = Avatar;
    }
    
    
    public int compareTo(Productos pro) {//para ordenar los clientes
		if(this.getID_Producto().compareTo(pro.getID_Producto())>0)
			return 1;
		if(this.getID_Producto().compareTo(pro.getID_Producto())<0)
			return -1;
		return 0;
		 }
    public boolean equals(Object c){
		return getID_Producto().equals(((Productos)c).getID_Producto());
        }
    
    public String toString() {
		StringBuffer tarjeta=new StringBuffer();
                tarjeta.append("ID_Producto: "+getID_Producto()+"\n");
                tarjeta.append("Nombre: "+getNombre()+"\n");
                tarjeta.append("Descripcion: "+getDescripcion()+"\n");
                tarjeta.append("Precio: "+getPrecio()+"\n");
                tarjeta.append("Stock: "+getStock()+"\n");
                tarjeta.append("Avatar: "+getAvatar()+"\n");
                return tarjeta.toString();
                
    }
    
    
    
}
