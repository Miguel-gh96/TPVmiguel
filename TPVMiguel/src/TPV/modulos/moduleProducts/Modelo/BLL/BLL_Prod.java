/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.moduleProducts.Modelo.BLL;

import TPV.librerias.FileUpload;
import TPV.librerias.Funciones;
import TPV.modulos.moduleProducts.Controlador.Controlador_Prod;
import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import TPV.modulos.moduleProducts.Modelo.Clases.Productos;
import TPV.modulos.moduleProducts.Modelo.Clases.SingletonProd;
import TPV.modulos.moduleProducts.Modelo.Clases.MiniSimpleTableModel_Prod;
import TPV.modulos.moduleProducts.Modelo.DAO.DAO_Prod;
import TPV.modulos.moduleProducts.Vista.PagerProducts;
import TPV.modulos.moduleProducts.Vista.PagerProducts;
import TPV.modulos.moduleProducts.Vista.UpdateProd;
import TPV.modulos.moduleProducts.pager.pagina;

/**
 *
 * @author pollotorrao
 */
public class BLL_Prod {
    
    public static void pideNombre() {

        DAO_Prod.pideNombre();
        
        if(PagerProducts.labErrorNom.isVisible() == true){
            PagerProducts.txtNombre.requestFocus();
        }

    }
    
    public static void pideID() {
        int pos;

        DAO_Prod.pideID();

        if (PagerProducts.labErrorID.isVisible()) {
            PagerProducts.txtID.requestFocus();
        } else {
            Productos emp = new Productos(PagerProducts.txtID.getText());
            pos = buscaID(emp);
            if (pos != -1) {

                PagerProducts.labErrorID.setText("El id ya se encuentra en la base de datos");
                PagerProducts.labErrorID.setVisible(true);
                PagerProducts.txtID.requestFocus();
                return;
            } else {
                PagerProducts.labErrorID.setVisible(false);
            }

        }

    }
    
    
    public static int buscaID(Productos prodID) {

        int aux = -1;
        BLLprod_DB _prod = new BLLprod_DB();
        _prod.listAllProductosBLL();

        for (int i = 0; i <= (SingletonProd.prod.size() - 1); i++) {
            if ((SingletonProd.prod.get(i).getID_Producto()).equals(prodID.getID_Producto()))//buclea hasta que encuentra un id que concuadre con el comparator de la madre y lo devuelve como aux
            {
                aux = i;
            }
        }
        return aux;//retorna aux, si lo ha encontrado dara la posicion, si no devolvera -1 lo cual significa que no hay id que concuadre
        
    }
    
    public static void pidePrecio() {
        DAO_Prod.pidePrecio();

        if(PagerProducts.labErrorPrecio.isVisible() == true){
            PagerProducts.txtPrecio.requestFocus();
        }
        
    }
    
    public static void pideStock() {
        DAO_Prod.pideStock();

        if(PagerProducts.labErrorStock.isVisible() == true){
            PagerProducts.txtStock.requestFocus();
        }
        
    }
    
    public static void pideDesc() {
        DAO_Prod.pideDesc();
        
        if(PagerProducts.labErrorDesc.isVisible() == true){
            PagerProducts.txtDesc.requestFocus();
        }

    }
    
    public static void pideProd() {
        pideNombre();
        pidePrecio();
        pideStock();
        pideDesc();
        
        DAO_Prod.pideProd();
        
         if(PagerProducts.labErrorNom.isVisible() == false && PagerProducts.labErrorPrecio.isVisible() ==false
                && PagerProducts.labErrorStock.isVisible() == false && PagerProducts.labErrorDesc.isVisible() == false
                && PagerProducts.labErrorID.isVisible() == false     && PagerProducts.labErrorID.isVisible() == false){
             
             if (SingletonProd.producto != null){
                 
                 BLLprod_DB _prod = new BLLprod_DB();
                 _prod.nuevoProdBLL();
                 ((MiniSimpleTableModel_Prod) Controlador_Prod.frmProductos.tablaFijos.getModel()).cargar();
                 
                //Clear
                PagerProducts.txtNombre.setText("");
                PagerProducts.txtID.setText("");
                PagerProducts.txtDesc.setText("");
                PagerProducts.txtID.setText("");
                PagerProducts.txtPrecio.setText("");
                PagerProducts.txtStock.setText("");
                
                JOptionPane.showMessageDialog(null,"Producto creado satisfactoriamente.");
                 
             }
             
             
         }
        
        
        
    }
    
    public static void cambiaProd() {
        cambiaNombre();
        cambiaPrecio();
        cambiaStock();
        cambiaDesc();
        
        
        DAO_Prod.cambiaProd(SingletonProd.producto);
        
         if(PagerProducts.labErrorNom.isVisible() == false && PagerProducts.labErrorPrecio.isVisible() ==false
                && PagerProducts.labErrorStock.isVisible() == false && PagerProducts.labErrorDesc.isVisible() == false
                && PagerProducts.labErrorID.isVisible() == false     && PagerProducts.labErrorID.isVisible() == false){
             
             if (SingletonProd.producto != null){
                 
                 BLLprod_DB _prod = new BLLprod_DB();
                 _prod.modificarProdBLL();
                 ((MiniSimpleTableModel_Prod) Controlador_Prod.frmProductos.tablaFijos.getModel()).cargar();
                 


                //Clear
                PagerProducts.txtNombre.setText("");
                PagerProducts.txtID.setText("");
                PagerProducts.txtDesc.setText("");
                PagerProducts.txtID.setText("");
                PagerProducts.txtPrecio.setText("");
                PagerProducts.txtStock.setText("");
                 
             }
             
             
         }
        
        
        
    }
    
    
    public static void cambiaNombre() {

        DAO_Prod.cambiaNombre();
        
        if(PagerProducts.labErrorNom.isVisible() == true){
            PagerProducts.txtNombre.requestFocus();
        }

    }
    
    public static void cambiaID() {
        int pos;

        DAO_Prod.cambiaID();

        if (UpdateProd.labErrorID.isVisible()) {
            UpdateProd.txtID.requestFocus();
        } else {
            Productos emp = new Productos(UpdateProd.txtID.getText());
            pos = buscaID(emp);
            if (pos != -1) {

                PagerProducts.labErrorID.setText("El id ya se encuentra en la base de datos");
                PagerProducts.labErrorID.setVisible(true);
                PagerProducts.txtID.requestFocus();
                return;
            } else {
                PagerProducts.labErrorID.setVisible(false);
            }

        }

    }
    
    
    public static void cambiaPrecio() {
        DAO_Prod.cambiaPrecio();

        if(PagerProducts.labErrorPrecio.isVisible() == true){
            PagerProducts.txtPrecio.requestFocus();
        }
        
    }
    
    public static void cambiaStock() {
        DAO_Prod.cambiaStock();

        if(PagerProducts.labErrorStock.isVisible() == true){
            PagerProducts.txtStock.requestFocus();
        }
        
    }
    
    public static void cambiaDesc() {
        DAO_Prod.cambiaDesc();
        
        if(PagerProducts.labErrorDesc.isVisible() == true){
            PagerProducts.txtDesc.requestFocus();
        }

    }
    
    
    public static void deleteProd() {
        int n = ((MiniSimpleTableModel_Prod) PagerProducts.tablaFijos.getModel()).getRowCount();
        String id;
        int pos = -1, opc, selec, selec1, inicio;

        if (n != 0) {
            inicio = (pagina.currentPageIndex - 1) * pagina.itemsPerPage;
            selec = PagerProducts.tablaFijos.getSelectedRow();
            selec1 = inicio + selec;
            if (selec == -1) {
                JOptionPane.showMessageDialog(null, "No hay un producto seleccionado", "Error!", 2);
            } else {
                id = (String) PagerProducts.tablaFijos.getModel().getValueAt(selec1, 0);
                opc = Funciones.valida_confirmado("Deseas borrar el producto con id: " + id, "Borrar Producto");
                if (opc == 0) {
                    SingletonProd.producto = new Productos(id);
                    pos = buscaID(SingletonProd.producto);
                    SingletonProd.prod.remove(pos);
                    BLLprod_DB _prod = new BLLprod_DB();
                    _prod.borrarEmpfBLL();
                    ((MiniSimpleTableModel_Prod) PagerProducts.tablaFijos.getModel()).removeRow(selec1);
                    ((MiniSimpleTableModel_Prod) Controlador_Prod.frmProductos.tablaFijos.getModel()).cargar();
                }
            }
        } else {
            Funciones.resultado("lista vacía", "Error!");
        }
    }

    public static int updateProd() {
        int n = ((MiniSimpleTableModel_Prod) PagerProducts.tablaFijos.getModel()).getRowCount();
        int selec, selec1, inicio;

        if (n != 0) {
            inicio = (pagina.currentPageIndex - 1) * pagina.itemsPerPage;
            selec = PagerProducts.tablaFijos.getSelectedRow();
            selec1 = inicio + selec;

            if (selec == -1) {
                JOptionPane.showMessageDialog(null, "No hay un producto seleccionado", "Error!", 2);
            } else {
                SingletonProd.producto = new Productos((String) PagerProducts.tablaFijos.getModel().getValueAt(selec1, 0));
                return 1;
            }

        } else {
            JOptionPane.showMessageDialog(null, "lista vacía", "Error!", 2);

        }

        return 0;

    }
    
    public static void rellenarProd() {
        int pos = -1;

        pos = buscaID(SingletonProd.producto);
        
        PagerProducts.txtNombre.setText(SingletonProd.prod.get(pos).getNombre());
        PagerProducts.txtID.setText(SingletonProd.prod.get(pos).getID_Producto());
        PagerProducts.txtDesc.setText(SingletonProd.prod.get(pos).getDescripcion());
        PagerProducts.txtPrecio.setText(""+SingletonProd.prod.get(pos).getPrecio());
        PagerProducts.txtStock.setText(""+SingletonProd.prod.get(pos).getStock());
        FileUpload.pintar(Controlador_Prod.frmProductos.labPrintAvatar, 120,90, 1,SingletonProd.prod.get(pos).getAvatar());
 

    }
    
    
     public static int buscarProd() {
        int n = ((MiniSimpleTableModel_Prod) PagerProducts.tablaFijos.getModel()).getRowCount();
        String id;
        int res = 0;
        int pos = -1, opc, selec, selec1, inicio;

        if (n != 0) {
            inicio = (pagina.currentPageIndex - 1) * pagina.itemsPerPage;
            selec = PagerProducts.tablaFijos.getSelectedRow();
            selec1 = inicio + selec;
            if (selec == -1) {
                JOptionPane.showMessageDialog(null, "No hay productos seleccionados", "Error!", 2);
            } else {
                id = (String) PagerProducts.tablaFijos.getModel().getValueAt(selec1, 0);
                opc = Funciones.valida_confirmado("Desea realizar el pedido  " + id, "Realizar pedido");
                if (opc == 0) {
                    SingletonProd.producto = new Productos(id);
                    SingletonProd.prod.get(buscaID(SingletonProd.producto));
                    SingletonProd.producto = SingletonProd.prod.get(buscaID(SingletonProd.producto));
                    res = 1;
                    
                }
            }
        } else {
            Funciones.resultado("lista vacía", "Error!");
        }
        
        return res;
        
    }
    
    
}
