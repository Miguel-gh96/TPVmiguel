/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleProducts.Modelo.BLL;

import TPV.modulos.moduleProducts.Modelo.DAO.DAO_Prod_Fitxers;



/**
 *
 * @author miguel
 */
public class BLL_Prod_Fitxers {
    
    
    public static void GuardarJSON() {
        BLLprod_DB _prod = new BLLprod_DB();
        _prod.listAllProductosBLL();
        DAO_Prod_Fitxers.GuardarJSON();
    }
    
    public static void GuardarXML() {
        BLLprod_DB _prod = new BLLprod_DB();
        _prod.listAllProductosBLL();
        DAO_Prod_Fitxers.GuardarXML();
    }
    
    public static void GuardarTXT() {
        BLLprod_DB _prod = new BLLprod_DB();
        _prod.listAllProductosBLL();
        DAO_Prod_Fitxers.GuardarTXT();
    }
    
	
    
    
}
