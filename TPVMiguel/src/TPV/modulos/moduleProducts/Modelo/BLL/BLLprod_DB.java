/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.moduleProducts.Modelo.BLL;

import TPV.clases.ConexionBD;
import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import TPV.modulos.moduleProducts.Modelo.Clases.Productos;
import TPV.modulos.moduleProducts.Modelo.Clases.SingletonProd;
import TPV.modulos.moduleProducts.Modelo.DAO.DAOprod_DB;

/**
 *
 * @author Jandro
 */
public class BLLprod_DB {
    
    public Productos buscarPorIDBLL(Productos pro){
        Connection _con = null;
        Productos _productoObtenido = null;
        ConexionBD _conexion_BD = new ConexionBD();
        _con = _conexion_BD.AbrirConexion();
        DAOprod_DB _productosDAOBD = new DAOprod_DB();
        
        _productoObtenido = _productosDAOBD.buscarPorIDDAO(_con, pro);
        _conexion_BD.CerrarConexion(_con);
        
        return _productoObtenido;
    }
    
    public void listAllProductosBLL() {
        
        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();
        DAOprod_DB _productosDAOBD = new DAOprod_DB();
        try {
            _productosDAOBD.listAllProductosDAO(_con);//Recuperamos los prodcutos
            
            } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");

        }
        _conexion_DB.CerrarConexion(_con);
 
    }
    
    public void nuevoProdBLL() {


        Connection _con = null;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();
        DAOprod_DB prod_DAO = new DAOprod_DB();

          prod_DAO.nuevoProdDAO(_con);
        _conexion_DB.CerrarConexion(_con);

    }
    
    public int modificarProdBLL() {
        int resultado = 0;
        Connection _con;
        ConexionBD _conexion_DB = new ConexionBD();
        _con = _conexion_DB.AbrirConexion();

        DAOprod_DB _prodDAO = new DAOprod_DB();

        resultado=_prodDAO.modificarProdDAO(_con);

        _conexion_DB.CerrarConexion(_con);
        
        return resultado;
    }
    
    
    public void borrarEmpfBLL() {


        Connection _con;
        ConexionBD _conexion_DB = new ConexionBD();

        _con = _conexion_DB.AbrirConexion();

        DAOprod_DB _prodDAO = new DAOprod_DB();

        _prodDAO.borrarProdDAO(_con);

        _conexion_DB.CerrarConexion(_con);
    }
   
    
}
