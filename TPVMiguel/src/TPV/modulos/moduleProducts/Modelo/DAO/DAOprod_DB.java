/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.moduleProducts.Modelo.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import TPV.modulos.moduleProducts.Modelo.Clases.Productos;
import TPV.modulos.moduleProducts.Modelo.Clases.SingletonProd;
import static TPV.modulos.moduleProducts.Modelo.Clases.MiniSimpleTableModel_Prod.datosaux;


/**
 *
 * @author pollotorrao
 */
public class DAOprod_DB {
    
   public void listAllProductosDAO(Connection con) {
        ResultSet rs = null;
        PreparedStatement stmt = null;
        
        
        SingletonProd.prod.clear();
        try {
            stmt = con.prepareStatement("SELECT * FROM TPV.Productos");
            rs = stmt.executeQuery();
            Productos _productos = null;
            while (rs.next()) {
                
                _productos = new Productos();
                _productos.setID_Producto(rs.getString("ID_Producto"));
                _productos.setNombre(rs.getString("Nombre"));
                _productos.setDescripcion(rs.getString("Descripcion"));
                _productos.setPrecio(rs.getFloat("Precio"));
                _productos.setStock(rs.getInt("Stock"));
                _productos.setAvatar(rs.getString("Avatar"));
                SingletonProd.prod.add(_productos);
                
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un problema al obtener los productos!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }


    }

    public Productos buscarPorIDDAO(Connection con, Productos pro) {
        Productos _productos = null;
        ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement("SELECT * FROM TPV.Productos WHERE ID_Producto=?");
            stmt.setString(1, pro.getID_Producto());
            rs = stmt.executeQuery();
            while (rs.next()) {
                _productos = new Productos(null, null, null, 0, 0, null);
                obtenUsuarioFila(rs, _productos);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un problema al buscar el producto por ID");
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en el Logger");
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en el Logger");
                }
            }
        }
        return _productos;
    }

    private void obtenUsuarioFila(ResultSet rs, Productos pro) {
        try {
            pro.setID_Producto(rs.getString("ID_Producto"));
            pro.setNombre(rs.getString("Nombre"));
            pro.setDescripcion(rs.getString("Descripcion"));
            pro.setPrecio(rs.getFloat("Precio"));
            pro.setStock(rs.getInt("Stock"));
            pro.setAvatar(rs.getString("Avatar"));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en el Logger");
        }
    }
    
    
    public int nuevoProdDAO(Connection con) {
        PreparedStatement stmt = null;
        int resultado=0;
        try {
            
            stmt = con.prepareStatement("INSERT INTO TPV.Productos"
                    + "(ID_Producto,Nombre,Descripcion,Precio,Stock,Avatar) "
                    + "VALUES(?,?,?,?,?,?)");
            stmt.setString(1, SingletonProd.producto.getID_Producto());
            stmt.setString(2, SingletonProd.producto.getNombre());
            stmt.setString(3, SingletonProd.producto.getDescripcion());
            stmt.setFloat(4, SingletonProd.producto.getPrecio());
            stmt.setInt(5, SingletonProd.producto.getStock());
            stmt.setString(6, SingletonProd.producto.getAvatar());
                         JOptionPane.showMessageDialog(null,SingletonProd.producto.toString());
            resultado=stmt.executeUpdate();
            
            
           
        } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, "Ha habido un problema al insertar un nuevo empleado fijo!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }
        return resultado;
    }
    
    public int modificarProdDAO(Connection con){

        PreparedStatement stmt = null;
        int resultado=0;
  
        try {
            stmt = con.prepareStatement("UPDATE Productos SET Nombre = ?, Descripcion  = ? , Precio = ?,Stock = ? ,Avatar = ? WHERE ID_Producto=?");
            stmt.setString(1, SingletonProd.producto.getNombre());
            stmt.setString(2, SingletonProd.producto.getDescripcion());
            stmt.setFloat(3, SingletonProd.producto.getPrecio());
            stmt.setInt(4, SingletonProd.producto.getStock());
            stmt.setString(5, SingletonProd.producto.getAvatar());
            stmt.setString(6, SingletonProd.producto.getID_Producto());
            resultado = stmt.executeUpdate();
            
            


        } catch (SQLException ex) {
           // throw new Exception("Ha habido un problema al insertar la factura "+ex.getMessage());
            JOptionPane.showMessageDialog(null, "Ha habido un problema al actualizar el empleado fijo!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    //Logger.getLogger(alumnoPresencialDAO.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "Ha habido un error Logger!");
                }
            }
        }
        return resultado;

    }
    
    public int borrarProdDAO(Connection con) {
        int resultado=0;
        PreparedStatement stmt = null;

        try {
            stmt = con.prepareStatement("DELETE FROM Productos WHERE ID_Producto=?");
            stmt.setString(1, SingletonProd.producto.getID_Producto());
            resultado = stmt.executeUpdate();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ha habido un error al eliminar el producto!");
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Error en el Logger!");
                }
            }
        }
        return resultado;
    }

    
}
