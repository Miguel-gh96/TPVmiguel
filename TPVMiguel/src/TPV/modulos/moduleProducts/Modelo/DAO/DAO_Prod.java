/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TPV.modulos.moduleProducts.Modelo.DAO;

import TPV.librerias.FileUpload;
import TPV.librerias.Validate;
import static TPV.modulos.moduleProducts.Controlador.Controlador_Prod.frmProductos;
import TPV.modulos.moduleProducts.Modelo.BLL.BLLprod_DB;
import TPV.modulos.moduleProducts.Modelo.Clases.MiniSimpleTableModel_Prod;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;

import TPV.modulos.moduleProducts.Modelo.Clases.Productos;
import TPV.modulos.moduleProducts.Modelo.Clases.SingletonProd;
import TPV.modulos.moduleProducts.Vista.PagerProducts;
import TPV.modulos.moduleProducts.Vista.PagerProducts;


/**
 *
 * @author pollotorrao
 */
public class DAO_Prod {
    
    
    public static void pideNombre() {

        if (PagerProducts.txtNombre.getText().isEmpty()) {
            PagerProducts.labErrorNom.setText("El campo del nombre no puede estar vacio ");
            PagerProducts.labErrorNom.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorNom.setVisible(false);

            if (Validate.palabraSoloLet(PagerProducts.txtNombre.getText()) == false) {
                PagerProducts.labErrorNom.setText("El campo del nombre solo puede tener letras");
                PagerProducts.labErrorNom.setVisible(true);
                return;
            } else {
                PagerProducts.labErrorNom.setVisible(false);
            }
        }

    }
    
    public static void pidePrecio() {
        if (PagerProducts.txtPrecio.getText().isEmpty()) {
            PagerProducts.labErrorPrecio.setText("El campo del telefono no puede estar vacio");
            PagerProducts.labErrorPrecio.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorPrecio.setVisible(false);
            try{
                Float.parseFloat(PagerProducts.txtPrecio.getText());
                PagerProducts.labErrorPrecio.setVisible(false);
            }catch(Exception e){
                PagerProducts.labErrorPrecio.setText("No ha introducido un número");
                PagerProducts.labErrorPrecio.setVisible(true);
                return;      
            }
            
        }

    }
    
    public static void pideStock() {
        if (PagerProducts.txtStock.getText().isEmpty()) {
            PagerProducts.labErrorStock.setText("El campo del telefono no puede estar vacio");
            PagerProducts.labErrorStock.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorStock.setVisible(false);
            try{
                Integer.parseInt(PagerProducts.txtStock.getText());
                PagerProducts.labErrorStock.setVisible(false);
            }catch(Exception e){
                PagerProducts.labErrorStock.setText("No ha introducido un número");
                PagerProducts.labErrorStock.setVisible(true);
                return;      
            }
            
        }

    }
    
    public static void pideDesc() {
        if (PagerProducts.txtDesc.getText().isEmpty()) {
            PagerProducts.labErrorDesc.setText("El campo de descripción no puede estar vacio");
            PagerProducts.labErrorDesc.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorDesc.setVisible(false);
            
        }

    }
    
    public static void pideID() {

        if (PagerProducts.txtID.getText().isEmpty()) {
            PagerProducts.labErrorID.setText("El campo del id no puede estar vacio");
            PagerProducts.labErrorID.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorID.setVisible(false);

        }

    }
    
    
    public static void pideProd() {
        String nombre,id,desc;
        int stock;
        float precio;
        
        if(PagerProducts.labErrorNom.isVisible() == false && PagerProducts.labErrorPrecio.isVisible() ==false
                && PagerProducts.labErrorStock.isVisible() == false && PagerProducts.labErrorDesc.isVisible() == false
                && PagerProducts.labErrorID.isVisible() == false     && PagerProducts.labErrorID.isVisible() == false){
        
            nombre = PagerProducts.txtNombre.getText();
            id = PagerProducts.txtID.getText();
            precio = Float.parseFloat(PagerProducts.txtPrecio.getText());
            stock =  Integer.parseInt(PagerProducts.txtStock.getText());
            desc = PagerProducts.txtDesc.getText();
            
            SingletonProd.producto = new Productos(id,nombre,desc,precio,stock,FileUpload.PATH_auto);
            
        }
        
        
    }
    
   public static void cambiaNombre() {

        if (PagerProducts.txtNombre.getText().isEmpty()) {
            PagerProducts.labErrorNom.setText("El campo del nombre no puede estar vacio ");
            PagerProducts.labErrorNom.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorNom.setVisible(false);

            if (Validate.palabraSoloLet(PagerProducts.txtNombre.getText()) == false) {
                PagerProducts.labErrorNom.setText("El campo del nombre solo puede tener letras");
                PagerProducts.labErrorNom.setVisible(true);
                return;
            } else {
                PagerProducts.labErrorNom.setVisible(false);
            }
        }

    }
    
    public static void cambiaPrecio() {
        if (PagerProducts.txtPrecio.getText().isEmpty()) {
            PagerProducts.labErrorPrecio.setText("El campo del telefono no puede estar vacio");
            PagerProducts.labErrorPrecio.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorPrecio.setVisible(false);
            try{
                Float.parseFloat(PagerProducts.txtPrecio.getText());
                PagerProducts.labErrorPrecio.setVisible(false);
            }catch(Exception e){
                PagerProducts.labErrorPrecio.setText("No ha introducido un número");
                PagerProducts.labErrorPrecio.setVisible(true);
                return;      
            }
            
        }

    }
    
    public static void cambiaStock() {
        if (PagerProducts.txtStock.getText().isEmpty()) {
            PagerProducts.labErrorStock.setText("El campo del telefono no puede estar vacio");
            PagerProducts.labErrorStock.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorStock.setVisible(false);
            try{
                Integer.parseInt(PagerProducts.txtStock.getText());
                PagerProducts.labErrorStock.setVisible(false);
            }catch(Exception e){
                PagerProducts.labErrorStock.setText("No ha introducido un número");
                PagerProducts.labErrorStock.setVisible(true);
                return;      
            }
            
        }

    }
    
    public static void cambiaDesc() {
        if (PagerProducts.txtDesc.getText().isEmpty()) {
            PagerProducts.labErrorDesc.setText("El campo de descripción no puede estar vacio");
            PagerProducts.labErrorDesc.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorDesc.setVisible(false);
            
        }

    }
    
    public static void cambiaID() {

        if (PagerProducts.txtID.getText().isEmpty()) {
            PagerProducts.labErrorID.setText("El campo del id no puede estar vacio");
            PagerProducts.labErrorID.setVisible(true);
            return;
        } else {
            PagerProducts.labErrorID.setVisible(false);

        }

    }
    
    public static void cambiaProd(Productos p) {
        String nombre,id,desc;
        int stock;
        float precio;
        
        if(PagerProducts.labErrorNom.isVisible() == false && PagerProducts.labErrorPrecio.isVisible() ==false
                && PagerProducts.labErrorStock.isVisible() == false && PagerProducts.labErrorDesc.isVisible() == false
                && PagerProducts.labErrorID.isVisible() == false     && PagerProducts.labErrorID.isVisible() == false){
                
                p.setNombre(PagerProducts.txtNombre.getText());
                p.setID_Producto(PagerProducts.txtID.getText());
                p.setDescripcion(PagerProducts.txtDesc.getText());
                p.setPrecio(Float.parseFloat(PagerProducts.txtPrecio.getText()));
                p.setStock(Integer.parseInt(PagerProducts.txtStock.getText()));
                p.setAvatar(FileUpload.PATH_auto);
            
            
        }
        
        
    }
    
    
    
    
    
}
