/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleProducts.Modelo.DAO;
import TPV.modulos.gestionPers.gestionEF.modelo.DAO.*;
import TPV.Ppal;
import TPV.librerias.Funciones;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EfBLLfitxers;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;

/**
 *
 * @author miguel
 */
public class DAO_Prod_Fitxers {
    
    
    
    public static void GuardarJSON(){
        TPV.modulos.moduleProducts.Modelo.libreriasProd.Json.generajsonProd();//Genera un archivo json con todos los datos del Arraylist de productos
       
    }
    
    public static void GuardarTXT() {
        TPV.modulos.moduleProducts.Modelo.libreriasProd.Txt.generatxtProd();//Lo mismo que antes pero con un txt
     
    }
    
    public static void GuardarXML() {
        TPV.modulos.moduleProducts.Modelo.libreriasProd.Xml.generaxmlProd();//Ídem pero para un xml
      
    }
    

	

    
}
