/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TPV.modulos.moduleProducts.Vista;

import TPV.modulos.gestionPers.gestionEF.vista.*;
import TPV.modulos.gestionPers.gestionEF.modelo.BLL.EfGraficoBLL;
import TPV.modulos.gestionPers.gestionEF.modelo.clases.ArraylistEF;
import com.toedter.calendar.JTextFieldDateEditor;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.DefaultComboBoxModel;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

/**
 *
 * @author miguel
 */
public class CreateProd extends javax.swing.JFrame {
    
    /**
     * Creates new form ReadEF
     */
    public CreateProd() {
        initComponents();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlEmpfMod = new javax.swing.JPanel();
        labTitulo = new javax.swing.JLabel();
        labNombre = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        labDNI = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        labTlfno = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        labCorreo = new javax.swing.JLabel();
        labErrorNom = new javax.swing.JLabel();
        labErrorID = new javax.swing.JLabel();
        labErrorPrecio = new javax.swing.JLabel();
        labCorrecto = new javax.swing.JLabel();
        labAnadir = new javax.swing.JLabel();
        labPrintAvatar = new javax.swing.JLabel();
        labChangeAvatar = new javax.swing.JLabel();
        labCorreo1 = new javax.swing.JLabel();
        txtStock = new javax.swing.JTextField();
        labErrorStock = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDesc = new javax.swing.JTextArea();
        labErrorDesc = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlEmpfMod.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        labTitulo.setText("Productos");
        pnlEmpfMod.add(labTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 12, -1, -1));

        labNombre.setText("Nombre del Producto");
        pnlEmpfMod.add(labNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, 170, -1));

        txtNombre.setText("Nombre");
        pnlEmpfMod.add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 69, 260, 28));

        labDNI.setText("ID_Producto");
        pnlEmpfMod.add(labDNI, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, -1));

        txtID.setText("00000000P");
        pnlEmpfMod.add(txtID, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, 256, 28));

        labTlfno.setText("Precio");
        pnlEmpfMod.add(labTlfno, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 50, -1, -1));
        pnlEmpfMod.add(txtPrecio, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 70, 256, 28));

        labCorreo.setText("Descripción");
        pnlEmpfMod.add(labCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 250, -1, -1));

        labErrorNom.setForeground(new java.awt.Color(255, 0, 0));
        labErrorNom.setText("El campo  es incorrecto.");
        pnlEmpfMod.add(labErrorNom, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 103, -1, -1));

        labErrorID.setForeground(new java.awt.Color(255, 0, 0));
        labErrorID.setText("El campo  es incorrecto.");
        pnlEmpfMod.add(labErrorID, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, -1, -1));

        labErrorPrecio.setForeground(new java.awt.Color(255, 0, 0));
        labErrorPrecio.setText("El campo  es incorrecto.");
        pnlEmpfMod.add(labErrorPrecio, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 100, -1, -1));

        labCorrecto.setForeground(java.awt.Color.green);
        labCorrecto.setText("Producto creado con éxito");
        pnlEmpfMod.add(labCorrecto, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 460, -1, -1));

        labAnadir.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        labAnadir.setText("Añadir");
        pnlEmpfMod.add(labAnadir, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 460, -1, -1));
        pnlEmpfMod.add(labPrintAvatar, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 70, 160, 130));

        labChangeAvatar.setText("Cambiar Foto");
        pnlEmpfMod.add(labChangeAvatar, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 220, -1, -1));

        labCorreo1.setText("Stock");
        pnlEmpfMod.add(labCorreo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 150, -1, -1));
        pnlEmpfMod.add(txtStock, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 170, 256, 28));

        labErrorStock.setForeground(new java.awt.Color(255, 0, 0));
        labErrorStock.setText("El campo  es incorrecto.");
        pnlEmpfMod.add(labErrorStock, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 210, -1, -1));

        txtDesc.setColumns(20);
        txtDesc.setRows(5);
        jScrollPane1.setViewportView(txtDesc);

        pnlEmpfMod.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 280, 260, 90));

        labErrorDesc.setForeground(new java.awt.Color(255, 0, 0));
        labErrorDesc.setText("El campo  es incorrecto.");
        pnlEmpfMod.add(labErrorDesc, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 380, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlEmpfMod, javax.swing.GroupLayout.PREFERRED_SIZE, 980, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(147, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlEmpfMod, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JLabel labAnadir;
    public static javax.swing.JLabel labChangeAvatar;
    public static javax.swing.JLabel labCorrecto;
    private javax.swing.JLabel labCorreo;
    private javax.swing.JLabel labCorreo1;
    private javax.swing.JLabel labDNI;
    public static javax.swing.JLabel labErrorDesc;
    public static javax.swing.JLabel labErrorID;
    public static javax.swing.JLabel labErrorNom;
    public static javax.swing.JLabel labErrorPrecio;
    public static javax.swing.JLabel labErrorStock;
    private javax.swing.JLabel labNombre;
    public static javax.swing.JLabel labPrintAvatar;
    private javax.swing.JLabel labTitulo;
    private javax.swing.JLabel labTlfno;
    public static javax.swing.JPanel pnlEmpfMod;
    public static javax.swing.JTextArea txtDesc;
    public static javax.swing.JTextField txtID;
    public static javax.swing.JTextField txtNombre;
    public static javax.swing.JTextField txtPrecio;
    public static javax.swing.JTextField txtStock;
    // End of variables declaration//GEN-END:variables
}
