CREATE DATABASE  IF NOT EXISTS `TPV` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `TPV`;
-- MySQL dump 10.13  Distrib 5.6.19, for linux-glibc2.5 (x86_64)
--
-- Host: 192.168.1.151    Database: TPV
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `EF`
--

DROP TABLE IF EXISTS `EF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EF` (
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `nif` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nacimiento` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `sueldo` float NOT NULL,
  `avatar` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Antiguedad` int(11) NOT NULL,
  `fecha_contratacion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`nif`),
  UNIQUE KEY `correo_UNIQUE` (`correo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EF`
--

LOCK TABLES `EF` WRITE;
/*!40000 ALTER TABLE `EF` DISABLE KEYS */;
INSERT INTO `EF` VALUES ('Miguel','Calabuig','Mayor 18','00000000T','18/10/1992','655280807','miguelet@gmail.com',22,1000,'src/TPV/imag/default-avatar.jpg',0,'18/10/2014'),('Sergio','Gonzalez','Los Abedules 30','00000001R','11/7/1989','624567890','sergiogonzalez89@hotmail.com',25,1000,'src/TPV/imag/default-avatar.jpg',0,'11/7/2014'),('Paco','Muñoz','Padre Fullana 10','00000002W','22/6/1979','625678901','pacomube@yahoo.com',35,1300,'src/TPV/imag/default-avatar.jpg',15,'22/6/1999'),('Ana','Sanchez','Doctor Pelayo 40','00000003A','11/8/1992','630363231','anasanchez7@gmail.com',22,1000,'src/TPV/imag/default-avatar.jpg',0,'11/8/2014'),('miguel','Torro','Ramal 12','04096345E','6/1/1996','962389446','miguel.3eso@gmail.com',19,1000,'src/TPV/imag/default-avatar.jpg',0,'6/1/2015'),('Monica','Gandía','Av.Almaig','07253376G','11/5/1990','680118945','monik90@gmail.com',24,1000,'src/TPV/imag/default-avatar.jpg',0,'6/5/2015'),('Paco','Gandia','Martinez Valls','23264476E','18/4/1998','680118945','miguelin@gmail.com',17,1000,'src/TPV/imag/default-avatar.jpg',0,'4/4/2015'),('Hristo','Mikolov','calle12','26846346X','5/1/1996','4954894948','hristo@gmail.com',19,1000,'src/TPV/imag/default-avatar.jpg',0,'6/6/2014'),('Cesar','Torro','calle12','34481223Z','2/5/1993','680118945','CesarTor@gmail.com',22,1000,'src/TPV/imag/default-avatar.jpg',0,'1/5/2015'),('Miguel','Gandia','calle12','42194776B','6/1/1996','680118945','miguelin12@gmail.com',19,1000,'src/TPV/imag/default-avatar.jpg',-1,'8/5/2015'),('Miguel','Gandia','calle12','48608794L','6/1/1996','680118945','miguel.gh96@gmail.com',19,1000,'src/TPV/imag/default-avatar.jpg',0,'10/9/2014'),('Tomas','Tortosa','calle10','78904161Q','6/5/1989','680118945','ciruelin@gmail.com',26,1000,'src/TPV/imag/default-avatar.jpg',0,'3/5/2015');
/*!40000 ALTER TABLE `EF` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `TPV`.`EF_AFTER_INSERT` AFTER INSERT ON `EF` FOR EACH ROW
BEGIN
insert into logNewEF (nif,nombre,fecha_insercion) values (new.nif,new.nombre,now());
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `TPV`.`EF_AFTER_UPDATE` AFTER UPDATE ON `EF` FOR EACH ROW
BEGIN
-- Solo se puede hacer modificaciones sobre un solo empleado no se puede moficar en global
insert into logCambioEF (`fecha_Modificacion`,`nif`,`nombreOld`,`NombreNew`,`apellidosOld`,`apellidosNew`,`direccionOld`,`direccionNew`,`fech_NacimientoOld`,
`fech_NacimientoNew`,`telfnoOld`,`tlfnoNew`,`correoOld`,`CorreoNew`,`fech_contOld`,`fecha_contNew`)
values (NOW(),old.nif,old.nombre,new.nombre,old.apellidos,new.apellidos,old.direccion,new.direccion,old.fecha_nacimiento,new.fecha_nacimiento,old.telefono,
new.telefono,old.correo,new.correo,old.fecha_contratacion,new.fecha_contratacion);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `TPV`.`EF_AFTER_DELETE` AFTER DELETE ON `EF` FOR EACH ROW
BEGIN
INSERT INTO `TPV`.`logDeleteEF`(`fecha_Eliminacion`,`nif`,`nombre`,`apellidos`) VALUES(NOW(),old.nif,old.nombre,old.apellidos);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Pedidos`
--

DROP TABLE IF EXISTS `Pedidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedidos` (
  `ID_Pedidos` int(50) NOT NULL DEFAULT '0',
  `ID_Producto` varchar(500) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `DNI` varchar(9) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `Cantidad` int(11) DEFAULT NULL,
  `Importe` float DEFAULT '0',
  PRIMARY KEY (`ID_Pedidos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Pedidos`
--

LOCK TABLES `Pedidos` WRITE;
/*!40000 ALTER TABLE `Pedidos` DISABLE KEYS */;
INSERT INTO `Pedidos` VALUES (0,'1234A','48608794L',1,1.5),(1,'1554C','48608794L',1,1),(3,'4514B','48608794L',3,45),(4,'1234A','48608794L',1,1.5),(5,'1234A','48608794L',2,3),(6,'0215A','48608794L',2,40),(7,'0215A','48608794L',1,20),(8,'0215A','48608794L',7,140);
/*!40000 ALTER TABLE `Pedidos` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `TPV`.`Pedidos_AFTER_INSERT` AFTER INSERT ON `Pedidos` FOR EACH ROW
BEGIN
UPDATE Productos SET Stock=Stock-new.Cantidad WHERE ID_Producto=new.ID_Producto;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Productos`
--

DROP TABLE IF EXISTS `Productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Productos` (
  `ID_Producto` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `Descripcion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `Precio` float NOT NULL,
  `Stock` int(11) NOT NULL,
  `Avatar` varchar(70) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`ID_Producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Productos`
--

LOCK TABLES `Productos` WRITE;
/*!40000 ALTER TABLE `Productos` DISABLE KEYS */;
INSERT INTO `Productos` VALUES ('0215A','tarta de arandanos','Tarta de arandanos  casera.',20,0,'src/TPV/imag/CA0XB.png'),('1234A','Pan redondo','Pan redondo de trigo.',1.5,30,'src/TPV/imag/O108A.png'),('1554C','Magdalena de chocolate','Madalena de chocolate estado unidense.',1,25,'src/TPV/imag/47N2P.png'),('2215B','Tarta de queso','Tarta de queso  con mermelada \npor la parte superior',10,10,'src/TPV/imag/XY7ET.png'),('4514B','tarta de manzana','Tarta de manzana recubierta.',15,3,'src/TPV/imag/Z2YQU.png'),('5645B','tarta de chocolate','Tarta con tres tipos de chocolate.',12,50,'src/TPV/imag/XY7ET.png');
/*!40000 ALTER TABLE `Productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `nif` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nacimiento` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `login` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `avatar` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`nif`),
  UNIQUE KEY `correo_UNIQUE` (`correo`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES ('Miguel','Guillem','Mayor 18','00000000T','18/10/1992','655280807','miguelet@gmail.com',22,'miguel123','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/U6NSC.jpg','user','0'),('Sergio','Gonzalez','Los Abedules 30','00000001R','11/7/1989','624567890','sergiogonzalez89@hotmail.com',25,'sergio','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','admin','0'),('Paco','Muñoz','Padre Fullana 10','00000002W','22/6/1979','625678901','pacomube@yahoo.com',35,'paco','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','admin','0'),('Ana','Sanchez','Doctor Pelayo 40','00000003A','11/8/1992','630363231','anasanchez7@gmail.com',22,'ana','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','admin','0'),('miguel','Torro','Ramal 12','04096345E','6/1/1996','962389446','miguel.3eso@gmail.com',19,'meganeo12','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','user','0'),('Monica','Gandía','Av.Almaig','07253376G','11/5/1990','680118945','monik90@gmail.com',24,'monik19','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','admin','0'),('Manolo','Perez','calle21','19234872H','7/5/1994','680118945','manolo12@gmail.com',21,'manoperez','7f31c778d8785a05d2de7f3e22c9468a','src/TPV/imag/default-avatar.jpg','user','0'),('Paco','Gandia','Martinez Valls','23264476E','18/4/1998','680118945','miguelin@gmail.com',17,'pacGan','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','admin','0'),('Martina','Guillem','av.almaig','26846346X','2/6/1989','68011872','martinaGill@gmail.com',25,'margill','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','user','0'),('Cesar','Gramage','calle12','34481223Z','2/5/1993','680118945','CesarTor@gmail.com',22,'Cesto','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','admin','0'),('Miguel','Gandia','calle12','42194776B','6/1/1996','564654654','miguelin12@gmail.com',19,'Migan','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','user','0'),('Pascual','Olcina','av.albaida','43532302H','5/1/1995','680128741','pacual@gmail.com',20,'pascu','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','user','0'),('Cesar','Polluelo','av.almaig','43553044Z','2/5/1999','680118945','ces@gmail.com',16,'cespo','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','user','0'),('Miguel','Gandia','calle12','48608794L','6/1/1996','680118945','miguel.gh96@gmail.com',19,'pull12','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','admin','0'),('Oscar','Martinez','av.albaida','65093621X','1/5/1994','680118945','oscarM@gmail.com',21,'oscma','9ae0147d65724f72f74804af4aac6f13','src/TPV/imag/default-avatar.jpg','admin','0');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `TPV`.`User_AFTER_INSERT` AFTER INSERT ON `User` FOR EACH ROW
BEGIN
insert into logNewUser (nif,nombre,fecha_insercion) values (new.nif,new.nombre,now());
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `TPV`.`User_AFTER_UPDATE` AFTER UPDATE ON `User` FOR EACH ROW
BEGIN
-- Solo se puede hacer modificaciones sobre un solo empleado no se puede moficar en global
insert into logCambioUser (`fecha_Modificacion`,`nif`,`nombreOld`,`NombreNew`,`apellidosOld`,`apellidosNew`,`direccionOld`,`direccionNew`,`fech_NacimientoOld`,
`fech_NacimientoNew`,`telfnoOld`,`tlfnoNew`,`correoOld`,`CorreoNew`,`UsuarioOld`,`UsuarioNew`,`tipoOld`,`tipoNew`)
values (NOW(),old.nif,old.nombre,new.nombre,old.apellidos,new.apellidos,old.direccion,new.direccion,old.fecha_nacimiento,new.fecha_nacimiento,old.telefono,
new.telefono,old.correo,new.correo,old.login,new.login,old.tipo,new.tipo);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`%`*/ /*!50003 TRIGGER `TPV`.`User_AFTER_DELETE` AFTER DELETE ON `User` FOR EACH ROW
BEGIN
INSERT INTO `TPV`.`logDeleteUser`(`fecha_Eliminacion`,`nif`,`nombre`,`apellidos`) VALUES(NOW(),old.nif,old.nombre,old.apellidos);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `logCambioEF`
--

DROP TABLE IF EXISTS `logCambioEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logCambioEF` (
  `fecha_Modificacion` datetime NOT NULL,
  `nif` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `nombreOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `NombreNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellidosOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellidosNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `direccionOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `direccionNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fech_NacimientoOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fech_NacimientoNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `telfnoOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `tlfnoNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correoOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `CorreoNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fech_contOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_contNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`fecha_Modificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logCambioEF`
--

LOCK TABLES `logCambioEF` WRITE;
/*!40000 ALTER TABLE `logCambioEF` DISABLE KEYS */;
INSERT INTO `logCambioEF` VALUES ('2015-04-27 10:52:19','48608794L','Eusebio','Ramon','Esparza','Esparza','calle12','calle12','06/01/1995','06/01/1995','680118945','680118945','ads@gmail.com','ads@gmail.com','06/01/2015','06/01/2015'),('2015-04-27 11:21:49','16386891N','Tono','Tono','Castaño','Perez','calle12','calle12','9/4/1994','9/4/1994','680118947','680118947','tono@gmail.com','tono@gmail.com','5/4/2015','5/4/2015'),('2015-04-27 12:45:59','73099467Q','Antonio','Antonio','Garcia','Garcia','Ramon y Cajal 71','Ramon y Cajal 71','30/8/1989','30/8/1989','623456789','623456789','punkato89@hotmail.com','punkato89@hotmail.com','30/8/2014','30/8/2014'),('2015-05-01 11:09:41','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-01 11:13:47','00000001R','Sergio','Sergio','Gonzalez','Gonzalez','Los Abedules 30','Los Abedules 30','11/7/1989','11/7/1989','624567890','624567890','sergiogonzalez89@hotmail.com','sergiogonzalez89@hotmail.com','11/7/2014','11/7/2014'),('2015-05-01 11:14:09','00000003A','Ana','Ana','Sanchez','Sanchez','Doctor Pelayo 40','Doctor Pelayo 40','11/8/1992','11/8/1992','630363231','630363231','anasanchez7@gmail.com','anasanchez7@gmail.com','11/8/2014','11/8/2014'),('2015-05-01 11:14:48','04096345E','miguel','miguel','Torro','Torro','Ramal 12','Ramal 12','6/1/1996','6/1/1996','962389446','962389446','miguel.gh96@gmail.com','miguel.gh96@gmail.com','6/1/2015','6/1/2015'),('2015-05-06 16:22:54','04096345E','miguel','miguel','Torro','Torro','Ramal 12','Ramal 12','6/1/1996','6/1/1996','962389446','962389446','miguel.gh96@gmail.com','miguel.gh96@gmail.com','6/1/2015','6/1/2015'),('2015-05-07 12:21:10','04096345E','miguel','miguel','Torro','Torro','Ramal 12','Ramal 12','6/1/1996','6/1/1996','962389446','962389446','miguel.gh96@gmail.com','miguel.gh96@gmail.com','6/1/2015','6/1/2015'),('2015-05-07 12:24:51','04096345E','miguel','miguel','Torro','Torro','Ramal 12','Ramal 12','6/1/1996','6/1/1996','962389446','962389446','miguel.gh96@gmail.com','miguel.gh96@gmail.com','6/1/2015','6/1/2015'),('2015-05-07 12:31:55','04096345E','miguel','miguel','Torro','Torro','Ramal 12','Ramal 12','6/1/1996','6/1/1996','962389446','962389446','miguel.gh96@gmail.com','miguel.gh96@gmail.com','6/1/2015','6/1/2015'),('2015-05-07 12:37:12','04096345E','miguel','miguel','Torro','Torro','Ramal 12','Ramal 12','6/1/1996','6/1/1996','962389446','962389446','miguel.gh96@gmail.com','miguel.gh96@gmail.com','6/1/2015','6/1/2015'),('2015-05-11 00:32:41','73099467Q','Antonio','Antonio','Garcia','Garcia','Ramon y Cajal 71','Ramon y Cajal 71','30/8/1989','30/8/1989','623456789','623456789','punkato89@hotmail.com','miguel.2eso@gmail.com','30/8/2014','30/8/2014'),('2015-05-11 00:33:31','23264476E','Paco','Paco','Gandia','Gandia','Martinez Valls','Martinez Valls','18/4/1998','18/4/1998','680118945','680118945','paco@gmail.com','miguelin@gmail.com','4/4/2015','4/4/2015'),('2015-05-11 00:34:07','42194776B','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguelin12@gmail.com','8/5/2015','8/5/2015'),('2015-05-11 09:11:02','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 09:11:04','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 09:25:41','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 09:25:42','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 09:30:06','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 09:30:07','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 09:33:52','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 09:34:12','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 09:37:14','04096345E','miguel','miguel','Torro','Torro','Ramal 12','Ramal 12','6/1/1996','6/1/1996','962389446','962389446','miguel.gh96@gmail.com','miguel.3eso@gmail.com','6/1/2015','6/1/2015'),('2015-05-11 09:38:40','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 09:38:41','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 10:25:06','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle23','6/5/1989','6/5/1989','680118945','680118945','miguel.gh96@gmail.com','ciruelin@gmail.com','3/5/2015','3/5/2015'),('2015-05-11 10:28:50','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 11:46:26','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 11:47:29','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 11:49:25','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 11:57:32','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 12:15:41','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 12:15:42','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 12:17:59','73099467Q','Antonio','Antonio','Garcia','Garcia','Ramon y Cajal 71','Ramon y Cajal 71','30/8/1989','30/8/1989','623456789','623456789','miguel.2eso@gmail.com','miguel.4eso@gmail.com','30/8/2014','30/8/2014'),('2015-05-11 13:22:34','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-11 13:23:10','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-11 13:34:41','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-11 13:35:05','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-11 14:12:09','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-11 14:12:35','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-11 14:13:27','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 14:17:15','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-11 14:43:00','34481223Z','Cesar','Cesar','Torro','Torro','calle12','calle12','2/5/1993','2/5/1993','680118945','680118945','CesarTor@gmail.com','CesarTor@gmail.com','15/5/2015','1/5/2015'),('2015-05-11 14:47:42','07253376G','Monica','Monica','Gandía','Gandía','Av.Almaig','Av.Almaig','11/5/1990','11/5/1990','680118945','680118945','monik90@gmail.com','monik90@gmail.com','6/5/2015','6/5/2015'),('2015-05-11 14:48:29','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-11 14:49:58','34481223Z','Cesar','Cesar','Torro','Torro','calle12','calle12','2/5/1993','2/5/1993','680118945','680118945','CesarTor@gmail.com','CesarTor@gmail.com','1/5/2015','1/5/2015'),('2015-05-11 15:11:11','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:11:14','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:12:59','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:30:20','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:32:24','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:44:33','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:44:37','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:46:18','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:46:20','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:47:27','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:47:40','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:49:35','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:49:46','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 15:51:38','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 16:04:02','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 16:04:05','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 16:05:06','07253376G','Monica','Monica','Gandía','Gandía','Av.Almaig','Av.Almaig','11/5/1990','11/5/1990','680118945','680118945','monik90@gmail.com','monik90@gmail.com','6/5/2015','6/5/2015'),('2015-05-11 16:10:48','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 16:11:47','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-11 16:11:55','48608794L','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','680118945','miguel.gh96@gmail.com','miguel.gh96@gmail.com','10/9/2014','10/9/2014'),('2015-05-14 13:36:44','73099467Q','Antonio','Antonio','Garcia','Garcia','Ramon y Cajal 71','Ramon y Cajal 71','30/8/1989','30/8/1989','623456789','623456789','miguel.4eso@gmail.com','miguel.4eso@gmail.com','30/8/2014','30/8/2014'),('2015-05-14 17:51:11','48287734Q','Yolanda','Yolando','Moneris','Moneris','calle12','calle12','1/5/1980','1/5/1980','68011844','68011844','yomogan@gmail.com','yomogan@gmail.com','1/5/2015','1/5/2015'),('2015-05-30 20:46:14','00000000T','Miguel','Miguel','Tortosa','Tortosa','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-05-30 20:46:49','00000003A','Ana','Ana','Sanchez','Sanchez','Doctor Pelayo 40','Doctor Pelayo 40','11/8/1992','11/8/1992','630363231','630363231','anasanchez7@gmail.com','anasanchez7@gmail.com','11/8/2014','11/8/2014'),('2015-05-31 22:18:49','00000000T','Miguel','Miguel','Tortosa','Perez','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-06-01 09:02:20','78904161Q','Tomas','Tomas','Tortosa','Tortosa','calle23','calle10','6/5/1989','6/5/1989','680118945','680118945','ciruelin@gmail.com','ciruelin@gmail.com','3/5/2015','3/5/2015'),('2015-06-01 11:39:08','00000000T','Miguel','Miguel','Perez','Calabuig','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-06-01 11:41:55','00000000T','Miguel','Miguel','Calabuig','Calabuig','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-06-01 11:42:00','00000000T','Miguel','Miguel','Calabuig','Calabuigs','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-06-01 11:42:05','00000000T','Miguel','Miguel','Calabuigs','Calabuigs','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014'),('2015-06-01 11:42:19','00000000T','Miguel','Miguel','Calabuigs','Calabuig','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','18/10/2014','18/10/2014');
/*!40000 ALTER TABLE `logCambioEF` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logCambioUser`
--

DROP TABLE IF EXISTS `logCambioUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logCambioUser` (
  `fecha_Modificacion` datetime NOT NULL,
  `nif` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `nombreOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `NombreNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellidosOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellidosNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `direccionOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `direccionNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fech_NacimientoOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fech_NacimientoNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `telfnoOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `tlfnoNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `correoOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `CorreoNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `UsuarioOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `UsuarioNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `tipoOld` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `tipoNew` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logCambioUser`
--

LOCK TABLES `logCambioUser` WRITE;
/*!40000 ALTER TABLE `logCambioUser` DISABLE KEYS */;
INSERT INTO `logCambioUser` VALUES ('2015-06-01 10:52:03','42194776B','Miguel','Miguel','Gandia','Gandia','calle12','calle12','6/1/1996','6/1/1996','680118945','564654654','miguelin12@gmail.com','miguelin12@gmail.com','Migan','Migan','admin','user'),('2015-06-01 11:38:45','34481223Z','Cesar','Cesar','Torro','Gramage','calle12','calle12','2/5/1993','2/5/1993','680118945','680118945','CesarTor@gmail.com','CesarTor@gmail.com','Cesto','Cesto','admin','admin'),('2015-06-01 16:39:17','00000000T','Miguel','Miguel','Gabilondo','Guillem','Mayor 18','Mayor 18','18/10/1992','18/10/1992','655280807','655280807','miguelet@gmail.com','miguelet@gmail.com','miguel123','miguel123','user','user');
/*!40000 ALTER TABLE `logCambioUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logDeleteEF`
--

DROP TABLE IF EXISTS `logDeleteEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logDeleteEF` (
  `fecha_Eliminacion` datetime NOT NULL,
  `nif` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`fecha_Eliminacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logDeleteEF`
--

LOCK TABLES `logDeleteEF` WRITE;
/*!40000 ALTER TABLE `logDeleteEF` DISABLE KEYS */;
INSERT INTO `logDeleteEF` VALUES ('2015-04-27 11:02:38','48608794L','Ramon',NULL),('2015-04-27 11:24:51','16386891N','Tono','Perez'),('2015-05-11 16:04:36','09161204M','Teresa','Campos'),('2015-05-14 15:54:09','94975674A','juan','tortosa'),('2015-05-14 17:51:33','48287734Q','Yolando','Moneris'),('2015-06-01 09:01:57','73099467Q','Antonio','Garcia');
/*!40000 ALTER TABLE `logDeleteEF` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logDeleteUser`
--

DROP TABLE IF EXISTS `logDeleteUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logDeleteUser` (
  `fecha_Eliminacion` datetime NOT NULL,
  `nif` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logDeleteUser`
--

LOCK TABLES `logDeleteUser` WRITE;
/*!40000 ALTER TABLE `logDeleteUser` DISABLE KEYS */;
INSERT INTO `logDeleteUser` VALUES ('2015-06-01 10:50:07','73099467Q','Antonio','Garcia'),('2015-06-01 11:34:10','37246110W','Borja','Esplugues'),('2015-06-01 11:38:04','29740882L','pere','camos'),('2015-06-01 16:41:19','89437389A','Ana','dasda');
/*!40000 ALTER TABLE `logDeleteUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logNewEF`
--

DROP TABLE IF EXISTS `logNewEF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logNewEF` (
  `nif` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_insercion` datetime DEFAULT NULL,
  PRIMARY KEY (`nif`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logNewEF`
--

LOCK TABLES `logNewEF` WRITE;
/*!40000 ALTER TABLE `logNewEF` DISABLE KEYS */;
INSERT INTO `logNewEF` VALUES ('07253376G','Monica','2015-05-11 09:50:52'),('09161204M','Teresa','2015-05-11 16:02:54'),('11119941Q','Niki','2015-04-27 10:19:26'),('16386891N','Tono','2015-04-27 11:15:01'),('23264476E','Paco','2015-04-27 11:47:02'),('26846346X','Hristo','2015-06-01 09:03:48'),('34481223Z','Cesar','2015-05-11 14:42:06'),('42194776B','Miguel','2015-05-03 21:17:41'),('48287734Q','Yolanda','2015-05-14 17:50:32'),('48608794L','Miguel','2015-05-10 19:47:04'),('78904161Q','Tomas','2015-05-10 19:54:49'),('94975674A','juan','2015-05-14 13:36:06');
/*!40000 ALTER TABLE `logNewEF` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logNewUser`
--

DROP TABLE IF EXISTS `logNewUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logNewUser` (
  `nif` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_insercion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logNewUser`
--

LOCK TABLES `logNewUser` WRITE;
/*!40000 ALTER TABLE `logNewUser` DISABLE KEYS */;
INSERT INTO `logNewUser` VALUES ('37246110W','Borja','2015-06-01 11:33:17'),('29740882L','pere','2015-06-01 11:37:53'),('89437389A','Ana','2015-06-01 16:41:05');
/*!40000 ALTER TABLE `logNewUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'TPV'
--
/*!50003 DROP PROCEDURE IF EXISTS `pocedure_EdadMedia` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `pocedure_EdadMedia`(OUT `media_edad` DOUBLE)
BEGIN
SELECT ROUND(AVG(edad)) into media_edad FROM EF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pocedure_EdadMediaUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `pocedure_EdadMediaUser`(OUT `media_edad` DOUBLE)
BEGIN
SELECT ROUND(AVG(edad)) into media_edad FROM User;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procedure_MaxMinEdad` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `procedure_MaxMinEdad`(OUT `empf_menor` INT, OUT `empf_mayor` INT)
BEGIN
SELECT MIN(edad),MAX(edad) into empf_menor,empf_mayor FROM EF ORDER BY edad;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `procedure_MaxMinEdadUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `procedure_MaxMinEdadUser`(OUT `user_menor` INT, OUT `user_mayor` INT)
BEGIN
SELECT MIN(edad),MAX(edad) into user_menor,user_mayor FROM User ORDER BY edad;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-04 17:31:47
